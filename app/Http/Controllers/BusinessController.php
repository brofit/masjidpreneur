<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fields;
use App\Models\Business;

class BusinessController extends Controller
{

    // middle check login status
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $field = \App\Models\Fields::all();
        return view('module-business.business', compact('field'));
    }

    public function add_bussiness()
    {
        return view('module-business.add');
    }
    
    public function list_business(){
        $business = Business::orderBy('id_business', 'asc')->get();
        $no = 0;
        $data = array();
        foreach ($business as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->business_name;
            $row[] = $list->business_fields->bsfields_name;
            $row[] = $list->brand;
            $row[] = $list->address;
            $row[] = $list->business_contact;
            $row[] = "
                <a href=\"$list->website\" target=\"_blank\" class=\"modal-basic\"><i class=\"icon icon-link\"></i></a>
                <a href=\"#detailBusiness\" class=\"modal-basic\"
                    onclick=\"editData('$list->id_business')\"><i class=\"icon icon-info\"></i></a>
                <a href=\"#deleteBusiness\" class=\"delete-row modal-basic\"
                    onclick=\"deleteData('$list->id_business')\"><i class=\"icon icon-trash\"></i></a>
            ";
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    // fungsi tampilkan bidang usaha
    public function list_business_fields(){
        $field = Fields::orderBy('id_bsfield', 'asc')->get();
        $no = 0;
        $data = array();
        foreach ($field as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->bsfields_name;
            $row[] = '0';
            $row[] = date('d/m/y H:m:s A', strtotime($list->created_at));
            $row[] = "
                <a href=\"#editBusinessCategory\" class=\"edit-modal modal-basic\"
                    onclick=\"editData('$list->id_bsfield')\"><i class=\"icon icon-pencil\"></i></a>
                <a href=\"#deleteBusinessCategory\" class=\"delete-modal modal-basic\"
                    onclick=\"deleteData('$list->id_bsfield')\"><i class=\"icon icon-trash\"></i>
                </a>
            ";
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }
    
    // fungsi simpan bidang usaha
    public function business_fields_store(Request $request)
    {
        $this->validate($request,[
            'business_category_name'=>'required',
        ]);

        $field = new Fields;
        $field->bsfields_name = $request['business_category_name'];
        $field->save();
        return redirect()->route('bidang-usaha');
    }

    // fungsi hapus bidang usaha
    public function business_fields_destroy($id_bsfield){
        $field = Fields::find($id_bsfield);
        $field->delete();
        return redirect()->route('bidang-usaha');
    }

    public function business_fields_edit($id_bsfield)
    {
        $field = Fields::find($id_bsfield);
        echo json_encode($field);
    }

    public function business_fields_update(Request $request, $id_bsfield)
    {
        $field = Fields::find($id_bsfield);
        $field->bsfields_name = $request['business_category_name'];
        $field->update();
    }

    public function business_member_detail($id_business){
        $business = Business::leftjoin('users', 'users.id_user', '=', 'business.id_user')
            ->where('business.id_business', '=', $id_business)
            ->first([
                'business.id_business',
                'business.id_user',
                'business.id_bsfield',
                'business.business_name',
                'business.brand',
                'business.desc',
                'business.address',
                'business.business_contact',
                'business.website',
                'users.fullname',
            ]);
        echo json_encode($business);
    }

    public function business_member_update(Request $request, $id_business){
        $validate = \Validator::make( $request->all(),
            [
                'id_bsfield'        => 'required|not_in:0',
                'business_name'     => 'required|max:100',
                'brand'             => 'required|max:200',
                'desc'              => 'required',
                'address'           => 'required|max:100',
                'business_contact'  => 'required|alpha_dash|max:20',
                'website'           => 'required|max:200',
            ]
        );

        if ($validate->fails()) {
            return redirect()
            ->route('semua-usaha')
            ->withErrors($validate)
            ->withInput();
        }

        $business_member = Business::findOrFail($id_business);
        $business_member->id_bsfield        = $request['id_bsfield'];
        $business_member->id_user           = $request['id_user'];
        $business_member->business_name     = $request['business_name'];
        $business_member->brand             = $request['brand'];
        $business_member->desc              = $request['desc'];
        $business_member->address           = $request['address'];
        $business_member->business_contact  = $request['business_contact'];
        $business_member->website           = $request['website'];
        $business_member->update();

        return redirect()->route('semua-usaha')
        ->with('status', 'Data telah berhasil diperbarui!');
    }

    public function business_member_destroy($id_business){
        $business_member = Business::where('id_business', '=', $id_business)->first();

        if ($business_member != null) {
            $business_member->delete();
            return redirect()->route('semua-usaha')
            ->with('status', 'Data telah berhasil dihapus!');
        }

        return response()->json(['status', 'Terjadi kesalahan, data gagal dihapus!']);
    }

    public function business_category()
    {
        return view('module-business.business-category');
    }
}
