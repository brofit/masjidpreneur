<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontpageController extends Controller
{
    /**
     * Display a Business Directory
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontpage.homepage-V02');
    }

    public function home_v01()
    {
        return view('frontpage.homepage-V01');
    }

    public function business_directory()
    {
        return view('frontpage.business-directory');
    }

    public function detail_business()
    {
        return view('frontpage.detail-business');
    }

    public function business_result_found()
    {
        return view('frontpage.business-found');
    }

    public function business_result_not_found()
    {
        return view('frontpage.business-not-found');
    }

    public function about_us()
    {
        return view('frontpage.about-us');
    }

    public function contact_us()
    {
        return view('frontpage.contact-us');
    }

    public function member_register()
    {
        return view('frontpage.register');
    }

    public function component()
    {
        return view('frontpage.component');
    }
}
