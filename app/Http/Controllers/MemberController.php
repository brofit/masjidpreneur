<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Members;
use App\Models\Business;
use App\Helpers\Autonumber;
use App\Models\Provinces;
use App\Models\Regencies;
use App\Models\Districts;
use App\Models\MemberPrograms;
use App\Models\Programs;
use Auth;
use File;

class MemberController extends Controller
{
    // middle check login status
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('module-members.member');
    }
    
    // tampil member
    public function add_members(){
        $provinces = Provinces::all();        
        return view('module-members.add', compact('provinces'));
    }

    // list data member
    public function list_members(){
        $member = User::orderby('id_user', 'asc')->where('role', '!=', 99)->get();
        $no = 0;
        $data = array();
        foreach ($member as $list) {
            /* 
            No|ID|Nama|Username|Email|B. Usaha|Usaha|Gender|Aksi
            */
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->id_user;
            $row[] = $list->fullname;
            $row[] = $list->user_name;
            $row[] = $list->email;

            // // bidang usaha
            $business = '';
            foreach($list->business as $bs) {
                $business .= $bs->business_fields->bsfields_name . '<br/>';
            }

            $row[] = $business;
            $row[] = $list->business->count();
            $row[] = $list->members->gender;
            $row[] =  "
                <a href=\"/admin/anggota/detail/$list->id_user\"><i class=\"icon icon-info\"></i></a>
                <a href=\"#deleteMember\" class=\"delete-row modal-basic\"
                    onclick=\"deleteData('$list->id_user')\"><i class=\"icon icon-trash\"></i></a>
            ";
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function member_store(Request $request){
        $validate = \Validator::make( $request->all(), [
                'fullname'        => 'required|max:100',
                'member_contact'  => 'required|numeric|digits_between:6,20',
                'dateofbirth'     => 'required',
                'hometown'        => 'required|max:50',
                'district'        => 'required|not_in:0',
                'regency'         => 'required|not_in:0',
                'province'        => 'required|not_in:0',
                'address'         => 'required',
                'email'           => 'required|email',
                'gender'          => 'required|max:12',
                'avatar'          => 'file|image|mimes:jpeg,png,jpg|max:2048',
                'password'        => 'required|min:2',
                're_password'     => 'required|same:password',
            ]
        );

        if ($validate->fails()) {
            return redirect()
                ->route('tambah-anggota')
                ->withErrors($validate)
                ->withInput();
        }

        $user = new User;
        $user->id_user = $request['id_user'];
        $user->fullname = $request['fullname'];
        $user->user_name = $request['username'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        
        if($request->hasFile('avatar')){
            $id_user = $request['id_user'];
            $file = $request->file('avatar');
            $avatar = "avatar__".$id_user.'__'.time().".".$file->getClientOriginalExtension();
            $path = storage_path('app/public/avatars/');
            
            $file->move($path,$avatar);
            $user->avatar = $avatar;
            $images = $avatar;
        }else{
            $images = $user->avatar;
        }
        $user->role = 1;
        $user->save();

        $member = new Members;
        $member->id_user = $request['id_user'];
        $member->member_contact = $request['member_contact'];
        $member->member_status = $request['member_status'];
        
        $date = str_replace('/', '-', $request['dateofbirth']);
        $member->dateofbirth = date('Y-m-d', strtotime($date));

        $member->hometown = $request['hometown'];
        $member->id = $request['district'];
        $member->address = $request['address'];
        $member->gender = $request['gender'];
        $member->group = $request['group'];
        $member->save();

        return redirect()
            ->route('tambah-anggota')
            ->with('status', 'Data member berhasil ditambahkan.');
    }

    public function member_destroy($id_member){
        $member = User::where('id_user', '=', $id_member)->first();

        if ($member != null) {
            if($member->avatar !== '!logged-user.jpg'){
                File::delete(storage_path('app/public/avatars/' .$member->avatar));
                $member->delete();
                return redirect()->route('semua-anggota');
            }else{
                $member->delete();
                return redirect()->route('semua-anggota');
            }
        }

        return response()->json(['success' => false, 'msg' => 'Get unknown error.']);
    }
    
    public function member_profil_update(Request $request, $id_user){
        $validate = \Validator::make( $request->all(), [
            'fullname'       => 'required|max:100',
            'member_contact' => 'required|numeric|digits_between:6,20',
            'dateofbirth'    => 'required',
            'hometown'       => 'required|max:50',
            'district'       => 'required|not_in:0',
            'regency'        => 'required|not_in:0',
            'province'       => 'required|not_in:0',
            'address'        => 'required',
            'gender'         => 'required|min:1|max:12',
            'avatar'         => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'password'       => 'min:1|min:2',
            're_password'    => 'same:password',
            ]
        );

        $user = User::where('id_user', '=', $id_user)->first();
        $member = Members::where('id_user', '=', $id_user)->first();

        if ($validate->fails()) {

            if(Auth::user()->role == 99){
                return redirect()
                ->route('anggota.detail', $user->id_user)
                ->withErrors($validate)
                ->withInput();
            }else{
                return redirect()
                ->route('member-profile', $user->id_user)
                ->withErrors($validate)
                ->withInput();
            }
        }

        $user->fullname = $request['fullname'];
        if($request->hasFile('avatar')){
            
            if($user->avatar !== '!logged-user.jpg'){
                File::delete(storage_path('app/public/avatars/' .$user->avatar));
            }

            $id_user = $request['id_user'];
            $file = $request->file('avatar');
            $avatar = "avatar__".$id_user.'__'.time().".".$file->getClientOriginalExtension();
            $path = storage_path('app/public/avatars/');
            
            $file->move($path,$avatar);
            $user->avatar = $avatar;
            $images = $avatar;
        }else{
            $images = $user->avatar;
        }
        echo json_encode($user);
        $user->update();

        $member->member_contact = $request['member_contact'];
        $date = str_replace('/', '-', $request['dateofbirth']);
        $member->dateofbirth = date('Y-m-d', strtotime($date));
        $member->hometown = $request['hometown'];
        $member->id = $request['district'];
        $member->address = $request['address'];
        $member->gender = $request['gender'];
        echo json_encode($member);
        $member->update();

        if(Auth::user()->role == 99){
            return redirect()->route('anggota.detail', $user->id_user);
        }else{
            return redirect()->route('member-profile', $user->id_user);
        }
    }

    public function member_detail($id_user){
        $provinces = \App\Models\Provinces::all();
        $field = \App\Models\Fields::all();
        $program = \App\Models\Programs::all();
        $procat = \App\Models\Procat::all();
        $mempro = \App\Models\MemberPrograms::all();
        // $user = User::leftjoin('members', 'members.id_user', '=', 'users.id_user')
        //     ->leftjoin('loc_districts', 'loc_districts.id', '=', 'members.id')
        //     ->leftjoin('business', 'business.id_user', '=', 'members.id_user')
        //     ->leftjoin('member_programs', 'member_programs.id_user', '=', 'members.id_user')
        //     ->where('users.id_user', '=', $id_user)->first();
        // $user = User::where('users.id_user', '=', $id_user)->first();

        $user = User::leftjoin('members', 'members.id_user', '=', 'users.id_user')
            ->leftjoin('loc_districts', 'loc_districts.id', '=', 'members.id')
            ->leftjoin('loc_regencies', 'loc_regencies.id', '=', 'loc_districts.regency_id')
            ->leftjoin('loc_provinces', 'loc_provinces.id', '=', 'loc_regencies.province_id')
            ->leftjoin('business', 'business.id_user', '=', 'members.id_user')
            ->leftjoin('member_programs', 'member_programs.id_user', '=', 'members.id_user')
            ->where('users.id_user', '=', $id_user )
            ->first([
                'users.id_user',
                'users.fullname',
                'users.user_name',
                'users.email',
                'users.avatar',
                'users.user_status',
                'members.id_member',
                'members.member_contact',
                'members.member_status',
                'members.dateofbirth',
                'members.hometown',
                'members.address',
                'members.gender',
                'members.group',
                'members.gender',
                'loc_districts.id as id_dis',
                'loc_districts.name as district_name',
                'loc_regencies.id as id_reg',
                'loc_regencies.name as regency_name',
                'loc_provinces.id as id_pro',
                'loc_provinces.name as province_name',
            ]);

        $districts = \App\Models\Districts::where('regency_id', '=' , $user->id_reg)->get();
        $regencies = \App\Models\Regencies::where('province_id', '=' , $user->id_pro)->get();

        if(Auth::user()->role == '99'){
            return view('module-members.detail', compact(
                'provinces', 
                'user', 
                'field', 
                'procat', 
                'mempro', 
                'program',
                'districts',
                'regencies'
            ));
        }else{
            return view('member.profile', compact(
                'provinces', 
                'user', 
                'field', 
                'procat', 
                'mempro', 
                'program',
                'districts',
                'regencies'
            ));
        }
    }

    // -- Module: Usaha Anggota -- //
    public function list_member_business($id_user){
        $member = Business::where('id_user', '=', $id_user)
            ->orderby('id_business', 'desc')->get();
        $no = 0;
        $data = array();
        foreach ($member as $list) {
            /* 
            No|Nama|Bidang Usaha|Dibuat pada|Aksi
            */
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->business_name;
            $row[] = $list->business_fields->bsfields_name;
            $row[] = date('d/m/y H:m:s A', strtotime($list->created_at));
            $row[] =  "
                <a href=\"#detailBusiness\" 
                    onclick=\"editBusiness('".$list->id_business."')\"><i class=\"icon icon-pencil\"></i></a>
                <a href=\"#deleteBusiness\" class=\"delete-row modal-basic\"
                    onclick=\"deleteBusiness('$list->id_business')\"><i class=\"icon icon-trash\"></i></a>
            ";
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function member_business_store(Request $request){
        $validate = \Validator::make( $request->all(),
            [
                'id_bsfield'        => 'required|not_in:0',
                'business_name'     => 'required|max:100',
                'brand'             => 'required|max:200',
                'desc'              => 'required',
                'address'           => 'required|max:100',
                'business_contact'  => 'required|numeric|digits_between:6,20',
                'website'           => 'required|max:200',
            ]
        );

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()]);
        }

        $member_business = new Business;
        $member_business->id_bsfield        = $request['id_bsfield'];
        $member_business->id_user           = $request['id_user'];
        $member_business->business_name     = $request['business_name'];
        $member_business->brand             = $request['brand'];
        $member_business->desc              = $request['desc'];
        $member_business->address           = $request['address'];
        $member_business->business_contact  = $request['business_contact'];
        $member_business->website           = $request['website'];
        $member_business->save();

        return response()->json(['success' => true, 'data' => $member_business]);
    }

    public function member_business_detail($id_business){
        $member_business = Business::findOrFail($id_business);
        echo json_encode($member_business);
    }

    public function member_business_update(Request $request, $id_business){
        $validate = \Validator::make( $request->all(),
            [
                'id_bsfield'        => 'required|not_in:0',
                'business_name'     => 'required|max:100',
                'brand'             => 'required|max:200',
                'desc'              => 'required',
                'address'           => 'required|max:100',
                'business_contact'  => 'required|numeric|digits_between:6,20',
                'website'           => 'required|max:200',
            ]
        );

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()]);
        }

        $member_business = Business::findOrFail($id_business);
        $member_business->id_bsfield        = $request['id_bsfield'];
        $member_business->id_user           = $request['id_user'];
        $member_business->business_name     = $request['business_name'];
        $member_business->brand             = $request['brand'];
        $member_business->desc              = $request['desc'];
        $member_business->address           = $request['address'];
        $member_business->business_contact  = $request['business_contact'];
        $member_business->website           = $request['website'];
        $member_business->update();

        return response()->json(['success' => true, 'data' => $member_business]);
    }
    
    public function member_business_destroy($id_business){
        $member_business = Business::where('id_business', '=', $id_business)->first();

        if ($member_business != null) {
            $member_business->delete();
            return response()->json(['success' => true, 'msg' => 'Berhasil menghapus data.']);
        }

        return response()->json(['success' => false, 'msg' => 'Get unknown error.']);
    }
    // -- End Module -- //

    // -- Modul: Program Anggota -- //
    public function list_member_programs($id_user){
        $member = MemberPrograms::leftjoin('programs', 'programs.id_program', '=', 'member_programs.id_program')
            ->leftjoin('program_categories', 'program_categories.id_procat', '=', 'programs.id_procat')
            ->where('member_programs.id_user', '=', $id_user)->get();
            // ->where('users.role', '!=', 99)
        $no = 0;
        $data = array();
        foreach ($member as $list) {
            /* 
            No|Nama|Kategori|Status|Aksi
            */
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->program_name;
            $row[] = $list->procat_name;
            $row[] = $list->procat_status;
            $row[] =  "
                <a href=\"#detailBusiness\" 
                    onclick=\"editProgram('$list->id_mempro')\"><i class=\"icon icon-pencil\"></i></a>
                <a href=\"#deleteBusiness\" class=\"delete-row modal-basic\"
                    onclick=\"deleteProgram('$list->id_mempro')\"><i class=\"icon icon-trash\"></i></a>
            ";
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function member_program_store(Request $request){
        $validate = \Validator::make( $request->all(),
            [
                'program' => 'required',
                'id_user' => 'required',
            ]
        );

        if ($validate->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validate->errors()]);
        }

        $member_program = new MemberPrograms;
        $member_program->id_user       = $request['id_user'];
        $member_program->id_program    = $request['program'];
        $member_program->save();
        // echo json_encode($member_program);
        return response()->json($member_program);
    }

    public function member_program_detail($id_program){
        $member_program = MemberPrograms::
        leftjoin('programs', 'programs.id_program', '=', 'member_programs.id_program')
        ->leftjoin('program_categories', 'program_categories.id_procat', '=', 'programs.id_procat')
        ->where('id_mempro', '=', $id_program)->first();
        return response()->json($member_program);
    }

    public function member_program_update(Request $request, $id_program){
        $validate = \Validator::make( $request->all(),
            [
                'program' => 'required',
                'id_user' => 'required',
            ]
        );

        if ($validate->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validate->errors()]);
        }

        $member_program = MemberPrograms::findOrFail($id_program);
        $member_program->id_user       = $request['id_user'];
        $member_program->id_program    = $request['program'];
        $member_program->update();

        return response()->json($member_program);
    }
    
    public function member_program_destroy($id_program){
        $member_program = MemberPrograms::findOrFail($id_program);
        $member_program->delete();
        if ($member_program->fails()) {
            return response()->json(['status' => 'error', 'msg' => 'error deleting data.']);
        }
    }
    // == End Module

    // -- Modul: Pengaturan Anggota -- //
    public function member_account_update(Request $request, $id_user){
        $validate = \Validator::make( $request->all(),
            ['email' => 'required']
        );

        if ($validate->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validate->errors()]);
        }

        $user = User::where('id_user', '=', $id_user)->first();
        $member = Members::where('id_user', '=', $id_user)->first();

        $user->email      = $request['email'];
        if(!empty($request['password'])){
            $user->password = bcrypt($request['password']);
        }
        
        if(Auth::user()->role == 99){
            $user->user_status = $request['member_status'];
            $member->member_status = $request['member_status'];
        }
        
        $user->update();
        $member->update();

        if(Auth::user()->role == 99){
            return redirect()->route('anggota.detail', $user->id_user);
        }else{
            return redirect()->route('profil.detail', $user->id_user);
        }
    }

    // == End Module

    // ================================================================================================
    // .start Branch : brofit
    // ================================================================================================
    public function member_profile(){
        $provinces  = \App\Models\Provinces::all();
        $field      = \App\Models\Fields::all();
        $program    = \App\Models\Programs::all();
        $procat     = \App\Models\Procat::all();
        $mempro     = \App\Models\MemberPrograms::all();
        $user = User::leftjoin('members', 'members.id_user', '=', 'users.id_user')
            ->leftjoin('loc_districts', 'loc_districts.id', '=', 'members.id')
            ->leftjoin('loc_regencies', 'loc_regencies.id', '=', 'loc_districts.regency_id')
            ->leftjoin('loc_provinces', 'loc_provinces.id', '=', 'loc_regencies.province_id')
            ->leftjoin('business', 'business.id_user', '=', 'members.id_user')
            ->leftjoin('member_programs', 'member_programs.id_user', '=', 'members.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user )
            ->first([
                'users.id_user',
                'users.fullname',
                'users.user_name',
                'users.email',
                'users.avatar',
                'users.user_status',
                'members.id_member',
                'members.member_contact',
                'members.member_status',
                'members.dateofbirth',
                'members.hometown',
                'members.address',
                'members.gender',
                'members.group',
                'members.gender',
                'loc_districts.id as id_dis',
                'loc_districts.name as district_name',
                'loc_regencies.id as id_reg',
                'loc_regencies.name as regency_name',
                'loc_provinces.id as id_pro',
                'loc_provinces.name as province_name',
            ]);
        
        $districts = \App\Models\Districts::where('regency_id', '=' , $user->id_reg)->get();
        $regencies = \App\Models\Regencies::where('province_id', '=' , $user->id_pro)->get();
        return view('member.profile', compact(
            'provinces', 
            'user', 
            'field', 
            'procat', 
            'mempro', 
            'program',
            'districts',
            'regencies'
        ));
    }

    public function changeAvatar( Request $request, $id_user){
        $validate = $this->validate($request, 
            [
                'avatar' => 'mimes:jpeg,png,bmp,tiff|max:4096',
            ],

            $messages = [
                'required' => 'The attribute field is required.',
                'mimes' => 'Only jpeg, png, bmp, tiff are allowed.'
            ]
        );


        // $user = User::find( \Auth::id() );
        $user = User::where('id_user', '=', $id_user)->first();

        // $fileName = '';

        $upload = $request->file('avatar');

        if ( $upload && $upload->isValid() ) {

            $fileName = "avatar__".$user->id_user.'__'.time().".".$upload->getClientOriginalExtension();

            // $fileName = md5( $upload->getClientOriginalName() ) . time() . '.png';

            $upload->storeAs('public/avatars', $fileName);

            if ( $user->avatar ) {
                \Storage::delete('public/avatars/' . $user->avatar );
            }
        }

        $user->avatar = $fileName;
        $user->save();

        return json_encode([
            'message' => 'success',
            'url' => asset('storage/avatars/' . $fileName)
        ]);
    }
    // ================================================================================================
    // .end Branch : brofit
    // ================================================================================================

    public function member_edit($id_user){
        $member = User::find($id_user);
        return response()->json($member);
    }

    public function provinces(){
        $provinces = Provinces::all();
        return response()->json($provinces);
    }

    public function regencies($id_province){
        $regencies = Regencies::where('province_id', '=', $id_province)->get();
        return response()->json($regencies);
    }
    
    public function districts($id_regency){
        $districts = Districts::where('regency_id', '=', $id_regency)->get();
        return response()->json($districts);
    }

    public function get_id_user() {
        $table = 'users';
        $primary = 'id_user';
        $prefix = 'U' . date('Ymd');
        $data = Autonumber::autonumber($table, $primary, $prefix);

        echo json_encode($data);
    }

    public function program_categories($id_procat){
        $program = Programs::where('id_procat', '=', $id_procat)->get();
        return response()->json($program);
    }
    
    public function tes(){
        // $data = User::leftjoin('members', 'members.id_user', '=', 'users.id_user')
        //     ->leftjoin('business', 'business.id_member', '=', 'members.id_member')
        //     ->leftjoin('business_fields', 'business_fields.id_bsfield', '=', 'business.id_bsfield')
        //     ->where('users.role', '!=', '17')->get();
        $data = User::orderby('id_user', 'asc')->where('role', '!=', 99)->get();
        // dd($data);
        return view('tes', compact('data'));
    }
}