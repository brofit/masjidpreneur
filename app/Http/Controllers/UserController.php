<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Members;
use App\Models\Provinces;
use App\Models\Regencies;
use App\Models\Districts;
use App\Helpers\Autonumber;

class UserController extends Controller
{
    // middle check login status
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        return view('module-users.user');
    }

    public function list_users(){
        $user = User::where('role', '=', 99)->get();

        // $user = User::joinleft('members', 'members.id_user', '=', 'users.id_user')
        //     ->where('role', '=', 99)->get();
        $no = 0;
        $data = array();
        foreach ($user as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->fullname;
            $row[] = $list->user_name;
            $row[] = $list->email;
            $row[] = $list->members->member_contact;
            $row[] = $list->members->gender;
            $row[] = date('d/m/Y', strtotime($list->created_at)) . ' ' . date('g:i A', strtotime($list->created_at));
            $row[] = $list->user_status;
            $row[] =  "
                <a href=\"/admin/pengguna/detail/".$list->id_user."\"><i class=\"icon icon-info\"></i></a>
                <a href=\"#deleteUser\" class=\"delete-row modal-basic\"
                    onclick=\"deleteData('".$list->id_user."')\"><i class=\"icon icon-trash\"></i></a>
            ";
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    /**
     * ADD NEW USER (ADMIN)
     */
    public function create()
    {
        $provinces = Provinces::all();
        return view('module-users.add', compact('provinces'));
    }
    
    public function getRegencies( $id )
    {
    	$regencies = Regencies::where( 'province_id', $id )->pluck( 'name', 'id' );
        return json_encode( $regencies );
    }

    public function getDistrict( $id )
    {
    	$districts = Districts::where( 'regency_id', $id )->pluck( 'name', 'id' );
        return json_encode( $districts );
    }

    public function store(Request $request)
    {
        $validate = \Validator::make( $request->all(), [
                'fullname'        => 'required|max:100',
                'user_name'       => 'required',
                'member_contact'  => 'required|numeric|digits_between:6,20',
                'dateofbirth'     => 'required',
                'email'           => 'required|email',
                'hometown'        => 'required|min:1|max:50',
                'regency'         => 'required|not_in:0',
                'address'         => 'required',
                'gender'          => 'required|min:1|max:12',
                'avatar'          => 'file|image|mimes:jpeg,png,jpg|max:2048',
                'password'        => 'min:1|min:2',
                're_password'     => 'same:password',
            ]
        );

        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'msg' => $validate->errors()]);
        }
        
        if ( $request->input('password' ) !== $request->input('re_password')) {
            return response()->json(['status' => 'failed', 'msg' => ['Password' => 'Kata sandi yang Anda masukkan tidak cocok. Silahkan ulangi lagi!']]);
        }

        $user = new User;
        $user->id_user         = $request['id_user'];
        $user->fullname        = $request['fullname'];
        $user->user_name       = $request['user_name'];
        $user->email           = $request['email'];
        $user->password        = bcrypt($request['password']);
        $user->role            = 99;
        $user->user_status     = $request['user_status'];

        if($request->hasFile('avatar')){
            $id_user = $request['id_user'];
            $file = $request->file('avatar');
            $avatar = "avatar__".$id_user.'__'.time().".".$file->getClientOriginalExtension();
            $path = storage_path('app/public/avatars/');
            $file->move($path, $avatar);
            $user->avatar = $avatar;
            $images = $avatar;
        }else{
            $images = $user->avatar;
        }

        $detail = new Members;
        $detail->id_user       = $request['id_user'];
        $detail->member_contact= $request['member_contact'];
        $detail->dateofbirth   = date('Y-m-d', strtotime(str_replace('/', '-', $request['dateofbirth'])));
        $detail->hometown      = $request['hometown'];
        $detail->id            = $request['district'];
        $detail->address       = $request['address'];
        $detail->gender        = $request['gender'];
        
        $user->save();
        echo json_encode($user);
        $detail->save();
        echo json_encode($detail);
    }

    public function get_id_user() {
        $table    = 'users';
        $primary  = 'id_user';
        $prefix   = 'U' . date('Ymd');
        $data     = Autonumber::autonumber($table, $primary, $prefix);

        echo json_encode($data);
    }

    /**
     * Setting
     */
    public function user_detail($id_user){
        $provinces = Provinces::all();
        // $user = User::leftjoin('members', 'members.id_user', '=', 'users.id_user')
        //     ->leftjoin('loc_districts', 'loc_districts.id', '=', 'members.id')
        //     ->where('users.id_user', '=', $id_user)->first();
        $user = User::leftjoin('members', 'members.id_user', '=', 'users.id_user')
            ->leftjoin('loc_districts', 'loc_districts.id', '=', 'members.id')
            ->leftjoin('loc_regencies', 'loc_regencies.id', '=', 'loc_districts.regency_id')
            ->leftjoin('loc_provinces', 'loc_provinces.id', '=', 'loc_regencies.province_id')
            ->where('users.id_user', '=', $id_user )
            ->first([
                'users.id_user',
                'users.fullname',
                'users.user_name',
                'users.email',
                'users.avatar',
                'users.user_status',
                'members.id_member',
                'members.member_contact',
                'members.member_status',
                'members.dateofbirth',
                'members.hometown',
                'members.address',
                'members.gender',
                'members.group',
                'members.gender',
                'loc_districts.id as id_dis',
                'loc_districts.name as district_name',
                'loc_regencies.id as id_reg',
                'loc_regencies.name as regency_name',
                'loc_provinces.id as id_pro',
                'loc_provinces.name as province_name',
            ]);
        $districts = Districts::where('regency_id', '=' , $user->id_reg)->get();
        $regencies = Regencies::where('province_id', '=' , $user->id_pro)->get();

        return view('module-users.detail', compact('user','provinces', 'regencies', 'districts'));
    }

    public function user_update(Request $request, $id_user){
        $validate = \Validator::make( $request->all(), [
                'fullname'      => 'required|max:100',
                'member_contact'=> 'required|numeric|digits_between:6,20',
                'dateofbirth'   => 'required',
                'hometown'      => 'required|min:1|max:50',
                'regency'       => 'required|not_in:0',
                'address'       => 'required',
                'gender'        => 'required|min:1|max:12',
                'email'         => 'required|email',
                'avatar'        => 'file|image|mimes:jpeg,png,jpg|max:2048',
                'user_status'   => 'required',
            ]
        );

        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'msg' => $validate->errors()]);
        }

        $user = User::where('id_user', '=', $id_user)->first();
        $user->fullname        = $request['fullname'];
        $user->user_name       = $request['user_name'];
        $user->email           = $request['email'];

        if(!empty($request['password'])){
            $user->password = bcrypt($request['password']);
        }
        
        // $user->role            = $request['role'];
        $user->user_status     = $request['user_status'];
        if($request->hasFile('avatar')){
            $id_user = $request['id_user'];
            $file = $request->file('avatar');
            $avatar = "avatar__".$id_user.'__'.time().".".$file->getClientOriginalExtension();
            $path = storage_path('app/public/avatars/');
            $file->move($path, $avatar);
            $user->avatar = $avatar;
            $images = $avatar;
        }else{
            $images = $user->avatar;
        }
        $user->update();
        echo json_encode($user);

        $detail = Members::where('id_user', '=', $id_user)->first();
        $detail->member_contact= $request['member_contact'];
        $detail->dateofbirth   = date('Y-m-d', strtotime(str_replace('/', '-', $request['dateofbirth'])));
        $detail->hometown      = $request['hometown'];
        $detail->id            = $request['district'];
        $detail->address       = $request['address'];
        $detail->gender        = $request['gender'];
        $detail->update();
        echo json_encode($detail);
    }

    public function user_destroy($id_user){
        $member = User::where('id_user', '=', $id_user)->first();
        $member->delete();
        return redirect()->route('semua-pengguna');
    }

    /**
     * Setting
     */
    public function setting()
    {
        return view('module-users.setting');
    }

    public function test(){
        $user = User::all();
        return view('tes', compact('user'));
    }

}
