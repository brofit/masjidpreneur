<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/semua-pengguna';
    
    public function redirectTo(){
        if(Auth::user()->role == 99){
            return route('semua-anggota');
        }elseif(Auth::user()->role == 1){
            return route('member-profile');
        }
    }

    protected function credentials(Request $request){        
        return [
            'email' => $request->{$this->username()}, 
            'password' => $request->password, 
            'user_status' => 'Aktif'
        ];
    }

    protected function sendFailedLoginResponse(Request $request){
        $errors = [$this->username() => trans('auth.failed')];

        // Load user from database
        $user = \App\Models\User::where($this->username(), $request->{$this->username()})->first();

        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->active != 'Aktif') {
            $errors = [$this->username() => trans('auth.notactivated')];
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function __construct(){
        $this->middleware('guest')->except('logout');
    }
}
