<?php

namespace App\Http\Controllers;

use App\Models\Programs;
use App\Models\Procat;
use Illuminate\Http\Request;
use App\Helpers\Autonumber;
use File;

class ProgramController extends Controller
{
    // middle check login status
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        $procat = Procat::all();
        return view('module-program.program', compact('procat'));
    }

    public function add_program(){
        $procat = Procat::all();
        return view('module-program.add', compact('procat'));
    }

    public function program_category(){
        return view('module-program.program-categories');
    }
    
    /**
     * Fungsi list data
     */
    public function list_programs(){
        $program = Programs::leftjoin('program_categories', 'program_categories.id_procat', '=', 'programs.id_procat')
            ->orderby('id_program', 'asc')->get();
        $no = 0;
        $data = array();
        foreach ($program as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->program_name;
            $row[] = $list->procat_name;
            $row[] = substr($list->desc, 0, 50) . ' ...';
            $row[] = '<a href="'.asset('storage/images/'. $list->attachment).'" target="_blank"><i class="icon icon-paper-clip"></i></a>';
            $row[] = $list->program_status;
            $row[] = $list->due_date . ' ' . date('G:i A', strtotime($list->due_time));
            $row[] = "
                    <a href=\"#editProgram\" class=\"modal-basic\"
                        onclick=\"editData('$list->id_program')\"><i class=\"icon icon-info\"></i></a>
                    <a href=\"#deleteProgram\" class=\"delete-modal modal-basic\"
                        onclick=\"deleteData('$list->id_program')\"><i class=\"icon icon-trash\"></i>
                    </a>
                ";
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }
    
    public function program_store(Request $request)
    {
        $validate = \Validator::make( $request->all(), [
                'program_category'  => 'required',
                'program_name'      => 'required|max:100',
                'description'       => 'required',
                'due_date_register' => 'required|date_format:d/m/Y',
                'due_time_register' => 'required',
                'status_program'    => 'required',
                'attachment'        => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            ]
        );

        if ($validate->fails()) {
            return redirect()
                ->route('tambah-program')
                ->withErrors($validate)
                ->withInput();
        }

        $program = new Programs;
        $program->id_procat         = $request['program_category'];
        $program->program_name      = $request['program_name'];
        $program->desc              = $request['description'];
        $date = date('Y-m-d', strtotime(str_replace('/', '-', $request['due_date_register'])));
        $time = date('H:i:m', strtotime($request['due_time_register']));
        $program->due_date          = $date;
        $program->due_time          = $time;
        $program->program_status    = $request['status_program'];

        if($request->hasFile('attachment')){
            $file = $request->file('attachment');
            $attachment_name = "attachment__".time().".".$file->getClientOriginalExtension();
            $path = storage_path('app/public/images/');

            $file->move($path, $attachment_name);
            $program->attachment = $attachment_name;
            $img_data = $attachment_name;
        }else{
            $img_data = $program->attachment;
        }
        
        $program->save();
        
        return redirect()
            ->route('semua-program')
            ->with([
                'message' => 'Data Berhasil disimpan.',
                'status' => 'Berhasil!'
            ]);
    }

    public function program_edit($id_program){
        $program = Programs::find($id_program);
        echo json_encode($program);

    }

    public function program_update(Request $request, $id_program){
        $validate = \Validator::make( $request->all(), [
                'program_category'  => 'required',
                'program_name'      => 'required|max:100',
                'description'       => 'required',
                // 'due_date_register' => 'required|date_format:d/m/Y',
                'due_time_register' => 'required',
                'status_program'    => 'required',
                'attachment'        => 'file|image|mimes:jpeg,png,jpg|max:2048',
            ]
        );

        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'msg' => $validate->errors()]);
        }

        $program = Programs::find($id_program);
        $program->id_procat         = $request['program_category'];
        $program->program_name      = $request['program_name'];
        $program->desc              = $request['description'];
        $date = date('Y-m-d', strtotime(str_replace('/', '-', $request['due_date_register'])));
        $time = date('H:i:m', strtotime($request['due_time_register']));
        $program->due_date          = $date;
        $program->due_time          = $time;
        $program->program_status    = $request['status_program'];

        if($request->hasFile('attachment')){
            $file = $request->file('attachment');
            $attachment_name = "attachment__".time().".".$file->getClientOriginalExtension();
            $path = storage_path('app/public/images/');
            File::delete(storage_path('app/public/images/' .$program->attachment));
            $file->move($path, $attachment_name);
            $program->attachment = $attachment_name;
            $img_data = $attachment_name;
        }else{
            $img_data = $program->attachment;
        }
        
        $program->update();
        // echo json_encode($program);
        return redirect()->route('semua-program')->with([
            'message' => 'Data Berhasil diperbarui.',
            'status' => 'Berhasil!'
        ]);
    }

    public function program_destroy($id_program){
        $program = Programs::find($id_program);
        File::delete(storage_path('app/public/images/' .$program->attachment));
        $program->delete();
        return redirect()->route('semua-program')->with([
            'message' => 'Data Berhasil dihapus.',
            'status' => 'Berhasil!'
        ]);
    }

    // ------------------------------ // 
    // == Control Kategori Program == //
    // ------------------------------ // 

    /**
     * Fungsi list data
     */
    public function list_program_categories(){
        $procat = Procat::orderBy('id_procat', 'asc')->get();
        $no = 0;
        $data = array();
        foreach ($procat as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->procat_name;
            $row[] = $list->procat_status;
            $row[] = date('d/m/Y H:m:s A', strtotime($list->created_at));
            $row[] = "
                <a href=\"#editProgramCategory\" class=\"edit-modal modal-basic\"
                    onclick=\"editData('$list->id_procat')\"><i class=\"icon icon-pencil\"></i></a>
                <a href=\"#deleteProgramCategory\" class=\"delete-modal modal-basic\"
                    onclick=\"deleteData('$list->id_procat')\"><i class=\"icon icon-trash\"></i>
                </a>
            ";
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function program_category_store(Request $request){
        
        $this->validate($request,[
            'procat_name'=>'required',
        ]);

        $procat = new Procat;
        $procat->id_procat = $request['id_procat'];
        $procat->procat_name = $request['procat_name'];
        $procat->procat_status = $request['procat_status'];
        $procat->save();
        return redirect()->route('kategori-program');
    }

    public function program_category_edit($id_procat){
        $procat = Procat::find($id_procat);
        echo json_encode($procat);
    }

    public function program_category_destroy($id_procat){
        $procat = Procat::find($id_procat);
        $procat->delete();
        return redirect()->route('kategori-program');
    }

    public function program_category_update(Request $request, $id_procat){
        $procat = Procat::find($id_procat);
        $procat->id_procat = $request['id_procat'];
        $procat->procat_name = $request['procat_name'];
        $procat->procat_status = $request['procat_status'];
        $procat->update();
        return redirect()->route('kategori-program');
    }

    public function get_number() {
        $table = 'program';
        $primary = 'id_program';
        $prefix = '#P' . date('Ymd');
        $data = Autonumber::autonumber($table, $primary, $prefix);

        echo json_encode($data);
    }

}
