<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth as Auth;

class CheckUser
{
    public function handle($request, Closure $next, $role){
        $user = Auth::user();
        if($user && $user->role != $role){
            return redirect('/');
        }else return $next($request);
    }

}
