<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberPrograms extends Model
{
    protected $table = 'member_programs';
    protected $primaryKey = 'id_mempro';

    public function users(){
        return $this->belongsTo('App\Models\User');
    }

    public function programs(){
        return $this->hasMany('App\Models\Programs', 'id_program', 'id_program');
    }
}
