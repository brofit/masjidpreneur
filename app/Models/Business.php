<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $table = 'business';
    protected $primaryKey = 'id_business';

    public function users(){
        return $this->belongsTo('App\Models\User');
    }

    public function business_fields(){
        return $this->hasOne('App\Models\Fields', 'id_bsfield', 'id_bsfield');
    }
}
