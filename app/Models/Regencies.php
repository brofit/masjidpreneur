<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regencies extends Model
{
    // kabupaten
    protected $table = 'loc_regencies';

    public function district(){
        return $this->hasMany('App\Models\Districts');
    }

    public function provincy(){
        return $this->belongsTo('App\Models\Provinces');
    }
    
}
