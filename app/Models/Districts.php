<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    // kecamatan
    protected $table = 'loc_districts';

    public function members(){
        return $this->hasMany('App\Models\Members', 'id_user');
    }

    public function regency(){
        return $this->belongsTo('App\Models\Regencies');
    }

}
