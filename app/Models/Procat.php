<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Procat extends Model
{
    protected $table = 'program_categories';
    protected $primaryKey = 'id_procat';

}
