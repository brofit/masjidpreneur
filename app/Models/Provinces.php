<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    // provinsi
    protected $table = 'loc_provinces';

    public function regency(){
        $this->hasMany('App\Models\Regencies');
    }
}
