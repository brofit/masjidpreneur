<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Programs extends Model
{
    protected $table = 'programs';
    protected $primaryKey = 'id_program';

    public function member_programs(){
        return $this->belongsTo('App\Models\MemberProgram');
    }

    public function users(){
        return $this->belongsTo('App\Models\User');
    }
}
