<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fields extends Model
{
    protected $table = 'business_fields';
    protected $primaryKey = 'id_bsfield';

    public function business(){
        return $this->belongsTo('App\Models\Business');
    }
}
