<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function province()
    {
        $data = Provinces::find($this->provinces);
        if ( $data ) {
            return $data->name;
        }
        return '';
    }

    public function regency()
    {
        $data = Regencies::find($this->regencies);
        if ( $data ) {
            return $data->name;
        }
        return '';
    }

    public function district()
    {
        $data = Districts::find($this->districts);
        if ( $data ) {
            return $data->name;
        }
        return '';
    }
    
    public function members(){
        return $this->hasOne('App\Models\Members', 'id_user', 'id_user');
    }

    public function business(){
        return $this->hasMany('App\Models\Business', 'id_user', 'id_user');
    }

    public function member_programs(){
        return $this->hasMany('App\Models\MemberPrograms', 'id_user', 'id_user');
    }

    public function programs(){
        return $this->hasMany('App\Models\Programs', 'id_program', 'id_program');
    }
}
