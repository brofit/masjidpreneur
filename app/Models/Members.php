<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    protected $table = 'members';
    protected $primaryKey = 'id_member';

    protected $fillable = [
        'member_contact', 'status', 'dateofbirth', 'hometown', 'id', 'address', 'gender', 'group',
    ];

    public function users(){
        return $this->belongsTo('App\Models\User');
    }
    
    public function districts(){
        return $this->belongsTo('App\Models\Districts');
    }
}