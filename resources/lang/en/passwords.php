<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    *

    'password' => 'Passwords must be at least eight characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",

    */

    'password'  => 'Passwords minimal harus 8 karakter dan sesuai dengan konfirmasi password.',
    'reset'     => 'Anda telah berhasil memulihkan akun dengan kata sandi yang baru saja dibuat.',
    'sent'      => 'Tautan reset password telah dikirim ke email Anda!',
    'token'     => 'Token reset password tidak valid.',
    'user'      => "Pengguna dengan email tersebut tidak ditemukan.",

];
