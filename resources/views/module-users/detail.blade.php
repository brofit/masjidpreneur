@extends('layouts.app')
@section('title', 'Detail Pengguna')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}">
@endsection
@section('content')
    <header class="page-header">
        <h2>{{ __('Tambah Pengguna') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="row">
        <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">
            <section class="card">
                <div class="card-body">
                    <div class="thumb-info">
                        @if(Auth::check())
							<img src="{{ asset('storage/avatars/' . $user->avatar ) }}" class="rounded img-fluid change-avatar" alt="Photo Profile">
						@else
							<img src="{{ asset('assets/images/!logged-user-lg.jpg') }}" class="rounded change-avatar" alt="avatar" width="100%" height="100%">
						@endif
                    </div>
                </div>
            </section>
        </div>
        <div class="col-lg-8 col-xl-9">
            <section class="card">
                <div class="card-body">
                    <form action="/admin/pengguna/update/{{ $user->id_user }}" 
                        id="form-content" 
                        method="POST" 
                        enctype="multipart/form-data" 
                        class="form">
                        @csrf @method('PATCH')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="fullname"><strong>{{ __('Nama lengkap') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="fullname" 
                                        id="fullname" 
                                        type="text" 
                                        class="form-control" 
                                        value="{{ $user->fullname }}" 
                                        tabindex="1"
                                        required>
                                        @if($errors->has('fullname'))
                                        <div class="text-danger">
                                            {{ $errors->first('fullname')}}
                                        </div>
                                        @endif
                                    <input type="hidden" 
                                        name="id_user" 
                                        id="id_user" 
                                        value="{{ $user->members->id_user }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="member_contact"><strong>{{ __('Nomor telpon') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="member_contact" 
                                        id="member_contact" 
                                        type="text" 
                                        class="form-control" 
                                        value="{{ $user->members->member_contact }}" 
                                        tabindex="2"
                                        required>
                                        @if($errors->has('member_contact'))
                                        <div class="text-danger">
                                            {{ $errors->first('member_contact')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="dateofbirth"><strong>{{ __('Tanggal lahir') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="dateofbirth" 
                                        id="dateofbirth" 
                                        type="text" 
                                        class="form-control" 
                                        value="{{ date('d/m/Y', strtotime($user->members->dateofbirth)) }}" 
                                        tabindex="3" 
                                        data-plugin-datepicker
                                        required>
                                        @if($errors->has('dateofbirth'))
                                        <div class="text-danger">
                                            {{ $errors->first('dateofbirth')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="hometown"><strong>{{ __('Kota kelahiran') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="text" 
                                        class="form-control" 
                                        name="hometown" 
                                        id="hometown" 
                                        value="{{ $user->members->hometown }}" 
                                        tabindex="4
                                        required">
                                        @if($errors->has('hometown'))
                                        <div class="text-danger">
                                            {{ $errors->first('hometown')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="province"><strong>{{ __('Provinsi') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select class="form-control" 
                                        name="province" 
                                        id="province" 
                                        tabindex="5" 
                                        data-plugin-selectTwo
                                        required>
                                        <option selected disabled>{{ __('Pilih provinsi') }}</option>
                                        @foreach($provinces as $prov)
                                            <option value="{{ $prov->id }}" @if($prov->id == $user->id_pro) selected @endif>{{ $prov->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('province'))
                                    <div class="text-danger">
                                        {{ $errors->first('province')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="regency"><strong>{{ __('Kabupaten') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select class="form-control" 
                                        name="regency" 
                                        id="regency" 
                                        tabindex="6" 
                                        data-plugin-selectTwo
                                        required>
                                        <option selected disabled>{{ __('Pilih kabupaten') }}</option>
                                        @foreach($regencies as $reg)
                                        <option value="{{ $reg->id }}" @if($reg->id == $user->id_reg) selected @endif>{{ $reg->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('regency'))
                                    <div class="text-danger">
                                        {{ $errors->first('regency')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="district"><strong>{{ __('Kecamatan') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select class="form-control" 
                                        name="district" 
                                        id="district" 
                                        tabindex="7" 
                                        data-plugin-selectTwo
                                        required>
                                        <option selected disabled>{{ __('Pilih kecamatan') }}</option>
                                        @foreach($districts as $dis)
                                        <option value="{{ $dis->id }}" @if($dis->id == $user->id_dis) selected @endif>{{ $dis->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('district'))
                                    <div class="text-danger">
                                        {{ $errors->first('district')}}
                                    </div>
                                     @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="address"><strong>{{ __('Alamat') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input class="form-control" 
                                        name="address" 
                                        id="address" 
                                        value="{{ $user->members->address }}" 
                                        tabindex="8"
                                        required>
                                        <p class="help-block">{{ __('Alamat bisa berupa jalan, perumahan, apartemen, lantai ke... ') }}</p>
                                        @if($errors->has('address'))
                                        <div class="text-danger">
                                            {{ $errors->first('address')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="gender"><strong>{{ __('Jenis kelamin') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="radio-custom radio-primary form-check-inline mr-5">
                                                <input type="radio" 
                                                    id="male" 
                                                    name="gender" 
                                                    value="Laki - Laki" 
                                                    tabindex="9" 
                                                    @if($user->members->gender == "Laki - Laki") checked @endif
                                                    required>
                                                    <label for="male">{{ __('Laki - Laki') }}</label>
                                            </div>
                                            <div class="radio-custom radio-primary form-check-inline">
                                                <input type="radio" 
                                                    id="female" 
                                                    name="gender" 
                                                    value="Perempuan"
                                                    @if($user->members->gender == "Perempuan") checked @endif
                                                    required>
                                                    <label for="female">{{ __('Perempuan') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="avatar"><strong>{{ __('Foto profil') }}</strong> {{ __('(optional)') }}</label>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="fileupload fileupload-exists" data-provides="fileupload">
                                                <div class="input-append">
                                                    <div class="uneditable-input">
                                                        <i class="fa fa-file fileupload-exists"></i>
                                                        <span class="fileupload-preview">{{ $user->avatar }}</span>
                                                    </div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileupload-exists">Ganti foto</span>
                                                        <span class="fileupload-new" tabindex="10">Unggah foto</span>
                                                        <input type="file" 
                                                            name="avatar" 
                                                            id="avatar"/>
                                                    </span>
                                                    <a href="#" 
                                                        class="btn btn-default fileupload-exists" 
                                                        data-dismiss="fileupload">
                                                        Hapus</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr class="divider mb-1">
                        <p class="m-0"><em><i class="icon icon-user"></i> Data akun dan autentifikasi pengguna</em></p>
                        <hr class="divider mt-1">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="email"><strong>{{ __('Email') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="email" 
                                        class="form-control" 
                                        name="email" 
                                        id="email" 
                                        value="{{ $user->email }}" 
                                        tabindex="11"
                                        required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="user_name"><strong>{{ __('Nama pengguna') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="text" 
                                        class="form-control" 
                                        name="user_name" 
                                        id="user_name" 
                                        value="{{ $user->user_name }}" 
                                        tabindex="12"
                                        required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="password"><strong>{{ __('Kata sandi baru') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="password" 
                                        class="form-control" 
                                        name="password" 
                                        id="password"
                                        value="" 
                                        tabindex="13"
                                        required>
                                        <p class="help-block">{{ __('Biarkan tetap kosong jika tidak ingin mengganti') }}</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="re_password"><strong>{{ __('Tulis ulang') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="password" 
                                        class="form-control" 
                                        name="re_password" 
                                        id="re_password" 
                                        tabindex="14"
                                        required>
                                        <p class="help-block">{{ __('Biarkan tetap kosong jika tidak ingin mengganti') }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="user_status"><strong>{{ __('Status pengguna') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="radio-custom radio-primary form-check-inline mr-5">
                                                <input type="radio" 
                                                    id="active" 
                                                    name="user_status"
                                                    value="Aktif" 
                                                    checked 
                                                    tabindex="15" 
                                                    @if($user->user_status == "Aktif") checked @endif
                                                    required>
                                                    <label for="active">{{ __('Aktif') }}</label>
                                            </div>
                                            <div class="radio-custom radio-primary form-check-inline">
                                                <input type="radio" 
                                                    id="deactive" 
                                                    name="user_status" 
                                                    value="Tidak Aktif" 
                                                    @if($user->user_status == "Tidak Aktif") checked @endif 
                                                    required>
                                                    <label for="deactive">{{ __('Tidak Aktif') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right mt-3">
                            <a href="#" class="btn btn-default go-back"><i class="icon icon-arrow-left-circle"></i> {{ __('Kembali') }}</a>
                            <button type="button" class="btn btn-primary update-data" tabindex="16"><i class="icon icon-doc"></i> {{ __('Simpan') }}</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
@endsection
@section('blockfoot')
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
    <script>
        $(function () {
            /*
            Get Regency 
            */
            $('select[name="province"]').on('change', function () {
                var RegencyID = $(this).val();
                if ( RegencyID ) {
                    $.ajax({
                        url: "{{ url('/admin/pengguna/getRegencies') }}/" + RegencyID,
                        type: "GET",
                        dataType: "json",
                        beforeSend: function () {
                            $('#loader').css("visibility", "visible");
                        },

                        success:function ( res ) {               
                            if ( res ) {
                                $('select[name="regency"]').empty();
                                $('select[name="regency"]').append('<option selected disabled>Pilih kabupaten</option>');
                                $.each( res, function ( key, value ) {
                                    $('select[name="regency"]').append('<option value="'+key+'">'+value+'</option>');
                                });
                                $('select[name="district"]').empty();
                                $('select[name="district"]').append('<option selected disabled>Pilih kecamatan</option>');
                            } else {
                                $('select[name="regency"]').empty();
                            }
                        }
                    });
                } else {
                    $('select[name="regency"]').empty();
                }
            });

            /*
            Get Districts 
            */
            $('select[name="regency"]').on('change', function () {
                var DistrictID = $(this).val();
                if ( DistrictID ) {
                    $.ajax({
                        url: "{{ url('/admin/pengguna/getDistricts') }}/" + DistrictID,
                        type: "GET",
                        dataType: "json",
                        beforeSend: function () {
                            $('#loader').css("visibility", "visible");
                        },

                        success:function ( res ) {               
                            if ( res ) {
                                $('select[name="district"]').empty();
                                $('select[name="district"]').append('<option disabled>Pilih kecamatan</option>');
                                $.each( res, function ( key, value ) {
                                    $('select[name="district"]').append('<option value="'+key+'">'+value+'</option>');
                                });
                            } else {
                                $('select[name="district"]').empty();
                            }
                        }
                    });
                } else {
                    $('select[name="district"]').empty();
                }
            });

            /*
            Submit Data
            */
            $('.update-data').click(function(e) {
		        var $this = $(this);
                var data  = $(this).parents("form").serialize();
                if(!e.isDefaultPrevented()){
                    $.ajax({
                        // url: "{{ route('update.pengguna', ':id_user') }}",
                        url: '/admin/pengguna/update/{{ $user->id_user }}',
                        type: "POST",
                        data: new FormData($(".form")[0]),
                        async: true,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success: function( data ) { 
                            var $error = '';
                            $.each(data.msg, function(key, value) {
                                $error += '- ' +value + '<br/>';
                            });

                            new PNotify({
                                title : 'Gagal..',
                                text  : $error,
                                type  : 'error'
                            });

                        },
                        error: function (data) {
                            new PNotify({
                                title : 'Berhasil..',
                                text  : 'Data pengguna telah berhasil diperbarui. Apakah ada perubahan lagi?',
                                type  : 'success'
                            });
                            setTimeout(function(){
                                window.location.href = '{{ route("detail.pengguna", $user->id_user) }}'
                            }, 2000)
                        }
                    });
                }
            });

            /*
            Navigation Active
            */
            $('.user').addClass('nav-expanded nav-active');
            $('.all-user').addClass('nav-active');

            /*
            GO PREVIOUS PAGE
            */
            $(document).ready(function() {
                $('a.go-back').click(function () {
                    parent.history.back();
                    return false;
                });
            });
        });
    </script>
@endsection