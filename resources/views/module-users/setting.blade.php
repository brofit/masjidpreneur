@extends('layouts.app')
@section('title', 'Pengaturan')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/cropper/cropper.min.css') }}" />
@endsection
@section('content')
    <header class="page-header">
        <h2>{{ __('Pengaturan') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="row">
        <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">
            <section class="card">
                <div class="card-body">
                    <div class="avatar-area">   
                        <label class="label mb-0" data-toggle="tooltip" title="Ganti foto profil">
                            @if(Auth::user()->avatar)
                                <img src="{{ asset('storage/avatars/' . Auth::user()->avatar ) }}" class="rounded img-fluid change-avatar" alt="Photo Profile">
                            @else
                                <img src="{{ asset('assets/images/!logged-user-lg.jpg') }}" class="rounded change-avatar" alt="avatar" width="100%" height="100%">
                            @endif
                            <input type="file" class="sr-only" id="input" name="avatar" accept="image/*">
                            <span class="btn btn-primary btn-change-avatar btn-file"><i class="icon icon-pencil"></i></span>
                        </label>
                        <div class="progress mt-2 mb-0">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">{{ __('0%') }}</div>
                        </div>
                        <div class="alert mt-2 mb-0" role="alert"></div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-lg-8 col-xl-9">
            <section class="card">
                <div class="card-body">
                    <form action="" method="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="full_name"><strong>{{ __('Nama lengkap') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="text" class="form-control" name="full_name" id="full_name" tabindex="1">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="username"><strong>{{ __('Nama pengguna') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="text" class="form-control" name="username" id="username" tabindex="2">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="email"><strong>{{ __('Alamat email') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="email" class="form-control" name="email" id="email" tabindex="3">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="phone_number"><strong>{{ __('Nomor telpon') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="text" class="form-control" name="phone_number" id="phone_number" tabindex="4">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="birth_date"><strong>{{ __('Tanggal lahir') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="text" class="form-control" name="birth_date" id="birth_date" tabindex="5" data-plugin-datepicker>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="birth_city"><strong>{{ __('Kota lahir') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="text" class="form-control" name="birth_city" id="birth_city" tabindex="6">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="loc_province"><strong>{{ __('Provinsi') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select class="form-control" name="loc_province" id="loc_province" tabindex="7" data-plugin-selectTwo>
                                        <option>{{ __('Pilih provinsi') }}</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="loc_districts"><strong>{{ __('Kabupaten') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select class="form-control" name="loc_districts" id="loc_districts" tabindex="8" data-plugin-selectTwo>
                                        <option>{{ __('Pilih kabupaten') }}</option>
                                        <option value="Karanganyar">Karanganyar</option>
                                        <option value="Karanganyar">Karanganyar</option>
                                        <option value="Karanganyar">Karanganyar</option>
                                        <option value="Karanganyar">Karanganyar</option>
                                        <option value="Karanganyar">Karanganyar</option>
                                        <option value="Karanganyar">Karanganyar</option>
                                        <option value="Karanganyar">Karanganyar</option>
                                        <option value="Karanganyar">Karanganyar</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="loc_sub_districts"><strong>{{ __('Kecamatan') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select class="form-control" name="loc_sub_districts" id="loc_sub_districts" tabindex="9" data-plugin-selectTwo>
                                        <option>{{ __('Pilih kecamatan') }}</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                        <option value="Mojogedang">Mojogedang</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="address"><strong>{{ __('Alamat') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="text" class="form-control" name="address" id="address" tabindex="10">
                                    <p class="help-block">{{ __('Alamat bisa berupa jalan, perumahan, apartemen lantai ke...') }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="sex"><strong>{{ __('Jenis kelamin') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="radio-custom radio-primary form-check-inline mr-5">
                                                <input type="radio" id="sex_1" name="sex" checked tabindex="11">
                                                <label for="sex_1">{{ __('Laki - Laki') }}</label>
                                            </div>
                                            <div class="radio-custom radio-primary form-check-inline">
                                                <input type="radio" id="sex_2" name="sex">
                                                <label for="sex_2">{{ __('Perempuan') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right mt-3">
                            <a href="#" class="btn btn-default go-back"><i class="icon icon-arrow-left-circle"></i> {{ __('Kembali') }}</a>
                            <button type="submit" class="btn btn-primary" tabindex="13"><i class="icon icon-doc"></i> {{ __('Simpan') }}</button>
                        </div>
                    </form>
                </div>
            </section>

            <section class="card">
                <div class="card-header">{{ __('Ubah kata sandi') }}</div>
                <div class="card-body">
                    <form action="" method="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="password"><strong>{{ __('Kata sandi baru') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="password" class="form-control" name="password" id="password" tabindex="14">
                                    <p class="help-block">{{ __('Biarkan tetap kosong jika tidak ingin mengganti') }}</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="re-password"><strong>{{ __('Tulis ulang') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="password" class="form-control" name="re-password" id="re-password" tabindex="15">
                                    <p class="help-block">{{ __('Biarkan tetap kosong jika tidak ingin mengganti') }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="text-right mt-3">
                            <a href="#" class="btn btn-default go-back"><i class="icon icon-arrow-left-circle"></i> {{ __('Kembali') }}</a>
                            <button type="submit" class="btn btn-primary" tabindex="16"><i class="icon icon-doc"></i> {{ __('Simpan') }}</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>

    <!-- MODAL CHANGE PHOTO -->
    <div class="modal modal-round modal-avatar fade" id="change-photo" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="img-container">
                        <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="crop">{{ __('Crop Photo') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">{{ __('Batal') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END : MODAL CHANGE PHOTO -->
@endsection
@section('blockfoot')
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('vendor/cropper/cropper.min.js') }}"></script>
    <script>
        $(function () {
            /*
            GO PREVIOUS PAGE
             */
            $(document).ready(function() {
                $('a.go-back').click(function () {
                    parent.history.back();
                    return false;
                });
            });
        });

        /*
        CHANGE ADMIN PHOTO
        */
        window.addEventListener('DOMContentLoaded', function () {
            var avatar = document.getElementById('avatar');
            var image = document.getElementById('image');
            var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#change-photo');
            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function (e) {
                var files = e.target.files;
                var done = function (url) {
                    input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    aspectRatio: 1,
                    viewMode: 3,
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function () {
                var initialAvatarURL;
                var canvas;

                $modal.modal('hide');

                if ( cropper ) {
                    canvas = cropper.getCroppedCanvas({
                        width: 400,
                        height: 400,
                    });

                    canvas.toBlob(function (blob) {
                        var formData = new FormData();

                        formData.append('avatar', blob);

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax("{{ route('change_avatar_admin', ['id' => Auth::id()]) }}", {
                            type: 'POST',
                            data: formData,
                            processData: false,
                            contentType: false,
                            xhr: function () {
                                var xhr = new XMLHttpRequest();

                                xhr.upload.onprogress = function (e) {
                                    var percent = '0';
                                    var percentage = '0%';

                                    if (e.lengthComputable) {
                                        percent = Math.round((e.loaded / e.total) * 100);
                                        percentage = percent + '%';
                                        $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                                    }
                                };

                                return xhr;
                            },

                            success: function (response) {
                                var json = $.parseJSON( response );
                                $('.change-avatar').attr('src', json.url);
                                $alert.show().addClass('alert-success').text('Upload success');
                            },

                            error: function () {
                                avatar.src = initialAvatarURL;
                                $alert.show().addClass('alert-warning').text('Upload error');
                            },

                            complete: function () {
                                $progress.hide();
                            },
                        });
                    });
                }
            });
        });
    </script>
@endsection