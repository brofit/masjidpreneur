<header class="header">
    <div class="logo-container">
        <a href="{{ asset('/') }}" class="logo">
            <img src="{{ asset('assets/images/favicon_62x62.png') }}" height="auto" alt="Reminder App" />
        </a>
        <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="header-right">
        <!-- {{-- USER BOX --}} -->
        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    @if(Auth::check())
                        <img 
                            src="{{ Storage::url('avatars' .'/'. Auth::user()->avatar) }}" 
                            alt="Photo Profile" class="rounded-circle change-avatar" 
                            data-lock-picture="{{ Auth::user()->avatar }}"/>
                    @else
                        <img 
                            src="{{ asset('assets/images/!logged-user.jpg') }}" 
                            alt="Photo Profile" 
                            class="rounded-circle change-avatar" 
                            data-lock-picture="{{ asset('images/!logged-user.jpg') }}" />
                    @endif
                </figure>
                <div class="profile-info">
                    <span class="name">@if (Auth::check()) {{ Auth::user()->fullname }} @else Guest @endif</span>
                    @if(Auth::check() && Auth::user()->role == 99)
                    <span class="role">Administrator</span>
                    @else
                    <span class="role">Member</span>
					@endif
                </div>
                <i class="icon icon-arrow-down custom-arrow-down"></i>
            </a>
            <div class="dropdown-menu">
                <ul class="list-unstyled mb-2">
                    <li class="divider"></li>
                    @if(Auth::check() && Auth::user()->role == 99)
                    <li>
                        <a role="menuitem" 
                            tabindex="-1" 
                            href="/admin/pengguna/detail/{{ Auth::user()->id_user }}">
                            <i class="icon icon-settings"></i>
                            Pengaturan</a>
                    </li>
                    @else
                    <li>
                        <a role="menuitem" 
                            tabindex="-1" 
                            href="/member/profil">
                            <i class="icon icon-settings"></i> 
                            Pengaturan</a>
                    </li>
					@endif
                    <li>
                        <a role="menuitem" 
                            tabindex="-1" 
                            href="{{ route('logout') }}"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="icon icon-logout"></i>
                            Keluar</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>