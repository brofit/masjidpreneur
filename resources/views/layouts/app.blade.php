<!doctype html>
<html class="fixed sidebar-light sidebar-left-sm clean-design" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>

		<meta charset="UTF-8">
		<title>MasjidPreneur - @yield('title')</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="keywords" content="ERP System" />
		<meta name="description" content="ERP System - Tebar System Development">
		<meta name="author" content="Tebar.co.id">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<link rel="shortcut icon" href="{{ asset('assets/images/favicon_62x62.png') }}" type="image/x-icon" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" />
   		<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/animate/animate.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/pnotify/pnotify.custom.css') }}" />
		@yield('blockhead')

		<!-- Assets CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/theme.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/skins/default.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">

		<!-- Head Libs -->
		<script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>

	</head>
	<body>

        <section class="body">

            {{-- HEADER --}}
            @include('layouts.header')

            {{-- WRAPPER --}}
            <div class="inner-wrapper">

                {{-- SIDEBAR LEFT --}}
                @include('layouts.sidebar_left')

                {{-- CONTENTS --}}
                <section role="main" class="content-body">
                    
                    {{-- DYNAMIC CONTENT --}}
                    @yield('content')
                </section>
            </div>

            {{-- SIDEBAR RIGHT --}}
            @include('layouts.sidebar_right')
        </section>

        <!-- Vendor JS -->
		<script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
		<script src="{{ asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
		<script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('vendor/common/common.js') }}"></script>
        <script src="{{ asset('vendor/nanoscroller/nanoscroller.js') }}"></script>
        <script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.js') }}"></script>
        <script src="{{ asset('vendor/nprogress/nprogress.js') }}"></script>
        <script src="{{ asset('vendor/pnotify/pnotify.custom.js') }}"></script>
		@yield('blockfoot')
		
		<!-- Assets JS -->
		<script src="{{ asset('assets/js/theme.js') }}"></script>
		<script src="{{ asset('assets/js/app.js') }}"></script>
		<script src="{{ asset('assets/js/theme.init.js') }}"></script>
				
    </body>
</html>
