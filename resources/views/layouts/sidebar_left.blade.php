<aside id="sidebar-left" class="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">{{ __('Navigation') }}</div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html"
            data-fire-event="sidebar-left-toggle">
            <i class="icon icon-menu" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
					@if(Auth::check() && Auth::user()->role == 99)
                    <li class="nav-parent member">
                        <a class="nav-link" href="#"><i class="icons icon-user-following"></i> <span>Anggota</span></a>
						<ul class="nav nav-children">
							<li class="add-member">
								<a class="nav-link" href="{{ route('tambah-anggota') }}">
									Tambah Anggota
								</a>
							</li>
							<li class="all-member">
								<a class="nav-link" href="{{ route('semua-anggota') }}">
									Semua Anggota
								</a>
							</li>
						</ul>
                    </li>
                    
                    <li class="nav-parent business">
                        <a class="nav-link" href="#"><i class="icons icon-pin"></i> <span>Usaha</span></a>
                        <ul class="nav nav-children">
							<li class="all-business">
								<a class="nav-link" href="{{ route('semua-usaha') }}">
									Semua Usaha
								</a>
							</li>
							<li class="all-business-category">
								<a class="nav-link" href="{{ route('bidang-usaha') }}">
									Bidang Usaha
								</a>
							</li>
						</ul>
                    </li>
                    
					<li class="nav-parent program">
                        <a class="nav-link" href="#"><i class="icons icon-notebook"></i> <span>Program</span></a>
						<ul class="nav nav-children">
							<li class="add-program">
								<a class="nav-link" href="{{ route('tambah-program') }}">
									Tambah Program
								</a>
							</li>
							<li class="all-program">
								<a class="nav-link" href="{{ route('semua-program') }}">
									Semua Program
								</a>
							</li>
							<li class="category-program">
								<a class="nav-link" href="{{ route('kategori-program') }}">
									Kategori Program
								</a>
							</li>
						</ul>
                    </li>
                    
                    <li class="nav-parent user">
						<a class="nav-link" href="#"><i class="icons icon-people"></i> <span>Pengguna</span></a>
						<ul class="nav nav-children">
							<li class="add-user">
								<a class="nav-link" href="{{ route('add-new-pengguna') }}">
									Tambah Pengguna
								</a>
							</li>
							<li class="all-user">
								<a class="nav-link" href="{{ route('semua-pengguna') }}">
									Semua Pengguna
								</a>
							</li>
						</ul>
                    </li>
					@else
                    <li class="member-profile">
						<a class="nav-link" href="{{ route('member-profile') }}"><i class="icons icon-user"></i> <span>Profile Saya</span></a>
					</li>
					@endif
                </ul>
            </nav>
        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>
    </div>
</aside>
