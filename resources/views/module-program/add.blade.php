@extends('layouts.app')
@section('title', 'Tambah Program')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}">
@endsection
@section('content')
    <section class="card">
        <div class="card-body">
        
            @if(session('status'))
                <div class="alert alert-success" role="alert">
                    <button type="button" 
                        class="close" 
                        data-dismiss="alert" 
                        aria-hidden="true"><i class="icon icon-close"></i></button>
                        <strong>{{ __('Berhasil!') }}</strong>
                        {{ session('status') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" 
                        class="close" 
                        data-dismiss="alert" 
                        aria-hidden="true"><i class="icon icon-close"></i></button>
                        <strong>{{ __('Gagal!') }}</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif

            <form action="{{ route('program.store') }}" method="POST" enctype="multipart/form-data">
                @csrf @method('POST')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mb-2">
                            <label for="program_name"><strong>{{ __('Nama program') }}</strong> {{ __('(harus diisi)') }}</label>
                            <input type="text" 
                                class="form-control" 
                                name="program_name" 
                                id="program_name" 
                                tabindex="1"
                                value="{{ old('program_name') }}"
                                required>
                                @if($errors->has('program_name'))
                                <div class="text-danger">
                                {{ $errors->first('program_name')}}
                                </div>
                                @endif
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mb-2">
                            <label for="program_category"><strong>{{ __('Kategori Program') }}</strong> {{ __('(harus diisi)') }}</label>
                            <select class="form-control" 
                                name="program_category" 
                                id="program_category" 
                                tabindex="2" 
                                data-plugin-selectTwo
                                required>
                                <option disabled selected value="0">{{ __('Pilih kategori program') }}</option>
                                @foreach($procat as $cat)
                                    @if(old('program_category') == $cat->id_procat)
                                    <option value="{{ $cat->id_procat }}" selected>{{ $cat->procat_name }}</option>
                                    @else
                                    <option value="{{ $cat->id_procat }}">{{ $cat->procat_name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('program_category'))
                            <div class="text-danger">
                                {{ $errors->first('program_category')}}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mb-2">
                            <label for="description"><strong>{{ __('Deskripsi') }}</strong> {{ __('(optional)') }}</label>
                            <textarea class="form-control" 
                                name="description" 
                                id="description" 
                                row="1" 
                                tabindex="3">
                                {{ old('description') }}
                            </textarea>
                            @if($errors->has('description'))
                            <div class="text-danger">
                            {{ $errors->first('description')}}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row no-gutters">
                    <div class="col-lg-6">
                        <div class="form-group mb-2 mr-2">
                            <label for="due_date_register"><strong>{{ __('Batas Pendaftaran') }}</strong> {{ __('(harus diisi)') }}</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon icon-calendar"></i></span>
                                <input type="text" 
                                    class="form-control" 
                                    name="due_date_register" 
                                    id="due_date_register" 
                                    tabindex="4" 
                                    data-plugin-datepicker
                                    value="{{ old('due_date_register') }}"
                                    required>
                            </div>
                            @if($errors->has('due_date_register'))
                            <div class="text-danger">
                            {{ $errors->first('due_date_register')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group mb-2">
                            <label for="due_time_register"><strong>{{ __('Jam selesai') }}</strong> {{ __('(harus diisi)') }}</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon icon-clock"></i></span>
                                <input type="text" 
                                    class="form-control" 
                                    name="due_time_register"
                                    id="due_time_register" 
                                    tabindex="5" 
                                    value="{{ old('due_time_register') }}"
                                    data-plugin-timepicker
                                    required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mb-2">
                            <label for="avatar"><strong>{{ __('Lampiran') }}</strong> {{ __('(optional)') }}</label>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input">
                                                <i class="fa fa-file fileupload-exists"></i>
                                                <span class="fileupload-preview">{{ old('attachment') }}</span>
                                            </div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-exists">Ganti foto</span>
                                                <span class="fileupload-new" tabindex="6">Unggah foto</span>
                                                <input type="file" 
                                                    id="attachment" 
                                                    name="attachment"
                                                    value="{{ old('attachment') }}">
                                            </span>
                                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($errors->has('attachment'))
                            <div class="text-danger">
                            {{ $errors->first('attachment')}}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="status_program"><strong>{{ __('Status program') }}</strong> {{ __('(harus diisi)') }}</label>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="radio-custom radio-primary form-check-inline mr-5">
                                <input type="radio" 
                                    id="open" 
                                    name="status_program"
                                    value="Open" 
                                    checked 
                                    tabindex="7">
                                <label for="open">{{ __('Open') }}</label>
                            </div>
                            <div class="radio-custom radio-primary form-check-inline">
                                <input type="radio" 
                                    id="close" 
                                    name="status_program" 
                                    value="Close">
                                <label for="close">{{ __('Close') }}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-action text-right mt-4">
                    <a href="#" class="btn btn-default go-back"><i class="icon icon-arrow-left-circle"></i> {{ __('Kembali') }}</a>
                    <button type="submit" class="btn btn-primary" tabindex="8"><i class="icon icon-doc"></i> {{ __('Tambahkan') }}</button>
                </div>
            </form>
        </div>
    </section>

    {{-- 
        <section class="card border">
            <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                <form action="store" method="post" class="form-horizontal form-bordered">
                @csrf @method('post')
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="id_program" class="control-label pt-2"><strong>{{ __('No. ID') }}</strong></label>
                            <input name="id_program" 
                                id="id_program" 
                                type="text" 
                                class="form-control"
                                required
                                @if($errors->has('id_program'))
                                    <div class="text-danger">
                                        {{ $errors->first('id_program')}}
                                    </div>
                                @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8">
                            <label for="program_name" class="control-label pt-2"><strong>{{ __('Nama Program') }} </strong> {{ __('(harus diisi)') }}</label>
                            <input name="program_name" 
                                id="program_name" 
                                type="text" 
                                class="form-control" 
                                placeholder="Nama Program">
                                @if($errors->has('program_name'))
                                    <div class="text-danger">
                                        {{ $errors->first('program_name')}}
                                    </div>
                                @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group mb-2">
                                <label for="id_cat_program"><strong>{{ __('Kategori') }}</strong> {{ __('(harus diisi)') }}</label>
                                <select class="form-control" 
                                    name="id_cat_program" 
                                    id="id_cat_program" 
                                    tabindex="9" 
                                    data-plugin-selectTwo>
                                    <option>Pilih kategori</option>
                                    <option value="#E201909030001">Workshop</option>
                                    <option value="#E201909030001">Pelatihan</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="text-medium">{{ __('Status Event (harus diisi)') }}</b>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="radio-custom">
                                    <input type="radio" name="stat" id="radio-active" value="0" checked>
                                    <label for="radio-active" class="">{{ __('Wajib') }} </label>
                                </div>
                                @if($errors->has('stat'))
                                    <div class="text-danger">
                                        {{ $errors->first('stat')}}
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="radio-custom">
                                    <input type="radio" name="stat" id="radio-deactive" value="1">
                                    <label for="radio-deactive" class=""> {{ __('Tidak Wajib ') }}</label>
                                </div>
                                @if($errors->has('stat'))
                                    <div class="text-danger">
                                        {{ $errors->first('stat')}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-right">
                            <button type="reset" class="btn btn-default mx-1"> {{ __('Kembali ') }}</button>
                            <button type="submit" class="btn btn-primary mx-1"> {{ __('Simpan ') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </section> 
    --}}

@endsection
@section('blockfoot')
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-timepicker/bootstrap-timepicker.js') }}"></script>
    <script type="text/javascript">
        /*
        Navigation Active
        */
        $('.program').addClass('nav-expanded nav-active');
        $('.add-program').addClass('nav-active');

        /*
        GO PREVIOUS PAGE
         */
         $(document).ready(function() {
            $('a.go-back').click(function () {
                parent.history.back();
                return false;
            });
            $('textarea').each(function(){$(this).val($(this).val().trim())});
        });

        $(".alert").fadeTo(3000, 1).fadeOut(1000, function(){
            $(".alert").fadeOut(6000);
        });

    </script>
@endsection