@extends('layouts.app')
@section('title', 'Semua Program')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}">
@endsection
@section('content')
    <header class="page-header">
        <h2>{{ __('Semua Program') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    
    <section class="card">
        <div class="card-body">
            @if (session('message'))
            <div class="alert alert-success" role="alert">
            <button type="button" 
                class="close" 
                data-dismiss="alert" 
                aria-hidden="true">
                <i class="icon icon-close"></i></button>
                <strong>{{ session('status') }}</strong><br/>
                {{ session('message') }}
            </div>
            @endif
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th>{{ __('No') }}</th>
                        <th>{{ __('Nama Program') }}</th>
                        <th>{{ __('Kategori') }}</th>
                        <th>{{ __('Deskripsi') }}</th>
                        <th>{{ __('Lampiran') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th width="135">{{ __('Batas Pendaftaran') }}</th>
                        <th width="40">{{ __('Aksi') }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Workshop Animasi-Start Up The Business of Animation</td>
                        <td>Workshop</td>
                        <td>Workshop yang diadakan oleh Gapura Digital Yogyakarta</td>
                        <td>
                            <a href="#"><i class="icon icon-paper-clip"></i></a>
                        </td>
                        <td>Open</td>
                        <td width="135">20/09/2019 at 11:20 PM</td>
                        <td width="40" class="actions">
                            <a href="#detailProgram" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Workshop Animasi-Start Up The Business of Animation</td>
                        <td>Workshop</td>
                        <td>Workshop yang diadakan oleh Gapura Digital Yogyakarta</td>
                        <td>
                            <a href="#"><i class="icon icon-paper-clip"></i></a>
                        </td>
                        <td>Open</td>
                        <td width="135">20/09/2019 at 11:20 PM</td>
                        <td width="40" class="actions">
                            <a href="#detailProgram" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Workshop Animasi-Start Up The Business of Animation</td>
                        <td>Workshop</td>
                        <td>Workshop yang diadakan oleh Gapura Digital Yogyakarta</td>
                        <td>
                            <a href="#"><i class="icon icon-paper-clip"></i></a>
                        </td>
                        <td>Open</td>
                        <td width="135">20/09/2019 at 11:20 PM</td>
                        <td width="40" class="actions">
                            <a href="#detailProgram" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Workshop Animasi-Start Up The Business of Animation</td>
                        <td>Workshop</td>
                        <td>Workshop yang diadakan oleh Gapura Digital Yogyakarta</td>
                        <td>
                            <a href="#"><i class="icon icon-paper-clip"></i></a>
                        </td>
                        <td>Open</td>
                        <td width="135">20/09/2019 at 11:20 PM</td>
                        <td width="40" class="actions">
                            <a href="#detailProgram" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Workshop Animasi-Start Up The Business of Animation</td>
                        <td>Workshop</td>
                        <td>Workshop yang diadakan oleh Gapura Digital Yogyakarta</td>
                        <td>
                            <a href="#"><i class="icon icon-paper-clip"></i></a>
                        </td>
                        <td>Open</td>
                        <td width="135">20/09/2019 at 11:20 PM</td>
                        <td width="40" class="actions">
                            <a href="#detailProgram" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>{{ __('No') }}</th>
                        <th>{{ __('Nama Program') }}</th>
                        <th>{{ __('Kategori') }}</th>
                        <th>{{ __('Deskripsi') }}</th>
                        <th>{{ __('Lampiran') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th width="135">{{ __('Batas Pendaftaran') }}</th>
                        <th width="40">{{ __('Aksi') }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </section>

    <!-- {{-- Modal : Detail Program --}} -->
    <div id="detailProgram" class="modal-block modal-block-primary modal-block-lg mfp-hide">
        <section class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3 class="mt-0 mb-1"><i class="icon icon-info"></i> {{ __('Detail Program') }}</h3>
                    <p>{{ __('Anda dapat melakukan perubahan data program pada formulir di bawah.') }}</p>
                </div>
                <form method="post" enctype="multipart/form-data" id="form-edit">
                    @csrf @method('patch')
                    <input type="hidden" name="save_method" id="save_method" value="edit">
                    <input type="hidden" name="id_program_edit" id="id_program_edit">
                    <input type="hidden" name="tgl" id="tgl">
                    <input type="hidden" name="jam" id="jam">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-2">
                                <label for="program_name"><strong>{{ __('Nama program') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="program_name" 
                                    id="program_name" 
                                    value="Workshop Animasi-Start Up The Business of Animation" 
                                    tabindex="1">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-2">
                                <label for="program_category"><strong>{{ __('Kategori Program') }}</strong> {{ __('(harus diisi)') }}</label>
                                <select class="form-control" name="program_category" id="program_category" tabindex="2" data-plugin-selectTwo-detailProgram>
                                    <option disabled value="0" selected>{{ __('Pilih kategori program') }}</option>
                                    @foreach($procat as $cat)
                                    <option value="{{ $cat->id_procat }}">{{ $cat->procat_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-2">
                                <label for="description"><strong>{{ __('Deskripsi') }}</strong> {{ __('(optional)') }}</label>
                                <textarea class="form-control" 
                                name="description" 
                                id="description" 
                                row="1" 
                                tabindex="3">Workshop yang diadakan oleh Gapura Digital Yogyakarta.</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row no-gutters">
                        <div class="col-lg-6">
                            <div class="form-group mb-2 mr-2">
                                <label for="due_date_register"><strong>{{ __('Batas Pendaftaran') }}</strong> {{ __('(harus diisi)') }}</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon icon-calendar"></i></span>
                                    <input type="text" 
                                    class="form-control" 
                                    name="due_date_register" 
                                    id="due_date_register" 
                                    value="26/09/2019" 
                                    tabindex="4" 
                                    data-plugin-datepicker>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group mb-2">
                                <label for="due_time_register"><strong>{{ __('Jam selesai') }}</strong> {{ __('(harus diisi)') }}</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon icon-clock"></i></span>
                                    <input 
                                    type="text" 
                                    class="form-control" 
                                    name="due_time_register" 
                                    id="due_time_register" 
                                    value="11:20 PM" 
                                    tabindex="5" 
                                    data-plugin-timepicker>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-2">
                                <label for="avatar"><strong>{{ __('Lampiran') }}</strong> {{ __('(optional)') }}</label>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="fileupload fileupload-exists" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview">Makna dibalik lomba 17 - Fanpage.png</span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists" tabindex="6">Ganti foto</span>
                                                    <span class="fileupload-new">Unggah foto</span>
                                                    <input type="file" name="attachment" id="attachment"/>
                                                </span>
                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Hapus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <label for="status_program"><strong>{{ __('Status program') }}</strong> {{ __('(harus diisi)') }}</label>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="radio-custom radio-primary form-check-inline mr-5">
                                    <input type="radio" 
                                        id="open" 
                                        name="status_program" 
                                        checked 
                                        tabindex="7" 
                                        value="Open">
                                        <label for="open">{{ __('Open') }}</label>
                                </div>
                                <div class="radio-custom radio-primary form-check-inline">
                                    <input type="radio" 
                                        id="close" 
                                        name="status_program" 
                                        value="Close">
                                        <label for="close">{{ __('Close') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-action text-center mt-4">
                        <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                        <button type="submit" class="btn btn-primary update-data" tabindex="8"><i class="icon icon-doc"></i> {{ __('Perbarui') }}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>

    <!-- {{-- Modal : Delete Program --}} -->
    <div id="deleteProgram" class="modal-block modal-block-primary modal-block-sm mfp-hide">
        <section class="card">
            <div class="card-body text-center">
                <h3 class="mt-0">Anda Yakin?</h3>
                <p>
                    {{ __('Data ini akan dihapus secara permanen. Sehingga Anda tidak akan bisa mengembalikannya lagi.') }}
                </p>
                <div class="modal-action mt-3">
                    <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                    <button type="button" class="btn btn-danger delete-data"><i class="icon icon-trash"></i> {{ __('Ya, hapus') }}</button>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('blockfoot')
    <script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-timepicker/bootstrap-timepicker.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            /*
            Datatable : List 'Usaha'
            */
            var $table = $('.table');
            var table = $table.DataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                processing: true,
                "columnDefs": [
                    { "orderable": false, "targets": 7 }
                ],
                "ajax": {
                    "url": "{{ route('program.data') }}",
                    "type": "GET",
                    "serverSide": true,
                }
            });

            /*
            Navigation Active
            */
            $('.program').addClass('nav-expanded nav-active');
            $('.all-program').addClass('nav-active');

            /*
            Modal : Delete Program Category
            */
            $('.modal-basic').magnificPopup({
                type: 'inline',
                preloader: false,
                modal: true
            });

            /*
            Modal Dismiss : Delete Program Category
            */
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });

            /*
            SELECT2 IN MODAL
            */
            (function($) {
                'use strict';
                if ( $.isFunction($.fn[ 'select2' ]) ) {
                    $(function() {
                        $('[data-plugin-selectTwo-detailProgram]').each(function() {
                            var $this = $( this ),
                                opts = {
                                    dropdownParent: $("#detailProgram")
                                };
                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;
                            $this.themePluginSelect2(opts);
                        });
                    });
                }
            }).apply(this, [jQuery]);
            $(".alert").delay(2000).fadeOut();
            $('.form-edit').on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    var id_program = $("#id_program_edit").val();                    
                    $.ajax({
                        url: "/admin/program/kategori-program/update/" + id_program,
                        type: "POST",
                        data: new FormData($(".form")[0]),
                        async: true,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $('.alert-success').css('display', 'block').delay(2000).fadeOut();
                            $(".table").DataTable().ajax.reload();
                            $.magnificPopup.close();
                        },
                        error: function (data) {
                            console.log(data)
                            $('.alert-danger').css('display', 'block').delay(2000).fadeOut();
                        }
                    });
                    return false;
                }
            });
            
        });

        /*
        Modal : Eeeeediiiiiiiiiit
        */
        function editData(id_program) {
            // alert('edit')
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#detailProgram'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        save_method = $("#save_method").val();
                        if (save_method === "edit"){
                            $.ajax({
                                url: "/admin/program/edit/" + id_program,
                                type: "GET",
                                dataType: "JSON",
                                success: function(data){
                                    var due_date = "{{ date('d/m/Y', strtotime('"+data.due_date+"')) }"
                                    $('#program_name').val(data.program_name);
                                    $('#program_category').val(data.id_procat).trigger('change');
                                    $('#description').val(data.desc);
                                    $('.fileupload-preview').text(data.attachment);
                                    $('#due_date_register').val(data.due_date);
                                    $('#due_time_register').val(data.due_time);
                                    if(data.program_status === 'Open') $("#open").attr("checked", "checked");
                                    else $("#close").attr("checked", "checked");
                                    $('#id_program').val(data.id_program);
                                    $('#form-edit').attr('action', "/admin/program/update/"+id_program);
                                },
                                error: function(data){
                                    alert("Error: Tidak dapat menampilkan edit Data!");
                                    console.log(data)
                                }
                            });
                        }
                    }
                }
            });
            $("#open").removeAttr("checked");
            $("#close").removeAttr("checked");
        }

        /*
        Modal : Delete 
        */
        function deleteData(id_program) {
            // alert(id)
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#deleteProgram'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        $(".delete-data").on('click', function(){
                            // alert('del');
                            $.ajax({
                                url: "/admin/program/hapus/"+id_program,
                                type: "POST",
                                data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                                success: function () {
                                    $(".alert-danger").css("display", "block").delay(2000).fadeOut();
                                    $.magnificPopup.close();
                                    $('.table').DataTable().ajax.reload();
                                    console.log()
                                },
                                error: function () {
                                    $(".alert-success").css("display", "block").delay(2000).fadeOut();
                                    console.log()
                                }
                            });                            
                        });
                    }
                }
            });
        }
    </script>
@endsection