<header class="page-header">
    <h2>{{ __('Detail Lokakarya') }}</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li><i class="fa fa-home"></i></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<section class="card" style="min-height: 800px;">
    <div class="card-header">{{ __('Lokakarya') }}</div>
    <form action="" id="form-content" method="POST" class="form-horizontal form-bordered" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="row">
                <div class="col-md-3 mb-3">
                    <div class="widget-toggle-expand mb-3 widget-collapsed border p-2 text-center">
                        <div class="widget-header">
                            <h5 class="mb-2">ID Lokakarya</h5>
                        </div>
                        <div class="widget-content-collapsed">
                            <label>WSH-0001</label>
                        </div>
                    </div>
                    <div class="widget-toggle-expand mb-3 widget-collapsed border p-2 text-center">
                        <div class="widget-header">
                            <h5 class="mb-2">Dibuat pada</h5>
                        </div>
                        <div class="widget-content-collapsed">
                            <label>02-09-2019 at 16:00 </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-9 mb-3">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="id_workshop" class="control-label pt-2">{{ __('No. ID'}}</label>
                            <input name="id_workshop" id="id_workshop" type="text" class="form-control">
                            @if($errors->has('id_workshop'))
                            <div class="text-danger">
                                {{ $errors->first('id_workshop')}}
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="workshop_name" class="control-label pt-2">{{ __('Nama lokakarya (harus diisi)') }}</label>
                            <input name="workshop_name" id="workshop_name" type="text" class="form-control" placeholder="Nama lokakarya">
                            @if($errors->has('workshop_name'))
                            <div class="text-danger">
                                {{ $errors->first('workshop_name')}}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <footer class="card-footer text-right">
            <button type="reset" class="btn btn-default"> {{ __('Kembali ') }}</button>
            <button type="submit" class="btn btn-primary mx-1"> {{ __('Simpan ') }}</button>
        </footer>
    </form>
</section>