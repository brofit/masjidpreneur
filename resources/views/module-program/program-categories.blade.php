@extends('layouts.app')
@section('title', 'Kategori Program')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
@endsection
@section('content')
    <header class="page-header">
        <h2>{{ __('Semua Kategori Program') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="row">
        <div class="col-lg-4">
            <section class="card mb-4">
                <header class="card-header">{{ __('Tambah Kategori Program') }}</header>
                <div class="card-body">
                    <form method="POST" class="form">
                        @csrf @method('POST')
                        <div class="form-group mb-2">
                            <label for="procat_name"><strong>{{ __('Nama Kategori') }}</strong>
                                {{ __('(harus diisi)') }}</label>
                            <input type="text" class="form-control" id="procat_name" name="procat_name">
                            <input type="hidden" name="save_method" id="save_method" value="add">
                            <p class="help-block">{{ __('Contoh : Workshop, Seminar, Kajian, Lomba, dll.') }}</p>
                        </div>
                        <div class="form-group mb-3">
                            <label for="procat_status"><strong>{{ __('Status program') }}</strong> {{ __('(harus diisi)') }}</label>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="radio-custom radio-primary form-check-inline mr-4">
                                        <input type="radio" id="required" name="procat_status" value="Wajib" checked>
                                        <label for="required">{{ __('Wajib') }}</label>
                                    </div>
                                    <div class="radio-custom radio-primary form-check-inline">
                                        <input type="radio" id="unrequired" name="procat_status" value="Tidak Wajib">
                                        <label for="unrequired">{{ __('Tidak Wajib') }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block"><i class="icon icon-doc"></i>{{ __('Tambahkan') }}</button>
                    </form>
                </div>
            </section>
        </div>

        <div class="col-lg-8">
            <section class="card">
                <header class="card-header">{{ __('Kategori Program') }}</header>
                <div class="card-body">
                    <div class="alert alert-success" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon icon-close"></i></button>
                        <strong>{{ __('Berhasil!') }}</strong> {{ __('Data berhasil dihapus dari basis data.') }}

                    </div>
                    <div class="alert alert-danger" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon icon-close"></i></button>
                        <strong>{{ __('Gagal!') }}</strong> {{ __('Data gagal dihapus dari basis data.') }}
                    </div>

                    <table class="table table-sm table-hover">
                        <thead>
                            <tr>
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama Kategori') }}</th>
                                <th>{{ __('Satus') }}</th>
                                <th>{{ __('Dibuat pada') }}</th>
                                <th width="40">{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Workshop</td>
                                <td>Wajib</td>
                                <td>12/09/2019 at 12.20</td>
                                <td width="40" class="actions">
                                    <a href="#edit_procat" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                    <a href="#delete_procat" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Seminar</td>
                                <td>Wajib</td>
                                <td>12/09/2019 at 12.20</td>
                                <td width="40" class="actions">
                                    <a href="#edit_procat" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                    <a href="#delete_procat" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Kajian</td>
                                <td>Wajib</td>
                                <td>12/09/2019 at 12.20</td>
                                <td width="40" class="actions">
                                    <a href="#edit_procat" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                    <a href="#delete_procat" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Lomba</td>
                                <td>Wajib</td>
                                <td>12/09/2019 at 12.20</td>
                                <td width="40" class="actions">
                                    <a href="#edit_procat" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                    <a href="#delete_procat" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama Kategori') }}</th>
                                <th>{{ __('Dibuat pada') }}</th>
                                <th>{{ __('Satus') }}</th>
                                <th width="40">{{ __('Aksi') }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </section>
        </div>
    </div>

    <!-- {{-- Modal : Edit Program Category --}} -->
    <div id="editProgramCategory" class="modal-block modal-block-primary modal-block-sm mfp-hide">
        <section class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3 class="mt-0 mb-2"><i class="icon icon-pencil"></i> {{ __('Ubah') }}</h3>
                    <p >{{ __('Ubah data kategori program') }}</p>
                </div>
                <form method="post" class="form">
                    @csrf @method('PATCH')
                    <div class="form-group mb-2">
                        <label for="procat_name"><strong>{{ __('Ubah nama kategori') }}</strong> {{ __('(harus diisi)') }}</label>
                        <input type="text" class="form-control" id="procat_name" name="procat_name" tabindex="1" value="Pertanian (Agriculture)">
                        <input type="hidden" name="save_method" id="save_method" value="edit">
                        <input type="hidden" name="id_procat_edit" id="id_procat_edit">
                    </div>
                    <div class="form-group mb-3">
                        <label for="procat_status"><strong>{{ __('Status program') }}</strong> {{ __('(harus diisi)') }}</label>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="radio-custom radio-primary form-check-inline mr-4">
                                    <input type="radio" id="edit_required" name="procat_status" value="Wajib">
                                    <label for="edit_required">{{ __('Wajib') }}</label>
                                </div>
                                <div class="radio-custom radio-primary form-check-inline">
                                    <input type="radio" id="edit_unrequired" name="procat_status" value="Tidak Wajib">
                                    <label for="edit_unrequired">{{ __('Tidak Wajib') }}</label>
                                </div>
                                <input type="text" name="test" id="status" class="display-none">
                            </div>
                        </div>
                    </div>
                    <div class="modal-action text-center mt-4">
                        <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                        <button type="submit" class="btn btn-primary" tabindex="3"><i class="icon icon-doc"></i> {{ __('Perbarui') }}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>

    <!-- {{-- Modal : Delete Program Category --}} -->
    <div id="deleteProgramCategory" class="modal-block modal-block-primary modal-block-sm mfp-hide">
        <section class="card">
            <div class="card-body text-center">
                <h3 class="mt-0">Anda Yakin?</h3>
                <p>
                    {{ __('Data ini akan dihapus secara permanen. Sehingga Anda tidak akan bisa mengembalikannya lagi.') }}
                    <input type="hidden" name="id_procat_delete" id="id_procat_delete">
                </p>
                <div class="modal-action mt-3">
                    <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                    <button type="submit" class="btn btn-danger delete-data"><i class="icon icon-trash"></i> {{ __('Ya, hapus') }}</button>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('blockfoot')
    <script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            /*
            Datatable : List 'Bidang Usaha'
            */
            var $table = $('.table');
            var table = $table.DataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                processing: true,
                "columnDefs": [
                    { "orderable": false, "targets": 4 }
                ],
                "ajax": {
                    "url": "{{ route('kategori.program.data') }}",
                    "type": "GET",
                    "serverSide": true
                },
            });

            /*
            Navigation Active
            */
            $('.program').addClass('nav-expanded nav-active');
            $('.category-program').addClass('nav-active');

            /*
            Modal : Delete Program Category
            */
            $('.modal-basic').magnificPopup({
                type: 'inline',
                preloader: false,
                modal: true
            });

            /*
            Modal Dismiss : Delete Program Category
            */
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });

            /* 
            Submit tanpa refresh
            */
            $('.form').on('submit', function (e) {
                if (!e.isDefaultPrevented()) {

                    save_method = $("#save_method").val();
                    var id_procat = $("#id_procat_edit").val();

                    // if (save_method === 'add') alert('add');
                    // else alert('edit' + id_procat);
                    
                    if (save_method === 'add') url = "{{ route('kategori.program.store') }}";
                    else url = "/admin/program/kategori-program/update/" + id_procat;
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: new FormData($(".form")[0]),
                        async: true,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $('.alert-success').css('display', 'block').delay(2000).fadeOut();
                            table.ajax.reload();
                            $('#procat_name').val('');
                            $.magnificPopup.close();
                        },
                        error: function (data) {
                            console.log(data)
                            $('.alert-danger').css('display', 'block').delay(2000).fadeOut();
                        }
                    });
                    return false;
                }
            });

        });

        /*
        Modal : Tampil Edit Bidang Usaha
        */
        function editData(id_procat) {
            // alert('edit')
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#editProgramCategory'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        save_method = $("#save_method").val();
                        if (save_method === "edit"){
                            $.ajax({
                                url: "/admin/program/kategori-program/edit/" + id_procat,
                                type: "GET",
                                dataType: "JSON",
                                success: function(data){
                                    $('#procat_name').val(data.procat_name);
                                    if(data.procat_status === 'Wajib') $("#edit_required").attr("checked", "checked");
                                    else $("#edit_unrequired").attr("checked", "checked");
                                    $('#id_procat_edit').val(data.id_procat);
                                },
                                error: function(){
                                    alert("Error: Tidak dapat menampilkan edit Data!");
                                    console.log(data)
                                }
                            });
                        }
                    }
                }
            });
            $("#edit_required").removeAttr("checked");
            $("#edit_unrequired").removeAttr("checked");
        }


        /*
        Modal : Delete Bidang Usaha
        */
        function deleteData(id_procat) {
            // alert(id)
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#deleteProgramCategory'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        $(".delete-data").on('click', function(){
                            // alert('del');
                            $.ajax({
                                url: "/admin/program/kategori-program/hapus/"+id_procat,
                                type: "POST",
                                data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                                success: function () {
                                    $(".alert-success").css("display", "block").delay(2000).fadeOut();
                                    $.magnificPopup.close();
                                    $('.table').DataTable().ajax.reload();
                                    console.log()
                                },
                                error: function () {    
                                    // $(".alert-danger").css("display", "block").delay(2000).fadeOut();
                                    console.log()
                                }
                            });                            
                        });
                    }
                }
            });
        }
    </script>
@endsection