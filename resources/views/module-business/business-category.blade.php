@extends('layouts.app')
@section('title', 'Bidang Usaha')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
@endsection
@section('content')
    <header class="page-header">
        <h2>{{ __('Bidang Usaha') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="row">
        <div class="col-lg-4">
            <section class="card mb-4">
                <header class="card-header">{{ __('Tambah Bidang Usaha') }}</header>
                <div class="card-body">
                    <form method="POST" class="form">
                        @csrf @method('post')
                        <div class="form-group mb-3">
                            <label for="business_category_name"><strong>{{ __('Nama bidang usaha') }}</strong> {{ __('(harus diisi)') }}</label>
                            <input type="text" class="form-control" id="business_category_name" name="business_category_name" tabindex="1">
                            <input type="hidden" name="save_method" id="save_method" value="add">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block"><i class="icon icon-doc"></i> Tambahkan</button>
                    </form>
                </div>
            </section>
        </div>
        
        <div class="col-lg-8">
            <section class="card">
                <header class="card-header">{{ __('Bidang Usaha') }}</header>
                <div class="card-body">

                    <div class="alert alert-success" style="display:none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>{{ __('Berhasil!') }}</strong> {{ __('Data berhasil dihapus dari basis data.') }}</a>
                    </div>
                    <div class="alert alert-danger" style="display:none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>{{ __('Gagal!') }}</strong> {{ __('Data gagal dihapus dari basis data.') }}</a>
                    </div>

                    <table class="table table-hover table-sm table-business-categories">
                        <thead>
                            <tr>
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama') }}</th>
                                <th>{{ __('Total Usaha') }}</th>
                                <th>{{ __('Dibuat pada') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Pertanian (Agriculture)</td>
                                <td>4</td>
                                <td>01/01/2016 at 02.00 PM</td>
                                <td class="actions">
                                    <a href="#editBusinessCategory" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                    <a href="#deleteBusinessCategory" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Pertambangan (Mining)</td>
                                <td>7</td>
                                <td>01/01/2016 at 02.00 PM</td>
                                <td class="actions">
                                    <a href="#editBusinessCategory" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                    <a href="#deleteBusinessCategory" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Pertambangan (Mining)</td>
                                <td>10</td>
                                <td>01/01/2016 at 02.00 PM</td>
                                <td class="actions">
                                    <a href="#editBusinessCategory" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                    <a href="#deleteBusinessCategory" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Pertambangan (Mining)</td>
                                <td>2</td>
                                <td>01/01/2016 at 02.00 PM</td>
                                <td class="actions">
                                    <a href="#editBusinessCategory" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                    <a href="#deleteBusinessCategory" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama') }}</th>
                                <th>{{ __('Total Usaha') }}</th>
                                <th>{{ __('Dibuat pada') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </section>
        </div>
    </div>

    {{-- Modal : Delete Business Category --}}
    <div id="deleteBusinessCategory" class="modal-block modal-block-primary modal-block-sm mfp-hide">
        <section class="card">
            <div class="card-body text-center">
                <h3 class="mt-0 mb-4">{{ __('Anda Yakin?') }}</h3>
                <p>
                    {{ __('Data ini akan dihapus secara permanen. Sehingga Anda
                    tidak akan bisa mengembalikannya lagi.') }}
                    <input type="hidden" name="id_bsfield_delete" id="id_bsfield_delete">
                </p>
                <div class="modal-action mt-3">
                    <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                    <button type="button" class="btn btn-danger delete-data"><i class="icon icon-trash"></i> {{ __('Ya, hapus') }}</button>
                </div>
            </div>
        </section>
    </div>

    {{-- Modal : Edit Business Category --}}
    <div id="editBusinessCategory" class="modal-block modal-block-primary modal-block-sm mfp-hide">
        <section class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3 class="mt-0 mb-3"><i class="icon icon-pencil"></i> {{ __('Ubah') }}</h3>
                    <p class="mb-2">{{ __('Ubah nama bidang usaha (harus diisi)') }}</p>
                </div>
                <form action="" method="post" class="form">
                    @csrf @method('PATCH')
                    <div class="form-group text-center">
                        <input type="text" class="form-control" id="business_category_name" name="business_category_name" tabindex="1" value="Pertanian (Agriculture)">
                        <input type="hidden" name="save_method" id="save_method" value="edit">
                        <input type="hidden" name="id_bsfield_edit" id="id_bsfield_edit">
                    </div>
                    <div class="modal-action text-center mt-4">
                        <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                        <button type="submit" class="btn btn-primary update-data"><i class="icon icon-doc"></i> {{ __('Perbarui') }}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('blockfoot')
    <script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript">
        var save_method;
        $(function () {
            /*
            Datatable : List 'Bidang Usaha'
            */
            var $table = $('.table');
            var table = $table.DataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                processing: true,
                "columnDefs": [
                    { "orderable": false, "targets": 4 }
                ],
                "ajax": {
                    "url": "{{ route('bidang.usaha.data') }}",
                    "type": "GET",
                    "serverSide": true
                },
            });

            /*
            Modal : Delete Business Category
            */
            $('.modal-basic').magnificPopup({
                type: 'inline',
                preloader: false,
                modal: true
            });

            /*
            Modal Dismiss : Delete Business Category
            */
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });

            /*
            Navigation Active
            */
            $('.business').addClass('nav-expanded nav-active');
            $('.all-business-category').addClass('nav-active');

            /* 
            Submit tanpa refresh
            */
            $('.form').on('submit', function (e) {
                if (!e.isDefaultPrevented()) {

                    save_method = $("#save_method").val();
                    var id_bsfield = $("#id_bsfield_edit").val();

                    // if (save_method === 'add') alert('add');
                    // else alert('edit' + id_bsfield);
                    
                    if (save_method === 'add') url = "{{ route('bidang.usaha.store') }}";
                    else url = "/admin/usaha/bidang-usaha/update/" + id_bsfield;
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: new FormData($(".form")[0]),
                        async: true,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $('.alert-success').css('display', 'block').delay(2000).fadeOut();
                            table.ajax.reload();
                            $('#business_category_name').val('');
                            $.magnificPopup.close();
                        },
                        error: function () {
                            $('.alert-danger').css('display', 'block').delay(2000).fadeOut();
                        }
                    });
                    return false;
                }
            });
        });

        /*
        Modal : Tampil Edit Bidang Usaha
        */
        function editData(id_bsfield) {
            // alert('edit')
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#editBusinessCategory'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        save_method = $("#save_method").val();
                        if (save_method !== "edit") {
                            alert("delete");
                        }else{
                            $.ajax({
                                url: "/admin/usaha/bidang-usaha/edit/" + id_bsfield,
                                type: "GET",
                                dataType: "JSON",
                                success: function(data){
                                    $('#business_category_name').val(data.bsfields_name);
                                    $('#id_bsfield_edit').val(data.id_bsfield);
                                },
                                error: function(){
                                    alert("Error: Tidak dapat menampilkan edit Data!");
                                    console.log(data)
                                }
                            });
                        }
                    }
                }
            });
        }


        /*
        Modal : Delete Bidang Usaha
        */
        function deleteData(id_bsfield) {
            // alert(id)
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#deleteBusinessCategory'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        $(".delete-data").on('click', function(){
                            // alert('del');
                            $.ajax({
                                url: "/admin/usaha/bidang-usaha/hapus/"+id_bsfield,
                                type: "POST",
                                data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                                success: function () {
                                    $(".alert-success").css("display", "block").delay(2000).fadeOut();
                                    $.magnificPopup.close();
                                    $('.table').DataTable().ajax.reload();
                                    console.log()
                                },
                                error: function () {    
                                    // $(".alert-danger").css("display", "block").delay(2000).fadeOut();
                                    console.log()
                                }
                            });                            
                        });
                        //alert("admin/bidang-usaha/hapus/"+id_bsfield);
                    }
                }
            });
        }
    </script>
@endsection