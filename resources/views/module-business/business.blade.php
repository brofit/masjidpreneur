@extends('layouts.app')
@section('title', 'Semua Usaha')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}">
@endsection
@section('content')
    <header class="page-header">
        <h2>{{ __('Semua Usaha') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <section class="card">
        <div class="card-body">
            
            @if(session('status'))
            <div class="alert alert-success val" role="alert">
                <button type="button" 
                    class="close" 
                    data-dismiss="alert" 
                    aria-hidden="true"><i class="icon icon-close"></i></button>
                    <strong>{{ __('Berhasil!') }}</strong>
                    {{ session('status') }}
            </div>
            @endif

            @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" 
                    class="close" 
                    data-dismiss="alert" 
                    aria-hidden="true"><i class="icon icon-close"></i></button>
                    <strong>{{ __('Gagal!') }}</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif

            <div class="alert del" role="alert" style="display:none">
                <button type="button" 
                    class="close" 
                    data-dismiss="alert" 
                    aria-hidden="true"><i class="icon icon-close"></i></button>
                    <strong>{{ __('Berhasil!') }}</strong>
                    <span class="msg"></span>
            </div>

            <table class="table table-hover table-sm table-business-categories">
                <thead>
                    <tr>
                        <th>{{ __('No') }}</th>
                        <th width="120">{{ __('Nama Usaha') }}</th>
                        <th width="120">{{ __('Bidang Usaha') }}</th>
                        <th>{{ __('Brand') }}</th>
                        <th>{{ __('Alamat') }}</th>
                        <th>{{ __('Kontak') }}</th>
                        <th width="65">{{ __('Aksi') }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td width="120">Perkebunan Kelapa Sawit</td>
                        <td width="120">Pertanian (Agriculture)</td>
                        <td>PT. Makmur Bersama</td>
                        <td>Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</td>
                        <td>(02177) 934 9200</td>
                        <td  width="65" class="actions">
                            <a href="" class="modal-basic"><i class="icon icon-link"></i></a>
                            <a href="#editBusinessCategory" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteBusinessCategory" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td width="120">Perkebunan Kelapa Sawit</td>
                        <td width="120">Pertanian (Agriculture)</td>
                        <td>PT. Makmur Bersama</td>
                        <td>Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</td>
                        <td>(02177) 934 9200</td>
                        <td  width="65" class="actions">
                            <a href="" class="modal-basic"><i class="icon icon-link"></i></a>
                            <a href="#editBusinessCategory" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteBusinessCategory" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td width="120">Perkebunan Kelapa Sawit</td>
                        <td width="120">Pertanian (Agriculture)</td>
                        <td>PT. Makmur Bersama</td>
                        <td>Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</td>
                        <td>(02177) 934 9200</td>
                        <td  width="65" class="actions">
                            <a href="" class="modal-basic"><i class="icon icon-link"></i></a>
                            <a href="#editBusinessCategory" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteBusinessCategory" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td width="120">Perkebunan Kelapa Sawit</td>
                        <td width="120">Pertanian (Agriculture)</td>
                        <td>PT. Makmur Bersama</td>
                        <td>Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</td>
                        <td>(02177) 934 9200</td>
                        <td  width="65" class="actions">
                            <a href="" class="modal-basic"><i class="icon icon-link"></i></a>
                            <a href="#editBusinessCategory" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteBusinessCategory" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td width="120">Perkebunan Kelapa Sawit</td>
                        <td width="120">Pertanian (Agriculture)</td>
                        <td>PT. Makmur Bersama</td>
                        <td>Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</td>
                        <td>(02177) 934 9200</td>
                        <td  width="65" class="actions">
                            <a href="https://ptmakmursejahterabersama.com" target="_blank"><i class="icon icon-link"></i></a>
                            <a href="#detailBusiness" class="modal-basic"><i class="icon icon-info"></i></a>
                            <a href="#deleteBusiness" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>{{ __('No') }}</th>
                        <th width="120">{{ __('Nama Usaha') }}</th>
                        <th width="120">{{ __('Bidang Usaha') }}</th>
                        <th>{{ __('Brand') }}</th>
                        <th>{{ __('Alamat') }}</th>
                        <th>{{ __('Kontak') }}</th>
                        <th width="65">{{ __('Aksi') }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </section>

    <!-- {{-- Modal : Detail Business --}} -->
    <div id="detailBusiness" class="modal-block modal-block-primary modal-block-lg mfp-hide">
        <section class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3 class="mt-0 mb-1"><i class="icon icon-info"></i> {{ __('Detail Usaha') }}</h3>
                    <p class="mb-2">{{ __('Pemilik usaha :') }} 
                        <a href="" class="member_name">
                            <strong>John Doe Junior</strong>
                        </a>
                    </p>
                    <p>{{ __('Anda dapat melakukan perubahan data usaha pada formulir di bawah.') }}</p>
                </div>
                <form action="" method="post" id="form-business" class="form">
                    @csrf @method('patch')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <input type="hidden" name="save_method" id="save_method" value="edit">
                                <input type="hidden" name="id_business" id="id_business">
                                <label for="business_name"><strong>{{ __('Nama usaha') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="business_name" 
                                    id="business_name" 
                                    value="Perkebunan Kelapa Sawit" 
                                    tabindex="1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="id_bsfield"><strong>{{ __('Bidang usaha') }}</strong> {{ __('(harus diisi)') }}</label>
                                <select class="form-control" 
                                    name="id_bsfield" 
                                    id="id_bsfield" 
                                    tabindex="2" 
                                    data-plugin-selectTwo-editBusiness>
                                    <option disabled>{{ __('Pilih kategori usaha') }}</option>
                                    @foreach($field as $f)
                                    <option value="{{ $f->id_bsfield }}">{{ $f->bsfields_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="brand"><strong>{{ __('Brand') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="brand" 
                                    id="brand" 
                                    value="PT. Makmur Bersama" 
                                    tabindex="3">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="business_contact"><strong>{{ __('phone') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="business_contact" 
                                    id="business_contact" 
                                    value="(02177) 934 9200" 
                                    tabindex="4">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="desc"><strong>{{ __('Deskripsi') }}</strong> {{ __('(optional)') }}</label>
                                <textarea class="form-control" 
                                    name="desc" 
                                    id="desc" 
                                    row="1" 
                                    tabindex="5">Kami selalu mengedepankan kemakmuran masyarakat Indonesia secara merata.</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="address"><strong>{{ __('Alamat') }}</strong> {{ __('(harus diisi)') }}</label>
                                <textarea class="form-control" 
                                    name="address" 
                                    id="address" 
                                    row="1" 
                                    tabindex="6">Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-2">
                                <label for="website"><strong>{{ __('Link detail') }}</strong> {{ __('(optional)') }}</label>
                                <input type="link" 
                                    class="form-control" 
                                    name="website" 
                                    id="website" 
                                    value="https://ptmakmursejahterabersama.com" tabindex="7">
                            </div>
                        </div>
                    </div>

                    <div class="modal-action text-center mt-4">
                        <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                        <button type="submit" class="btn btn-primary" tabindex="8"><i class="icon icon-doc"></i> {{ __('Perbarui') }}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>

    <!-- {{-- Modal : Delete Business --}} -->
    <div id="deleteBusiness" class="modal-block modal-block-primary modal-block-sm mfp-hide">
        <section class="card">
            <div class="card-body text-center">
                <h3 class="mt-0 mb-4">{{ __('Anda Yakin?') }}</h3>
                <p>
                    {{ __('Data ini akan dihapus secara permanen. Sehingga Anda
                    tidak akan bisa mengembalikannya lagi.') }}
                </p>
                <div class="modal-action mt-3">
                    <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                    <button type="submit" class="btn btn-danger delete-business"><i class="icon icon-trash"></i> {{ __('Ya, hapus') }}</button>
                </div>
            </div>
        </section>
    </div>

@endsection
@section('blockfoot')
    <script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            /*
            Datatable : List 'Bidang Usaha'
            */
            var $table = $('.table');
            var table = $table.DataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                processing: true,
                "columnDefs": [
                    { "orderable": false, "targets": 6 }
                ],
                "ajax": {
                    "url": "{{ route('usaha.data') }}",
                    "type": "GET",
                    "serverSide": true
                },
            });

            /*
            Modal : Delete Business Category
            */
            $('.modal-basic').magnificPopup({
                type: 'inline',
                preloader: false,
                modal: true
            });

            /*
            Modal Dismiss : Delete Business Category
            */
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });

            /*
            Navigation Active
            */
            $('.business').addClass('nav-expanded nav-active');
            $('.all-business').addClass('nav-active');

            /*
            SELECT2 IN MODAL
            */
            (function($) {
                'use strict';
                if ( $.isFunction($.fn[ 'select2' ]) ) {
                    $(function() {
                        $('[data-plugin-selectTwo-editBusiness]').each(function() {
                            var $this = $( this ),
                                opts = {
                                    dropdownParent: $("#detailBusiness")
                                };
                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;
                            $this.themePluginSelect2(opts);
                        });
                    });
                }
            }).apply(this, [jQuery]);

            $(".val").css("display", "block").delay(2000).fadeOut()

        });
        /*
        Modal : Eeeeediiiiiiiiiit
        */
        function editData(id_business) {
            // alert('edit')
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#detailBusiness'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        
                        $.ajax({
                            url: "/admin/usaha/detail/" + id_business,
                            type: "GET",
                            dataType: "JSON",
                            success: function(data){
                                $('.member_name').html(data.fullname);
                                $('.member_name').attr('href', 'http://app.local:1231/admin/anggota/detail/' + data.id_user);
                                $('#id_business').val(data.id_business);
                                $('#id_bsfield').val(data.id_bsfield).trigger('change');
                                $('#business_name').val(data.business_name);
                                $('#brand').val(data.brand);
                                $('#business_contact').val(data.business_contact);
                                $('#desc').val(data.desc);
                                $('#address').val(data.address);
                                $('#website').val(data.website);
                                $('#form-business').attr('action', "/admin/usaha/update/"+id_business);
                            },
                            error: function(data){
                                alert("Error retrieving data.");
                            }
                        });
                        
                    }
                }
            });
        }

        // Modal : delete business
        function deleteData(id_business) {
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#deleteBusiness'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        $(".delete-business").on('click', function(){
                            $.ajax({
                                url: "/admin/usaha/hapus/"+id_business,
                                type: "POST",
                                data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                                success: function (data) {
                                    $('.alert').addClass('alert-success');
                                    $('.msg').append('<br/> Data telah berhasil dihapus.')
                                    $(".alert").css("display", "block").delay(2000).fadeOut()
                                    $('.table').DataTable().ajax.reload()
                                    $.magnificPopup.close()
                                },
                                error: function () {
                                    $('.alert').addClass('alert-danger');
                                    $('.msg').append('<br/> Data telah gagal dihapus.')
                                    $(".alert").css("display", "block").delay(2000).fadeOut()
                                    $('.table').DataTable().ajax.reload()
                                    $.magnificPopup.close()
                                }
                            });                            
                        });
                    }
                }
            });
        }
    </script>
@endsection