@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <header class="page-header">
        <h2>{{ __('Dashboard') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <section class="card" style="min-height: 800px;">
        <div class="card-header">{{ __('Dashboard') }}</div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <p>You are logged in!</p>

            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente eum maxime quasi harum sed tenetur sunt dignissimos aperiam voluptas ab, sequi in dicta quisquam dolorum, deserunt obcaecati, omnis doloribus eveniet.
        </div>
    </section>
@endsection