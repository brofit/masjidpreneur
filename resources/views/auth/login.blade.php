@extends('auth.layout')
@section('title', 'Masuk')
@section('content')
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group mb-3">
            <label for="email"><strong>{{ __('Alamat email') }}</strong></label>
            <input type="email" name="email" id="email" class="form-control form-control-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required autofocus />
            @if ($errors->has('email'))
            <div class="invalid-feedback" role="alert">{{ $errors->first('email') }}</div>
            @endif
        </div>
        <div class="form-group mb-3">
            <div class="clearfix">
                <label for="password" class="float-left"><strong>{{ __('Kata sandi') }}</strong></label>
                @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}" class="float-right">{{ __('Lupa kata sandi?') }}</a>
                @endif
            </div>
            <input type="password" name="password" id="password" class="form-control form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required />
            @if ($errors->has('password'))
            <div class="invalid-feedback" role="alert">{{ $errors->first('password') }}</div>
            @endif
        </div>
        <div class="row mb-3">
            <div class="col-8">
                <div class="checkbox-custom checkbox-default">
                    <input id="RememberMe" name="rememberme" type="checkbox" {{ old('remember') ? 'checked' : '' }} />
                    <label for="RememberMe">{{ __('Ingat Saya') }}</label>
                </div>
            </div>
            <div class="col-4 text-right">
                <button type="submit" class="btn btn-primary btn-lg mt-2"><i class="icon icon-login"></i> {{ __('Login') }}</button>
            </div>
        </div>

        <p class="text-center mb-0">{{ __('Tidak mempunyai akun?') }} <a href="{{ URL('pendaftaran') }}">{{ __('Daftar sekarang!') }}</a></p>
    </form>
@endsection
