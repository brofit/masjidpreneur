@extends('auth.layout')
@section('title', 'Lupa Kata Sandi')
@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="alert alert-success" role="alert">
            {{ __('Masukkan email Anda di bawah ini dan kami akan mengirimkan Anda petunjuk pemulihan akun!') }}
        </div>
        <div class="form-group">
            <input 
                id="email" 
                type="email" 
                class="form-control @error('email') is-invalid @enderror" 
                name="email" value="{{ old('email') }}" 
                required 
                autocomplete="email" 
                autofocus>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary btn-block mt-4 mb-4"><i class="icon icon-paper-plane"></i> {{ __('Kirimkan saya link untuk pemulihan akun') }}</button>
        <p class="text-center mb-0">{{ __('Sudah ingat kata sandi?') }}<a href="{{ url('/login') }}"> Masuk sekarang!</a></p>
    </form>
@endsection
