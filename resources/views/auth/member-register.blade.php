@extends('frontpage.layouts.app')
@section('title', 'Pendaftaran Anggota')
@section('blockhead')
@endsection
@section('content')
    <section class="tbr_coming_soon_wrap" style="background-image: url('{{ asset('assets/images/coming_soon_background.jpg') }}');">
        <div class="tbr_sc_overlay"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="tbr_sc_content">
                        <h1 class="tbr_sc_heading">Comming Soon</h1>
                        <p class="mb-0">An awesome new company for web resources is coming very soon.</p>
                        <p>Enter your email to request an eraly invitation!</p>
                        <form action="" method="">
                            <div class="input-group tbr_advanced_form_control">
                                <input type="text" class="form-control" aria-describedby="tbr_btn_addon_search_dir" placeholder="Enter email address">
                                <div class="input-group-append">
                                    <button class="btn btn-purple" type="submit" id="tbr_btn_addon_search_dir"><i class="icon icon-paper-plane"></i> <span>Submit</span></button>
                                </div>
                            </div>
                        </form>
                        <a href="" class="btn btn-primary"><i class="icon icon-home"></i> Back to Home</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('blockfoot')
@endsection