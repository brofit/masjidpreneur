@extends('layouts.app')
@section('title', 'Tambah Anggota')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}">
@endsection
@section('content')
    <header class="page-header">
        <h2>{{ __('Tambah Anggota') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    
    <div class="row">
        <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">
            <section class="card">
                <div class="card-body">
                    <div class="thumb-info">
                        <img src="{{ asset('assets/images/!logged-user-lg.jpg') }}" class="rounded img-fluid" alt="John Doe">
                    </div>
                </div>
            </section>
        </div>

        <div class="col-lg-8 col-xl-9">
            <section class="card">
                <div class="card-body">

                    @if(session('status'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" 
                                class="close" 
                                data-dismiss="alert" 
                                aria-hidden="true"><i class="icon icon-close"></i></button>
                                <strong>{{ __('Berhasil!') }}</strong>
                                {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" 
                                class="close" 
                                data-dismiss="alert" 
                                aria-hidden="true"><i class="icon icon-close"></i></button>
                                <strong>{{ __('Gagal!') }}</strong>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif

                    <form id="form-content" method="POST" enctype="multipart/form-data" class="forms" action="{{ route('member.store') }}">
                        @csrf @method('POST')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <input name="id_user" id="id_user" type="hidden">
                                    <label for="fullname" class="control-label"><strong>{{ __('Nama lengkap') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="fullname" 
                                        id="fullname" 
                                        type="text" 
                                        class="form-control"
                                        value="{{ old('fullname') }}"
                                        required>
                                        @if($errors->has('fullname'))
                                        <div class="text-danger">
                                            {{ $errors->first('fullname')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="member_contact" class="control-label"><strong>{{ __('Nomor telpon') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="member_contact" 
                                        id="member_contact" 
                                        type="text" 
                                        class="form-control" 
                                        value="{{ old('member_contact') }}"
                                        required>
                                        @if($errors->has('member_contact'))
                                        <div class="text-danger">
                                            {{ $errors->first('member_contact')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="dateofbirth" class="control-label"><strong>{{ __('Tanggal lahir') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="dateofbirth" 
                                        id="dateofbirth" 
                                        type="text" 
                                        class="form-control" 
                                        data-plugin-datepicker 
                                        value="{{ old('dateofbirth') }}"
                                        required>
                                    @if($errors->has('dateofbirth'))
                                    <div class="text-danger">
                                        {{ $errors->first('dateofbirth')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="hometown" class="control-label"><strong>{{ __('Kota lahir') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="hometown" 
                                        id="hometown" 
                                        type="text" 
                                        class="form-control" 
                                        value="{{ old('hometown') }}"
                                        required>
                                        @if($errors->has('hometown'))
                                        <div class="text-danger">
                                            {{ $errors->first('hometown')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="province" class="control-label"><strong>{{ __('Provinsi') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select data-plugin-selectTwo 
                                        class="form-control" 
                                        name="province" 
                                        id="province">
                                        <option disabled selected>{{ __('Pilih provinsi') }}</option>
                                        @foreach ($provinces as $prov)
                                            <option value="{{ $prov->id }}">{{ $prov->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('province'))
                                    <div class="text-danger">
                                        {{ $errors->first('province')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="regency" class="control-label"><strong>{{ __('Kabupaten') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select data-plugin-selectTwo 
                                        class="form-control" 
                                        name="regency" 
                                        id="regency">
                                        <option disabled selected>{{ __('Pilih kabupaten') }}</option>
                                    </select>
                                    @if($errors->has('regency'))
                                    <div class="text-danger">
                                        {{ $errors->first('regency')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="district" class="control-label"><strong>{{ __('Kecamatan') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select data-plugin-selectTwo 
                                        class="form-control" 
                                        name="district" 
                                        id="district">
                                        <option disabled selected>{{ __('Pilih kecamatan') }}</option>
                                    </select>
                                    @if($errors->has('district'))
                                    <div class="text-danger">
                                        {{ $errors->first('district')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="address" class="control-label"><strong>{{ __('Alamat') }}</strong> {{ __('(optional)') }}</label>
                                    <input type="text" 
                                        name="address" 
                                        id="address" 
                                        class="form-control" 
                                        value="{{ old('address') }}"
                                        required>
                                        <p class="help-block">{{ __('Alamat bisa berupa jalan, perumahan, apartemen, lantai ke...') }}</p>
                                        @if($errors->has('address'))
                                        <div class="text-danger">
                                            {{ $errors->first('address')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="gender"><strong>{{ __('Jenis kelamin') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="radio-custom radio-primary form-check-inline">
                                                <input type="radio" 
                                                    name="gender" 
                                                    id="radio-male" 
                                                    value="Laki - Laki" 
                                                    checked>
                                                    <label for="radio-male" class=""> {{ __('Laki - Laki ') }}</label>
                                                    @if($errors->has('gender'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('gender')}}
                                                    </div>
                                                    @endif
                                            </div>
                                            <div class="radio-custom radio-primary form-check-inline">
                                                <input type="radio" 
                                                    name="gender" 
                                                    id="radio-female" 
                                                    value="Perempuan">
                                                    <label for="radio-female" class=""> {{ __('Perempuan ') }}</label>
                                                    @if($errors->has('gender'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('gender')}}
                                                    </div>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="group" class="control-label"><strong>{{ __('Group') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <select data-plugin-selectTwo 
                                        class="form-control" 
                                        name="group" 
                                        id="group">
                                        <option value="" disabled>{{ __('Pilih group') }}</option>
                                        <option value="Anggota Biasa" selected>Anggota Biasa</option>
                                        <option value="Anggota Kehormatan">Anggota Kehormatan</option>
                                        <option value="Pembina">Pembina</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="avatar"><strong>{{ __('Foto profil') }}</strong> {{ __('(optional)') }}</label>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="input-append">
                                                    <div class="uneditable-input">
                                                        <i class="fa fa-file fileupload-exists"></i>
                                                        <span class="fileupload-preview"></span>
                                                    </div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileupload-exists">Ganti foto</span>
                                                        <span class="fileupload-new" tabindex="12">Unggah foto</span>
                                                        <input type="file" 
                                                            id="avatar" 
                                                            name="avatar"
                                                            value="{{ old('avatar') }}"/>
                                                    </span>
                                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Hapus</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr class="divider mb-1">
                        <p class="m-0"><em><i class="icon icon-user"></i> Data akun dan autentifikasi anggota</em></p>
                        <hr class="divider mt-1">

                        <!-- {{-- AUTHENTICATION --}} -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="email" class="control-label"><strong>{{ __('Alamat email') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="email" 
                                        id="email" 
                                        type="email" 
                                        class="form-control" 
                                        value="{{ old('email') }}"
                                        required>
                                        @if($errors->has('email'))
                                        <div class="text-danger">
                                            {{ $errors->first('email')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="username" class="control-label"><strong>{{ __('Nama pengguna') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input name="username" 
                                        id="username" 
                                        type="text" 
                                        class="form-control" 
                                        value="{{ old('username') }}"
                                        required>
                                        @if($errors->has('username'))
                                        <div class="text-danger">
                                            {{ $errors->first('username')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-2">
                                    <label for="password"><strong>{{ __('Kata sandi baru') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="password" 
                                        class="form-control" 
                                        name="password" 
                                        id="password" 
                                        tabindex="13"
                                        value="{{ old('password') }}"
                                        required>
                                        @if($errors->has('password'))
                                        <div class="text-danger">
                                            {{ $errors->first('password')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="re_password"><strong>{{ __('Tulis ulang') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <input type="password" 
                                        class="form-control" 
                                        name="re_password" 
                                        id="re_password" 
                                        tabindex="14"
                                        value="{{ old('re_password') }}"
                                        required>
                                        @if($errors->has('re_password'))
                                        <div class="text-danger">
                                            {{ $errors->first('re_password')}}
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-2">
                                    <label for="member_status"><strong>{{ __('Status') }}</strong> {{ __('(harus diisi)') }}</label>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="radio-custom radio-primary form-check-inline mr-4">
                                                <input type="radio" 
                                                    name="member_status" 
                                                    id="active" 
                                                    value="Aktif" 
                                                    checked>
                                                    <label for="active" class=""> {{ __('Aktif') }}</label>
                                                    @if($errors->has('member_status'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('member_status')}}
                                                    </div>
                                                    @endif
                                            </div>
                                            <div class="radio-custom radio-primary form-check-inline">
                                                <input type="radio" 
                                                    name="member_status" 
                                                    id="deactive" 
                                                    value="Tidak Aktif">
                                                    <label for="deactive" class=""> {{ __('Nonaktifkan') }}</label>
                                                    @if($errors->has('member_status'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('member_status')}}
                                                    </div>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right mt-4">
                            <a href="#" class="btn btn-default go-back"><i class="icon icon-arrow-left-circle"></i> {{ __('Kembali') }}</a>
                            <button type="submit" class="btn btn-primary" tabindex="15"><i class="icon icon-doc"></i> {{ __('Simpan') }}</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('blockfoot')
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            /** Tampilkan datatables */
            $.ajax({
                url: "{{ route('user.id_user')}}",
                type: "GET",
                dataType: "JSON",
                success: function(data){
                    $("#id_user").val(data);
                },
                error: function(data){
                    alert('ID Anggota gagal ditampilkan!');
                    console.log(data);
                }
            });

            $('#province').on('change', function(e){
                // console.log(e);
                var id_province = e.target.value;
                $.get('kabupaten/' + id_province, function(data){
                    // console.log(data);                    
                    $('#regency').empty();
                    $('#regency').append('<option value="0" disabled selected>Pilih kabupaten</option>');
                    
                    $('#district').empty();
                    $('#district').append('<option value="0" disabled selected>Pilih kecamatan</option>');
                
                    $.each(data, function(index, regenciesObj){
                        $('#regency').append('<option value="'+ regenciesObj.id +'">'+ regenciesObj.name +'</option>');
                    });
                });
            });
            
            $('#regency').on('change', function(e){
                // console.log(data);
                var id_regency = e.target.value;
                $.get('kecamatan/' + id_regency, function(data){
                    $('#district').empty();
                    $('#district').append('<option value="0" disabled selected>Pilih kecamatan</option>');
                    
                    $.each(data, function(index, districtsObj){
                        $('#district').append('<option value="'+ districtsObj.id +'">'+ districtsObj.name +'</option>');
                    });
                });
            });
            
            $('.form').on('submit', function (e) {
                var data  = $(this).parents("form").serialize();
                if (!e.isDefaultPrevented()) {
                    var url = "{{ route('member.store') }}";
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: new FormData($(".form")[0]),
                        async: true,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            window.location.href = "{{ route('semua-anggota') }}";
                        },
                        error: function () {
                            alert('Gagal menyimpan data anggota!');
                        }
                    });
                    return false;
                }
            });

            /*
            Navigation Active
            */
            $('.member').addClass('nav-expanded nav-active');
            $('.add-member').addClass('nav-active');

            /*
            GO PREVIOUS PAGE
            */
            $(document).ready(function() {
                $('a.go-back').click(function () {
                    parent.history.back();
                    return false;
                });
            });

            $("#cek").on('click', function(){
                // alert('cek');
                // var tgl = $("#dateofbirth").val();
                // alert(tgl);
            });

            // $(".alert").fadeTo(3000, 1).fadeOut(1000, function(){
            //     $(".alert").fadeOut(6000);
            // });
        });
    </script>
@endsection