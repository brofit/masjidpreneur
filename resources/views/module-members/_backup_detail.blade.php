@extends('layouts.app')
@section('title', 'Detail Anggota')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Detail Anggota') }}</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li><i class="fa fa-home"></i></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<section class="card" style="min-height: 800px;">
    <div class="card-header">{{ __('Anggota') }}</div>
    <form action="" id="form-content" method="POST" class="form-horizontal form-bordered" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card-body">
            <!-- Modal Upload -->
            <div name="modal-upload" id="modal-upload" class="modal-block mfp-hide">
                {{ csrf_field() }}
                <section class="card">
                    <div class="card-body">
                        <div class="card-actions">
                            <a href="#" class="card-action-dismiss modal-dismiss"></a>
                        </div>
                        <div class="modal-wrapper text-center">
                            <a href="#file" id="upload_link">
                                <img src="{{ asset('assets/images/!logged-user-lg.jpg') }}" alt="user-photo"
                                    style="width: 200px; height: 200px;">
                            </a>
                        </div>
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                            {{ $error }} <br />
                            @endforeach
                        </div>
                        @endif
                    </div>
                    <footer class="card-footer">
                        <div class="row">
                            <div class="col-md-12 text-center m-auto">
                                <button class="btn btn-default modal-dismiss">{{ __('Batalkan') }}</button>
                                <button class="btn btn-primary">{{ __('Simpan') }}</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>
            <!-- END Modal Upload -->
            <div class="row">
                <div class="col-md-3 mb-3">
                    <div class="thumb-info mb-3">
                        <img src="{{ asset('assets/images/!logged-user.jpg') }}" class="rounded img-fluid" alt="John Doe">
                        <div class="thumb-info-title">
                            <span class="thumb-info-inner">John Doe</span>
                            <span class="thumb-info-type">CEO</span>
                        </div>
                    </div>
                    <div class="widget-toggle-expand mb-3 widget-collapsed p-2">
                        <div class="widget-header">
                            <h5 class="mb-2">Ganti foto profil</h5>
                            <!-- <div class="widget-toggle">+</div> -->
                            <input type="file" name="foto">
                        </div>
                        <div class="widget-content-collapsed">
                            
                        </div>
                    </div>
                    <div class="widget-toggle-expand mb-3 widget-collapsed border p-2 text-center">
                        <div class="widget-header">
                            <h5 class="mb-2">ID Anggota</h5>
                        </div>
                        <div class="widget-content-collapsed">
                            <label>AGT-0001</label>
                        </div>
                    </div>
                    <div class="widget-toggle-expand mb-3 widget-collapsed border p-2 text-center">
                        <div class="widget-header">
                            <h5 class="mb-2">Dibuat pada</h5>
                        </div>
                        <div class="widget-content-collapsed">
                            <label>02-09-2019 at 16:00 </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-9 mb-3">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="nama" class="control-label pt-2">{{ __('Nama lengkap (harus diisi)') }}</label>
                            <input name="nama" id="nama" type="text" class="form-control" placeholder="Nama lengkap">
                            @if($errors->has('nama'))
                            <div class="text-danger">
                                {{ $errors->first('nama')}}
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="uname" class="control-label pt-2">{{ __('Username (harus diisi)') }}</label>
                            <input name="uname" id="uname" type="text" class="form-control" placeholder="Username">
                            @if($errors->has('uname'))
                            <div class="text-danger">
                                {{ $errors->first('uname')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="email" class="control-label pt-2">{{ __('Email (harus diisi)') }}</label>
                            <input name="email" id="email" type="email" class="form-control" placeholder="Email">
                            @if($errors->has('email'))
                            <div class="text-danger">
                                {{ $errors->first('email')}}
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="hp" class="control-label pt-2">{{ __('Nomor telpon (harus diisi)') }}</label>
                            <input name="hp" id="hp" type="number" class="form-control"
                                placeholder="Nomor yang bisa di hubungi">
                            @if($errors->has('hp'))
                            <div class="text-danger">
                                {{ $errors->first('hp')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="date" class="control-label pt-2">{{ __('Tanggal lahir (harus diisi)') }}</label>
                            <input name="date" id="date" type="date" class="form-control">
                            @if($errors->has('date'))
                            <div class="text-danger">
                                {{ $errors->first('date')}}
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="kota" class="control-label pt-2">{{ __('Kota lahir (harus diisi)') }}</label>
                            <select data-plugin-selectTwo class="form-control" name="kota" id="kota">
                                <option value="" disabled>{{ __('Pilih Kota') }}</option>
                            </select>
                            @if($errors->has('kota'))
                            <div class="text-danger">
                                {{ $errors->first('kota')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="prov" class="control-label pt-2">{{ __('Provinsi (harus diisi)') }}</label>
                            <select data-plugin-selectTwo class="form-control" name="prov" id="prov">
                                <option value="" disabled>{{ __('Pilih Provinsi') }}</option>
                            </select>
                            @if($errors->has('prov'))
                            <div class="text-danger">
                                {{ $errors->first('prov')}}
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="kab" class="control-label pt-2">{{ __('Kabupaten/Kota (harus diisi)') }}</label>
                            <select data-plugin-selectTwo class="form-control" name="kab" id="kab">
                                <option value="" disabled>{{ __('Pilih Kabupaten') }}</option>
                            </select>
                            @if($errors->has('kab'))
                            <div class="text-danger">
                                {{ $errors->first('kab')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="camat" class="control-label pt-2">{{ __('Kecamatan (harus diisi)') }}</label>
                            <select data-plugin-selectTwo class="form-control" name="camat" id="camat">
                                <option value="" disabled>{{ __('Pilih Kecamatan') }}</option>
                            </select>
                        </div>
                        @if($errors->has('camat'))
                        <div class="text-danger">
                            {{ $errors->first('camat')}}
                        </div>
                        @endif
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="alamat" class="control-label pt-2">{{ __('Alamat (optional)') }}</label>
                            <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat">
                            <span class="text-thin"> <i>
                                    {{ __('Alamat bisa berupa jalan, perumahan, apartemen, lantai ke... ') }}</i></span>
                        </div>
                        @if($errors->has('alamat'))
                        <div class="text-danger">
                            {{ $errors->first('alamat')}}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <span class="text-medium">{{ __('Jenis kelamin (harus diisi)') }}</b>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="radio-custom">
                                        <input type="radio" name="gender" id="radio-male" value="Laki-laki" checked>
                                        <label for="radio-male" class=""> {{ __('Laki-laki ') }}</label>
                                    </div>
                                    @if($errors->has('gender'))
                                    <div class="text-danger">
                                        {{ $errors->first('gender')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <div class="radio-custom">
                                        <input type="radio" name="gender" id="radio-female" value="Perempuan">
                                        <label for="radio-female" class=""> {{ __('Perempuan ') }}</label>
                                    </div>
                                    @if($errors->has('gender'))
                                    <div class="text-danger">
                                        {{ $errors->first('gender')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                    </div>
                    <div class="form-group">
                        <span class="text-medium">{{ __('Role (harus diisi)') }}</b>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="radio-custom">
                                        <input type="radio" name="status" id="radio-admin" value="Admin"
                                            data-toggle="collapse" data-target="#collapse-role" checked>
                                        <label for="radio-admin" class=""> {{ __('Admin ') }}</label>
                                    </div>
                                    @if($errors->has('status'))
                                    <div class="text-danger">
                                        {{ $errors->first('status')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <div class="radio-custom">
                                        <input type="radio" name="status" id="radio-member" value="Anggota"
                                            data-toggle="collapse" data-target="#collapse-role">
                                        <label for="radio-member" class=""> {{ __('Anggota (Member)') }} </label>
                                    </div>
                                    @if($errors->has('status'))
                                    <div class="text-danger">
                                        {{ $errors->first('status')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            {{-- Accordion --}}
                            <div id="accordion">
                                <div id="collapse-role" class="collapse">
                                    <div class="row mb-3 accordion-add">
                                        <div class="col-md-6 p-3">
                                            <label for="bidang"
                                                class="control-label pt-2">{{ __('Pilih bidang usaha (harus diisi)') }}</label>
                                            <select data-plugin-selectTwo data-plugin-options='{"width":"100%"}'
                                                name="bidang" id="bidang" class="form-control">
                                                <option value="Bidang Usaha">{{ __('Bidang Usaha') }}</option>
                                            </select>
                                            @if($errors->has('bidang'))
                                            <div class="text-danger">
                                                {{ $errors->first('bidang')}}
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6 p-3">
                                            <label for="usaha"
                                                class="control-label pt-2">{{ __('Pilih usaha (harus diisi)') }}</label>
                                            <select data-plugin-selectTwo data-plugin-options='{"width":"100%"}'
                                                name="usaha" id="usaha" class="form-control">
                                                <option value="Usaha">{{ __('Usaha') }}</option>
                                            </select>
                                            @if($errors->has('usaha'))
                                            <div class="text-danger">
                                                {{ $errors->first('usaha')}}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Accordion --}}
                    </div>
                    <div class="form-group">
                        <span class="text-medium">{{ __('Status Anggota (harus diisi)') }}</b>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="radio-custom">
                                        <input type="radio" name="stat" id="radio-active" value="Active" checked>
                                        <label for="radio-active" class="">{{ __('Active') }} </label>
                                        @if($errors->has('stat'))
                                        <div class="text-danger">
                                            {{ $errors->first('stat')}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio-custom">
                                        <input type="radio" name="stat" id="radio-deactive" value="Deactive">
                                        <label for="radio-deactive" class=""> {{ __('Deactive ') }}</label>
                                    </div>
                                    @if($errors->has('stat'))
                                    <div class="text-danger">
                                        {{ $errors->first('stat')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="pass" class="control-label pt-2">{{ __('Kata sandi (optional)') }}</label>
                            <input type="password" name="pass" id="pass" class="form-control">
                            <span class="text-thin"> <i> {{ __('Biarkan tetap kosong jika tidak ingin mengganti') }}
                                </i></span>
                            @if($errors->has('pass'))
                            <div class="text-danger">
                                {{ $errors->first('pass')}}
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="repass"
                                class="control-label pt-2">{{ __('Tulis ulang kata sandi (harus diisi)') }}</label>
                            <input type="password" name="pass2" id="pass2" class="form-control">
                            <span class="text-thin"> <i> {{ __('Biarkan tetap kosong jika tidak ingin mengganti') }}
                                </i></span>
                            @if($errors->has('pass'))
                            <div class="text-danger">
                                {{ $errors->first('pass')}}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="card-footer text-right">
            <button type="reset" class="btn btn-default"> {{ __('Kembali ') }}</button>
            <button type="submit" class="btn btn-primary mx-1"> {{ __('Simpan ') }}</button>
        </footer>
    </form>
</section>
@endsection

@section('blockfoot')
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $('.beranda').removeClass('nav-active');
        $('.semua-anggota').removeClass('nav-active');

        $('.anggota').addClass('nav-expanded');
        $('.tambah-anggota').addClass('nav-active');
    });

</script>
@endsection
