@extends('layouts.app')
@section('title', 'Semua Anggota')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Semua Anggota') }}</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li><i class="fa fa-home"></i></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<section class="card">
    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <table class="table table-hover table-sm table-member" id="datatable-default" role="grid" style="width: 930px;">
            <thead>
                <tr>
                    <th>{{ __('No') }}</th>
                    <th>{{ __('ID') }}</th>
                    <th>{{ __('Nama') }}</th>
                    <th>{{ __('Username') }}</th>
                    <th>{{ __('Email') }}</th>
                    <th>{{ __('B. Usaha') }}</th>
                    <th>{{ __('Usaha') }}</th>
                    <th>{{ __('Gender') }}</th>
                    <th>{{ __('Aksi') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>#U201909099999</td>
                    <td>brofit</td>
                    <td>brofitjr</td>
                    <td>brofitjp@gmail.com</td>
                    <td>Pertanian</td>
                    <td>10</td>
                    <td>Laki - Laki</td>
                    <td class="actions">
                        <a href=""><i class="icon icon-info"></i></a>
                        <a href="#deleteMember" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>{{ __('No') }}</th>
                    <th>{{ __('ID') }}</th>
                    <th>{{ __('Nama') }}</th>
                    <th>{{ __('Username') }}</th>
                    <th>{{ __('Email') }}</th>
                    <th>{{ __('B. Usaha') }}</th>
                    <th>{{ __('Usaha') }}</th>
                    <th>{{ __('Gender') }}</th>
                    <th>{{ __('Aksi') }}</th>
                </tr>
            </tfoot>
        </table>
    </div>
</section>

{{-- Modal : Delete Member --}}
<div id="deleteMember" class="modal-block modal-block-primary modal-block-sm mfp-hide">
    <section class="card">
        <div class="card-body text-center">
            <h3 class="mt-0 mb-4">{{ __('Anda Yakin?') }}</h3>
            <p>
                {{ __('Data ini akan dihapus secara permanen. Sehingga Anda
                    tidak akan bisa mengembalikannya lagi.') }}
            </p>
            <div class="modal-action mt-3">
                <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i>
                    {{ __('Batalkan') }}</button>
                <button type="submit" class="btn btn-danger delete-data"><i class="icon icon-trash"></i>
                    {{ __('Ya, hapus') }}</button>
            </div>
        </div>
    </section>
</div>
@endsection
@section('blockfoot')
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        /*
        Datatable : List 'Anggota'
        */
        var $table = $('.table');
        var table = $table.DataTable({
            sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
            processing: true,
            ordering: false,
            autoWidth: false,
            "columnDefs": [{
                "orderable": false,
                "targets": 6
            }],
            "ajax": {
                "url": "{{ route('member.data') }}",
                "type": "GET",
                "serverSide": true
            },
        });

        /*
        Modal : Delete Business Category
        */
        $('.modal-basic').magnificPopup({
            type: 'inline',
            preloader: false,
            modal: true
        });

        /*
        Modal Dismiss : Delete Business Category
        */
        $(document).on('click', '.modal-dismiss', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });

        /*
        Navigation Active
        */
        $('.member').addClass('nav-expanded nav-active');
        $('.all-member').addClass('nav-active');
    });

    /*
    Modal : Delete 
    */
    function deleteData(id_member) {
        // alert(id)
        $.magnificPopup.open({
            modal: true,
            items: {
                src: $('#deleteMember'),
                type: 'inline',
            },
            callbacks: {
                open: function () {
                    $(".delete-data").on('click', function(){
                        // alert('del');
                        $.ajax({
                            url: "/admin/anggota/hapus/"+id_member,
                            type: "POST",
                            data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                            success: function () {
                                $(".alert-success").css("display", "block").delay(2000).fadeOut();
                                $.magnificPopup.close();
                                $('.table').DataTable().ajax.reload();
                                console.log()
                            },
                            error: function () {    
                                // $(".alert-danger").css("display", "block").delay(2000).fadeOut();
                                console.log()
                            }
                        });                            
                    });
                }
            }
        });
    }
</script>
@endsection
