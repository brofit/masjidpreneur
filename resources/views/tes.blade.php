<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-body">
            <table class="table table-bordered table-sm" id="table">
                <thead>
                    <tr>
                        <!--  No|ID|Nama|Username|Email|B. Usaha|Usaha|Gender|Aksi -->
                        <th>ID User</th>
                        <th>Pengguna</th>
                        <th>Fullname</th>
                        <th>Email</th>
                        <th>Telp</th>
                        <th>Bidang Usaha</th>
                        <th>Usaha</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $u)
                    <tr>
                        <td>{{ $u->id_user }}</td>
                        <td>{{ $u->user_name }}</td>
                        <td>{{ $u->fullname }}</td>
                        <td>{{ $u->email }}</td>
                        <td>{{ $u->members->dateofbirth }}</td>
                        <td>
                            @foreach($u->business as $b)
                            {{ $b->business_fields->bsfields_name }} <br/>
                            @endforeach
                        </td>
                        <td>
                            {{$u->business->count()}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(function(){
        $('.table').DataTable({
            processing: true,
            ordering: true,
            autoWidth: false,
            "columnDefs": [{
                "orderable": false,
                "targets": 6
            }],
        });
    });
</script>

</body>
</html>