@extends('layouts.app')
@section('title', 'Profil Saya')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/cropper/cropper.min.css') }}" />
@endsection
@section('content')
    <header class="page-header">
        <h2>{{ __('Detail Anggota') }}</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li><i class="fa fa-home"></i></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    
    <div class="row">
        <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">
            <section class="card mb-3">
                <div class="card-body">
                    <div class="avatar-area">   
                        <label class="label mb-0" data-toggle="tooltip" title="Ganti foto profil">
                            @if(Auth::user()->avatar)
                            <img src="{{ asset('storage/avatars/' . Auth::user()->avatar ) }}" 
                                class="rounded img-fluid change-avatar" 
                                alt="Photo Profile">
                            @else
                            <img src="{{ asset('assets/images/!logged-user-lg.jpg') }}" 
                                class="rounded change-avatar" 
                                alt="avatar" 
                                width="100%" 
                                height="100%">
                            @endif
                            <input type="file" 
                                class="sr-only" 
                                id="input" 
                                name="avatar" 
                                accept="image/*">
                            <span class="btn btn-primary btn-change-avatar btn-file"><i class="icon icon-pencil"></i></span>
                        </label>
                        <div class="progress mt-2 mb-0">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" 
                                role="progressbar" 
                                aria-valuenow="0" 
                                aria-valuemin="0" 
                                aria-valuemax="100">{{ __('0%') }}</div>
                        </div>
                        <div class="alert mt-2 mb-0" role="alert"></div>
                    </div>
                </div>
            </section>
        </div>

        <div class="col-lg-8 col-xl-9">
            <div class="tabs">
                <!-- {{-- TAB NAVIGATION --}} -->
                <ul class="nav nav-tabs">
                    <li class="nav-item active"><a class="nav-link" href="#setting-general" data-toggle="tab"><i class="icon icon-user"></i> Data pribadi</a></li>
                    <li class="nav-item "><a class="nav-link" href="#programs" data-toggle="tab"><i class="icon icon-notebook"></i> Program</a></li>
                    <li class="nav-item "><a class="nav-link" href="#business" data-toggle="tab"><i class="icon icon-pin"></i> Usaha</a></li>
                    <li class="nav-item "><a class="nav-link" href="#authentications" data-toggle="tab"><i class="icon icon-settings"></i> Pengaturan akun</a></li>
                </ul>
                <!-- {{-- TAB CONTENTS --}} -->
                <div class="tab-content">
                    <!-- tab-content : profile -->
                    <div id="setting-general" class="tab-pane active">
                        <!-- tab-content : setting general -->
                        <form action="{{ route('profil.member.update', Auth::user()->id_user) }}" id="form-content" method="POST" enctype="multipart/form-data">
                            @csrf @method('PATCH')
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group mb-2">
                                        <label for="fullname" class="control-label"><strong>{{ __('Nama lengkap') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <input name="fullname" 
                                            id="fullname" 
                                            type="text" 
                                            class="form-control"
                                            value="{{ $user->fullname }}">
                                        <input 
                                            type="hidden" 
                                            name="id_user" 
                                            id="id_user"
                                            value="{{ $user->id_user }}">
                                        @if($errors->has('fullname'))
                                        <div class="text-danger">
                                            {{ $errors->first('fullname')}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="member_contact" class="control-label"><strong>{{ __('Nomor telpon') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <input name="member_contact" 
                                            id="member_contact" 
                                            type="text" 
                                            class="form-control"
                                            value="{{ $user->member_contact }}">
                                            @if($errors->has('member_contact'))
                                            <div class="text-danger">
                                                {{ $errors->first('member_contact')}}
                                            </div>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="dateofbirth" class="control-label"><strong>{{ __('Tanggal lahir') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <input name="dateofbirth" 
                                            id="dateofbirth" 
                                            type="text" 
                                            class="form-control" 
                                            data-plugin-datepicker
                                            value="{{ date('d/m/Y', strtotime($user->members->dateofbirth)) }}">
                                            @if($errors->has('dateofbirth'))
                                            <div class="text-danger">
                                                {{ $errors->first('dateofbirth')}}
                                            </div>
                                            @endif
                                    </div>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="hometown" class="control-label"><strong>{{ __('Kota lahir') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <input name="hometown" 
                                            id="hometown" 
                                            type="text" 
                                            class="form-control" required="required"
                                            value="{{ $user->hometown }}">
                                            @if($errors->has('hometown'))
                                            <div class="text-danger">
                                                {{ $errors->first('hometown')}}
                                            </div>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="province" class="control-label"><strong>{{ __('Provinsi') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <select data-plugin-selectTwo 
                                            class="form-control" 
                                            name="province" 
                                            id="province">
                                            <option disabled>{{ __('Pilih provinsi') }}</option>
                                            @foreach ($provinces as $prov)
                                            <option value="{{ $prov->id }}" @if($prov->id == $user->id_pro) selected @endif>{{ $prov->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('province'))
                                        <div class="text-danger">
                                            {{ $errors->first('province')}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="regency" class="control-label"><strong>{{ __('Kabupaten') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <select data-plugin-selectTwo 
                                            class="form-control" 
                                            name="regency" 
                                            id="regency">
                                            <option selected disabled>{{ __('Pilih kabupaten') }}</option>
                                            @foreach($regencies as $reg)
                                            <option value="{{ $reg->id }}" @if($reg->id == $user->id_reg) selected @endif>{{ $reg->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('regency'))
                                        <div class="text-danger">
                                            {{ $errors->first('regency')}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="district" class="control-label"><strong>{{ __('Kecamatan') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <select data-plugin-selectTwo 
                                            class="form-control" 
                                            name="district" 
                                            id="district">
                                            <option selected disabled>{{ __('Pilih kecamatan') }}</option>
                                            @foreach($districts as $dis)
                                            <option value="{{ $dis->id }}" @if($dis->id == $user->id_dis) selected @endif>{{ $dis->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('district'))
                                        <div class="text-danger">
                                            {{ $errors->first('district')}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group mb-2">
                                        <label for="address" class="control-label"><strong>{{ __('Alamat') }}</strong> {{ __('(optional)') }}</label>
                                        <input type="text" 
                                            name="address" 
                                            id="address" 
                                            class="form-control"
                                            value="{{ $user->members->address }}">
                                            <p class="help-block">{{ __('Alamat bisa berupa jalan, perumahan, apartemen, lantai ke...') }}</p>
                                            @if($errors->has('address'))
                                            <div class="text-danger">
                                                {{ $errors->first('address')}}
                                            </div>
                                            @endif
                                    </div>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group mb-2">
                                        <label for="gender"><strong>{{ __('Jenis kelamin') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <div class="row">
                                            <div class="col-lg-12">

                                                <div class="radio-custom radio-primary form-check-inline mr-4">
                                                    <input type="radio" id="male" name="gender" 
                                                        value="Laki - Laki" tabindex="9" 
                                                        @if($user->members->gender == "Laki - Laki")checked @endif>
                                                    <label for="male" class=""> {{ __('Laki - Laki ') }}</label>
                                                    @if($errors->has('gender'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('gender')}}
                                                    </div>
                                                    @endif
                                                </div>
                                                <div class="radio-custom radio-primary form-check-inline">
                                                    <input type="radio" id="female" name="gender" 
                                                        value="Perempuan"@if($user->members->gender == "Perempuan") checked @endif>
                                                    <label for="female" class=""> {{ __('Perempuan ') }}</label>
                                                    @if($errors->has('gender'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('gender')}}
                                                    </div>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="divider mb-1">
                            <div class="text-right mt-4">
                                <a href="#" class="btn btn-default go-back"><i class="icon icon-arrow-left-circle"></i> {{ __('Kembali') }}</a>
                                <button type="submit" class="btn btn-primary" tabindex="13"><i class="icon icon-doc"></i> {{ __('Simpan') }}</button>
                            </div>
                        </form>
                    </div>

                    <!-- tab-content : business -->
                    <div id="business" class="tab-pane">
                        <div class="alert alert-default">
                            {{ __('Jika data usaha kosong maka secara otomatis status anggota ini menjadi tidak aktif.') }}</a>
                        </div>
                        <div class="alert alert-success" style="display:none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon icon-close"></i></button>
                            <strong>Berhasil!</strong> Usaha anggota berhasil disimpan.</a>
                        </div>
                        <div class="alert alert-danger" style="display:none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon icon-close"></i></button>
                            <strong>Gagal!</strong> Usaha anggota gagal disimpan.</a>
                        </div>
                        <a href="#addBusiness" class="btn btn-primary btn-sm modal-basic mb-3"><i class="icon icon-plus"></i> Tambah Usaha</a>
                        <table class="table table-hover table-sm business-table">
                            <thead>
                                <tr>
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Nama') }}</th>
                                    <th>{{ __('Bidang Usaha') }}</th>
                                    <th>{{ __('Dibuat pada') }}</th>
                                    <th>{{ __('Aksi') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Kelapa Sawit</td>
                                    <td>Pertanian (Agriculture)</td>
                                    <td>01/01/2016 at 02.00 PM</td>
                                    <td class="actions">
                                        <a href="#detailBusiness" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                        <a href="#deleteBusiness" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Kelapa Sawit</td>
                                    <td>Pertanian (Agriculture)</td>
                                    <td>01/01/2016 at 02.00 PM</td>
                                    <td class="actions">
                                        <a href="#detailBusiness" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                        <a href="#deleteBusiness" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Kelapa Sawit</td>
                                    <td>Pertanian (Agriculture)</td>
                                    <td>01/01/2016 at 02.00 PM</td>
                                    <td class="actions">
                                        <a href="#detailBusiness" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                        <a href="#deleteBusiness" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Nama') }}</th>
                                    <th>{{ __('Bidang Usaha') }}</th>
                                    <th>{{ __('Dibuat pada') }}</th>
                                    <th>{{ __('Aksi') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                    <!-- tab-content : programs -->
                    <div id="programs" class="tab-pane">
                        <div class="alert alert-default">
                            {{ __('Jika data program kosong maka secara otomatis status anggota ini menjadi tidak aktif.') }}
                        </div>
                        <div class="alert alert-success" style="display:none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon icon-close"></i></button>
                            <strong>Berhasil!</strong> Program anggota berhasil disimpan.</a>
                        </div>
                        <div class="alert alert-danger" style="display:none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon icon-close"></i></button>
                            <strong>Gagal!</strong> Program anggota gagal disimpan.</a>
                        </div>
                        <a href="#addProgram" class="btn btn-primary btn-sm modal-basic mb-3"><i class="icon icon-plus"></i> Tambah Program</a>
                        <table class="table table-hover table-sm program-table">
                            <thead>
                                <tr>
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Nama') }}</th>
                                    <th>{{ __('Kategori') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Aksi') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Workshop Animasi-Start Up The Business of Animation</td>
                                    <td>Workshop</td>
                                    <td width="90">Wajib</td>
                                    <td class="actions">
                                        <a href="#editProgram" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                        <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Workshop Design-Sharing Session UXiD Solo: Understanding Digital Product Experience</td>
                                    <td>Workshop</td>
                                    <td width="90">Tidak Wajib</td>
                                    <td class="actions">
                                        <a href="#editProgram" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                        <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>DILo Solo Roadshow Goes to Fakultas Ilmu Komputer Universitas Duta Bangsa</td>
                                    <td>DILo</td>
                                    <td width="90">Tidak Wajib</td>
                                    <td class="actions">
                                        <a href="#editProgram" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                        <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Pelatihan Startup Digital dan Bootcamp 2018: Penguatan Kemampuan Industri Berbasis Digital</td>
                                    <td>Workshop</td>
                                    <td width="90">Wajib</td>
                                    <td class="actions">
                                        <a href="#editProgram" class="modal-basic"><i class="icon icon-pencil"></i></a>
                                        <a href="#deleteProgram" class="delete-row modal-basic"><i class="icon icon-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Nama') }}</th>
                                    <th>{{ __('Kategori') }}</th>
                                    <th width="90">{{ __('Status') }}</th>
                                    <th>{{ __('Aksi') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                    <!-- tab-content : account setting -->
                    <div id="authentications" class="tab-pane">
                        <form action="{{ route('akun.profil.update', $user->id_user) }}" method="POST">
                            @csrf @method('PATCH')
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="email" class="control-label"><strong>{{ __('Alamat email') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <input name="email" 
                                            id="email" 
                                            type="email" 
                                            class="form-control"
                                            value="{{ $user->email }}">
                                            @if($errors->has('email'))
                                            <div class="text-danger">
                                                {{ $errors->first('email')}}
                                            </div>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="user_name" class="control-label"><strong>{{ __('Nama pengguna') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <input name="user_name" 
                                            id="user_name" 
                                            type="text" 
                                            class="form-control"
                                            value="{{ $user->user_name }}" 
                                            disabled>
                                            @if($errors->has('user_name'))
                                            <div class="text-danger">
                                                {{ $errors->first('user_name')}}
                                            </div>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="password"><strong>{{ __('Kata sandi baru') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <input type="password" 
                                            class="form-control" 
                                            name="password" 
                                            id="password" 
                                            tabindex="14">
                                        <p class="help-block mb-0">{{ __('Biarkan tetap kosong jika tidak ingin mengganti') }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group mb-2">
                                        <label for="re-password"><strong>{{ __('Tulis ulang') }}</strong> {{ __('(harus diisi)') }}</label>
                                        <input type="password" 
                                            class="form-control" 
                                            name="re_password" 
                                            id="re_password" 
                                            tabindex="15">
                                        <p class="help-block mb-0">{{ __('Biarkan tetap kosong jika tidak ingin mengganti') }}</p>
                                    </div>
                                </div>
                            </div>
                            <hr class="divider mb-1">
                            <div class="text-right mt-3">
                                <a href="#" class="btn btn-default go-back"><i class="icon icon-arrow-left-circle"></i> {{ __('Kembali') }}</a>
                                <button type="submit" class="btn btn-primary" tabindex="16"><i class="icon icon-doc"></i> {{ __('Simpan') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- {{-- Modal : Add Business --}} -->
    <div id="addBusiness" class="modal-block modal-block-primary modal-block-lg mfp-hide">
        <section class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3 class="mt-0 mb-2"><i class="icon icon-pin"></i> {{ __('Tambah Usaha') }}</h3>
                    <p>Silahkan isi untuk data usaha baru pada formulir di bawah ini.</p>
                </div>
                <form action="" method="POST" id="form-business" class="form-business">
                    @csrf @method('POST')
                    <input type="hidden" name="save_method" id="save_method" value="add">
                    <input type="hidden" name="id_user" id="id_user" value="{{ $user->id_user }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="business_name"><strong>{{ __('Nama usaha') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="business_name" 
                                    id="business_name" 
                                    tabindex="1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="id_bsfield"><strong>{{ __('Bidang usaha') }}</strong> {{ __('(harus diisi)') }}</label>
                                <select class="form-control" 
                                    name="id_bsfield" 
                                    id="id_bsfield" 
                                    tabindex="2" 
                                    data-plugin-selectTwo-addBusiness>
                                    <option selected>{{ __('Pilih kategori usaha') }}</option>
                                    @foreach($field as $f)
                                    <option value="{{ $f->id_bsfield }}">{{ $f->bsfields_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="brand"><strong>{{ __('Brand') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="brand" 
                                    id="brand" 
                                    tabindex="3">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="business_contact"><strong>{{ __('No. Telepon') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="business_contact" 
                                    id="business_contact" 
                                    tabindex="4">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="desc"><strong>{{ __('Deskripsi') }}</strong> {{ __('(optional)') }}</label>
                                <textarea class="form-control" 
                                    name="desc" 
                                    id="desc" 
                                    row="1" 
                                    tabindex="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="address"><strong>{{ __('Alamat') }}</strong> {{ __('(harus diisi)') }}</label>
                                <textarea class="form-control" 
                                    name="address" 
                                    id="address" 
                                    row="1" 
                                    tabindex="6"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-2">
                                <label for="website"><strong>{{ __('Link detail') }}</strong> {{ __('(optional)') }}</label>
                                <input type="link" 
                                    class="form-control" 
                                    name="website" 
                                    id="website" 
                                    tabindex="7">
                            </div>
                        </div>
                    </div>

                    <div class="modal-action text-center mt-4">
                        <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                        <button type="button" class="btn btn-primary save-business" tabindex="8"><i class="icon icon-doc"></i> {{ __('Simpan') }}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>

    <!-- {{-- Modal : Edit Business --}} -->
    <div id="detailBusiness" class="modal-block modal-block-primary modal-block-lg mfp-hide">
        <section class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3 class="mt-0 mb-2"><i class="icon icon-info"></i> {{ __('Detail Usaha') }}</h3>
                    <p>{{ __('Anda dapat melakukan perubahan data usaha pada formulir di bawah.') }}</p>
                </div>
                <form action="" method="" id="form-business" class="form-business">
                    @csrf @method('PATCH')
                    <input type="hidden" name="save_method" id="save_method" value="edit">
                    <input type="hidden" name="id_user" id="id_user" value="{{ $user->id_user }}">
                    <input type="hidden" name="id_business_edit" id="id_business_edit">
                    <input type="hidden" name="id" id="id" value="{{ $user->id_bsfield }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="business_name"><strong>{{ __('Nama usaha') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text"
                                    class="form-control" 
                                    name="business_name" 
                                    id="business_name" 
                                    value="Perkebunan Kelapa Sawit" 
                                    tabindex="1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="id_bsfield"><strong>{{ __('Bidang usaha') }}</strong> {{ __('(harus diisi)') }}</label>
                                <select class="form-control" 
                                    name="id_bsfield" 
                                    id="id_bsfield" 
                                    tabindex="2" 
                                    data-plugin-selectTwo-detailBusiness>
                                    <option>{{ __('Pilih kategori usaha') }}</option>
                                    @foreach($field as $f)
                                    <option value="{{ $f->id_bsfield }}" 
                                    @if($user->id_bsfield == $f->id_bsfield) selected @endif>{{ $f->bsfields_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="brand"><strong>{{ __('Brand') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="brand" 
                                    id="brand" 
                                    value="PT. Makmur Bersama" 
                                    tabindex="3">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="business_contact"><strong>{{ __('No. Telepon') }}</strong> {{ __('(harus diisi)') }}</label>
                                <input type="text" 
                                    class="form-control" 
                                    name="business_contact" 
                                    id="business_contact" 
                                    value="(02177) 934 9200" 
                                    tabindex="4">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="desc"><strong>{{ __('Deskripsi') }}</strong> {{ __('(optional)') }}</label>
                                <textarea class="form-control" 
                                    name="desc" 
                                    id="desc" 
                                    row="1" 
                                    tabindex="5">
                                    Kami selalu mengedepankan kemakmuran masyarakat Indonesia secara merata.
                                </textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label for="address"><strong>{{ __('Alamat') }}</strong> {{ __('(harus diisi)') }}</label>
                                <textarea class="form-control" 
                                    name="address" 
                                    id="address" 
                                    row="1" 
                                    tabindex="6">
                                    Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552
                                </textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-2">
                                <label for="website"><strong>{{ __('Link detail') }}</strong> {{ __('(optional)') }}</label>
                                <input type="link" 
                                    class="form-control" 
                                    name="website" 
                                    id="website" 
                                    value="https://ptmakmursejahterabersama.com" 
                                    tabindex="7">
                            </div>
                        </div>
                    </div>

                    <div class="modal-action text-center mt-4">
                        <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                        <button type="button" class="btn btn-primary save-business" tabindex="8"><i class="icon icon-doc"></i> {{ __('Perbarui') }}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>

    <!-- {{-- Modal : Delete Business --}} -->
    <div id="deleteBusiness" class="modal-block modal-block-primary modal-block-sm mfp-hide">
        <section class="card">
            <div class="card-body text-center">
                <h3 class="mt-0 mb-4">{{ __('Anda Yakin?') }}</h3>
                <p>
                    {{ __('Data ini akan dihapus secara permanen. Sehingga Anda
                    tidak akan bisa mengembalikannya lagi.') }}
                    <input type="hidden" 
                        name="id_business_delete" 
                        id="id_business_delete">
                </p>
                <div class="modal-action mt-3">
                    <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                    <button type="button" class="btn btn-danger delete-business"><i class="icon icon-trash"></i> {{ __('Ya, hapus') }}</button>
                </div>
            </div>
        </section>
    </div>

    <!-- {{-- Modal : Add Program --}} -->
    <div id="addProgram" class="modal-block modal-block-primary modal-block-md mfp-hide">
        <section class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3 class="mt-0 mb-2"><i class="icon icon-notebook"></i> {{ __('Tambah Program') }}</h3>
                    <p>{{ __('Anda dapat menambahkan lebih dari 1 program yang sudah diikuti.') }}</p>
                </div>
                <form action="" method="POST" id="form-program" class="form-program">
                    @csrf @method('POST')
                    <input type="hidden" name="save_method" id="save_method" value="add">
                    <input type="hidden" name="id_user" id="id_user" value="{{ $user->id_user }}">
                    <div class="form-group mb-2">
                        <label for="program_category"><strong>{{ __('Pilih kategori program') }}</strong> {{ __('(harus diisi)') }}</label>
                        <select class="form-control procat" 
                            name="program_category" 
                            id="program_category" 
                            data-plugin-selectTwo-addProgram>
                            <option selected disabled>{{ __('Pilih kategori program') }}</option>
                            @foreach($procat as $p)
                            <option value="{{ $p->id_procat }}">{{ $p->procat_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-2">
                        <label for="program"><strong>{{ __('Pilih program') }}</strong> {{ __('(harus diisi)') }}</label>
                        <select class="form-control mempro" 
                            name="program" 
                            id="program" 
                            data-plugin-selectTwo-addProgram required="required">
                            <option selected disabled>{{ __('Pilih program') }}</option>
                        </select>
                    </div>

                    <div class="modal-action text-center mt-4">
                        <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                        <button type="button" class="btn btn-primary save-program"><i class="icon icon-doc"></i> {{ __('Tambahkan') }}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>

    <!-- {{-- Modal : Edit Program --}} -->
    <div id="detailProgram" class="modal-block modal-block-primary modal-block-md mfp-hide">
        <section class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3 class="mt-0 mb-2"><i class="icon icon-pencil"></i> {{ __('Ubah Program') }}</h3>
                    <p>{{ __('Anda dapat memilih kembali dari program yang sudah diikuti.') }}</p>
                </div>
                <form action="" method="POST" id="form-program" class="form-program">
                    @csrf @method('PATCH')
                    <input type="hidden" name="save_method" id="save_method" value="edit">
                    <input type="hidden" name="id_user" id="id_user" value="{{ $user->id_user }}">
                    <input type="hidden" name="id_mempro_edit" id="id_mempro_edit">
                    <div class="form-group mb-2">
                        <label for="program_category_edit"><strong>{{ __('Pilih kategori program') }}</strong> {{ __('(harus diisi)') }}</label>
                        <select class="form-control procat" 
                            name="program_category_edit" 
                            id="program_category_edit" 
                            data-plugin-selectTwo-detailProgram>
                            <option>{{ __('Pilih kategori program') }}</option>
                            @foreach($procat as $p)
                            <option value="{{ $p->id_procat }}">{{ $p->procat_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-2">
                        <label for="program"><strong>{{ __('Pilih program') }}</strong> {{ __('(harus diisi)') }}</label>
                        <select class="form-control mempro" 
                            name="program" 
                            id="program" 
                            data-plugin-selectTwo-detailProgram
                            required="required">
                            <option>{{ __('Pilih program') }}</option>
                        </select>
                    </div>

                    <div class="modal-action text-center mt-4">
                        <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                        <button type="button" class="btn btn-primary save-program"><i class="icon icon-doc"></i> {{ __('Simpan') }}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>

    <!-- {{-- Modal : Delete Program --}} -->
    <div id="deleteProgram" class="modal-block modal-block-primary modal-block-sm mfp-hide">
        <section class="card">
            <div class="card-body text-center">
                <h3 class="mt-0 mb-4">{{ __('Anda Yakin?') }}</h3>
                <p>
                    {{ __('Data ini akan dihapus secara permanen. Sehingga Anda
                    tidak akan bisa mengembalikannya lagi.') }}
                </p>
                <div class="modal-action mt-3">
                    <button class="btn btn-default modal-dismiss"><i class="icon icon-close"></i> {{ __('Batalkan') }}</button>
                    <button type="button" class="btn btn-danger delete-program"><i class="icon icon-trash"></i> {{ __('Ya, hapus') }}</button>
                </div>
            </div>
        </section>
    </div>

    <!-- MODAL CHANGE PHOTO -->
    <div class="modal modal-round modal-avatar fade" id="change-photo" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="img-container">
                        <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="crop">{{ __('Crop Photo') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">{{ __('Batal') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END : MODAL CHANGE PHOTO -->
@endsection

@section('blockfoot')
    <script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('vendor/cropper/cropper.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            var save_method
            /*
            Datatable : List 'Usaha'
            */
            var tbusiness = $('.business-table').DataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                processing: true,
                "columnDefs": [
                    { "orderable": false, "targets": 4 }
                ],
                "ajax": {
                    "url": "/member/profil/usaha/{{ $user->id_user }}",
                    "type": "GET",
                    "serverSide": true
                },
            });

            /*
            Datatable : List 'Program'
            */
            var tprogram = $('.program-table').DataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                processing: true,
                "columnDefs": [
                    { "orderable": false, "targets": 4 }
                ],
                "ajax": {
                    "url": "/member/profil/program/{{ $user->id_user }}",
                    "type": "GET",
                    "serverSide": true
                },
            });

            /*
            Modal : Delete Business Category
            */
            $('.modal-basic').magnificPopup({
                type: 'inline',
                preloader: false,
                modal: true
            });

            /*
            Navigation Active
            */
            $('.member-profile').addClass('nav-active');

            /*
            Modal Dismiss : Delete Business Category
            */
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });

            /*
            Navigation Active
            */
            $('.member').addClass('nav-expanded nav-active');
            $('.all-member').addClass('nav-active');

            /*
            GO PREVIOUS PAGE
            */
            $(document).ready(function() {
                $('a.go-back').click(function () {
                    parent.history.back();
                    return false;
                });
            });

            /*
            SELECT2 IN MODAL
            */
            (function($) {
                'use strict';
                if ( $.isFunction($.fn[ 'select2' ]) ) {
                    $(function() {
                        $('[data-plugin-selectTwo-addBusiness]').each(function() {
                            var $this = $( this ),
                                opts = {
                                    dropdownParent: $("#addBusiness")
                                };
                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;
                            $this.themePluginSelect2(opts);
                        });
                        $('[data-plugin-selectTwo-detailBusiness]').each(function() {
                            var $this = $( this ),
                                opts = {
                                    dropdownParent: $("#detailBusiness")
                                };
                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;
                            $this.themePluginSelect2(opts);
                        });
                        $('[data-plugin-selectTwo-addProgram]').each(function() {
                            var $this = $( this ),
                                opts = {
                                    dropdownParent: $("#addProgram")
                                };
                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;
                            $this.themePluginSelect2(opts);
                        });
                        $('[data-plugin-selectTwo-detailProgram]').each(function() {
                            var $this = $( this ),
                                opts = {
                                    dropdownParent: $("#detailProgram")
                                };
                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;
                            $this.themePluginSelect2(opts);
                        });
                    });
                }
            }).apply(this, [jQuery]);

            // Get: Provincy
            $('#province').on('change', function(e){
                // console.log(e);
                var id_province = e.target.value;
                $.get('/member/profil/kabupaten/' + id_province, function(data){
                    // console.log(data);                    
                    $('#regency').empty();
                    $('#regency').append('<option value="0" disable selected>Pilih kabupaten</option>');
                    
                    $('#district').empty();
                    $('#district').append('<option value="0" disable selected">Pilih kecamatan</option>');
                
                    $.each(data, function(index, regenciesObj){
                        $('#regency').append('<option value="'+ regenciesObj.id +'">'+ regenciesObj.name +'</option>');
                    });
                });
            });

            // Get: Regency
            $('#regency').on('change', function(e){
                var id_regency = e.target.value;
                $.get('/member/profil/kecamatan/' + id_regency, function(data){
                    $('#district').empty();
                    $('#district').append('<option value="0" disable selected>Pilih kecamatan</option>');
                    $.each(data, function(index, districtsObj){
                        $('#district').append('<option value="'+ districtsObj.id +'">'+ districtsObj.name +'</option>');
                    });
                });
            });

            // Get: Program Categories
            $('.procat').on('change', function(e){
                var id_procat = e.target.value;
                $.get('/member/profil/program/kategori/' + id_procat, function(data){
                    $('.mempro').empty();
                    $('.mempro').append('<option value="0" disable selected>Pilih program</option>');
                    $.each(data, function(index, programObj){
                        $('.mempro').append('<option value="'+ programObj.id_program +'">'+ programObj.program_name +'</option>');
                    });
                });
            });

            // Module : form submit bisnis anggota
            $('.save-business').on('click', function (e) {
                if (!e.isDefaultPrevented()) {

                    save_method = $("#save_method").val();
                    var id_business = $("#id_business_edit").val();

                    if (save_method === 'add') url = "{{ route('usaha.profil.store') }}";
                    else url = "/member/profil/usaha/update/" + id_business;
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        contentType: false,
                        data: new FormData($(".form-business")[0]),
                        async: true,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $('.alert-success').css('display', 'block').delay(2000).fadeOut();
                            $('.business-table').DataTable().ajax.reload();
                            $.magnificPopup.close();
                        },
                        error: function () {
                            $('.alert-danger').css('display', 'block').delay(2000).fadeOut();
                        }
                    });
                    return false;
                }
            });

            // Module : form submit program anggota
            $('.save-program').on('click', function (e) {
                if (!e.isDefaultPrevented()) {

                    save_method = $("#save_method").val();
                    var id_mempro = $("#id_mempro_edit").val();

                    if (save_method === 'add') url = "{{ route('program.profil.store') }}";
                    else url = "/member/profil/program/update/" + id_mempro;
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        contentType: false,
                        data: new FormData($(".form-program")[0]),
                        async: true,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $('.alert-success').css('display', 'block').delay(2000).fadeOut();
                            $('.program-table').DataTable().ajax.reload();
                            $.magnificPopup.close();
                        },
                        error: function () {
                            $('.alert-danger').css('display', 'block').delay(2000).fadeOut();
                        }
                    });
                    return false;
                }
            });
        });
        
        /*
        CHANGE MEMBER PHOTO
        */
        window.addEventListener('DOMContentLoaded', function () {
            var avatar = document.getElementById('avatar');
            var image = document.getElementById('image');
            var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#change-photo');
            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function (e) {
                var files = e.target.files;
                var done = function (url) {
                    input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    aspectRatio: 1,
                    viewMode: 3,
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function () {
                var initialAvatarURL;
                var canvas;

                $modal.modal('hide');

                if ( cropper ) {
                    canvas = cropper.getCroppedCanvas({
                        width: 400,
                        height: 400,
                    });

                    canvas.toBlob(function (blob) {
                        var formData = new FormData();

                        formData.append('avatar', blob);

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        // $.ajax("{{ route('change_avatar', ['id_user' => Auth::id()]) }}", {

                        $.ajax("{{ route('change_avatar', $user->id_user) }}", {
                            type: 'POST',
                            data: formData,
                            processData: false,
                            contentType: false,
                            xhr: function () {
                                var xhr = new XMLHttpRequest();

                                xhr.upload.onprogress = function (e) {
                                    var percent = '0';
                                    var percentage = '0%';

                                    if (e.lengthComputable) {
                                        percent = Math.round((e.loaded / e.total) * 100);
                                        percentage = percent + '%';
                                        $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                                    }
                                };

                                return xhr;
                            },

                            success: function (response) {
                                var json = $.parseJSON( response );
                                $('.change-avatar').attr('src', json.url);
                                $alert.show().addClass('alert-success').text('Upload success');
                            },

                            error: function () {
                                avatar.src = initialAvatarURL;
                                $alert.show().addClass('alert-warning').text('Upload error');
                            },

                            complete: function () {
                                $progress.hide();
                            },
                        });
                    });
                }
            });
        });

        // Modal : edit business
        function editBusiness(id_business) {
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#detailBusiness'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        // $(".edit-business").on('click', function(){
                            $.ajax({
                                url: "/member/profil/usaha/detail/" + id_business,
                                type: "GET",
                                dataType: "JSON",
                                success: function(data){
                                    $('#id_user').val(data.id_user);
                                    $('#id_business_edit').val(data.id_business);
                                    $('#business_name').val(data.business_name);
                                    $('#brand').val(data.brand);
                                    $('#desc').val(data.desc);
                                    $('#address').val(data.address);
                                    $('#business_contact').val(data.business_contact);
                                    $('#website').val(data.website);
                                },
                                error: function(){
                                    alert("Error: Tidak dapat menampilkan edit Data!");
                                    console.log(data)
                                }
                            });
                        // });
                    }
                }
            });
        }

        // Modal : delete business
        function deleteBusiness(id_business) {
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#deleteBusiness'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        $(".delete-business").on('click', function(){
                            // alert('del');
                            $.ajax({
                                url: "/member/profil/usaha/hapus/"+id_business,
                                type: "POST",
                                data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                                success: function () {
                                    $(".alert-danger").css("display", "block").delay(2000).fadeOut();
                                    $.magnificPopup.close();
                                    console.log()
                                },
                                error: function () {    
                                    $(".alert-success").css("display", "block").delay(2000).fadeOut();
                                    $.magnificPopup.close();
                                    $('.business-table').DataTable().ajax.reload();
                                    console.log()
                                }
                            });                            
                        });
                    }
                }
            });
        }

        // Modal : edit program
        function editProgram(id_mempro) {
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#detailProgram'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        $.ajax({
                            url: "/member/profil/program/detail/" + id_mempro,
                            type: "GET",
                            dataType: "JSON",
                            success: function(data){
                                $('#id_user').val(data.id_user);
                                $('#id_mempro_edit').val(data.id_mempro);
                                $('#program').append('<option value="'+ data.id_program +'" selected>'+ data.program_name +'</option>');                            },
                            error: function(){
                                alert("Error: Tidak dapat menampilkan edit Data!");
                                console.log(data)
                            }
                        });
                    }
                }
            });
        }
        
        // Modal : delete program
        function deleteProgram(id_mempro) {
            $.magnificPopup.open({
                modal: true,
                items: {
                    src: $('#deleteProgram'),
                    type: 'inline',
                },
                callbacks: {
                    open: function () {
                        $(".delete-program").on('click', function(){
                            // alert('del');
                            $.ajax({
                                url: "/member/profil/program/hapus/"+id_mempro,
                                type: "POST",
                                data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                                success: function () {
                                    $(".alert-danger").css("display", "block").delay(2000).fadeOut();
                                    $.magnificPopup.close();
                                    console.log()
                                },
                                error: function () {    
                                    $(".alert-success").css("display", "block").delay(2000).fadeOut();
                                    $.magnificPopup.close();
                                    $('.program-table').DataTable().ajax.reload();
                                    console.log()
                                }
                            });                            
                        });
                    }
                }
            });
        }

    </script>
@endsection
