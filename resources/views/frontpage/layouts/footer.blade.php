<footer class="tbr_footer">
    <div class="container">
        <div class="tbr_company_info">
            <div class="row">
                <div class="col-md-8">
                    <p>&copy; Copyright 2019 - <a href="">MasjidPreneur Ltd</a>. All Rights Reserved.</p>
                </div>
                <div class="col-md-4">
                    <ul class="tbr_list_social_icon">
                        <li><a href=""><i class="icon icon-social-facebook"></i></a></li>
                        <li><a href=""><i class="icon icon-social-instagram"></i></a></li>
                        <li><a href=""><i class="icon icon-social-twitter"></i></a></li>
                        <li><a href=""><i class="icon icon-social-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>