<header class="tbr_main_header">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-3">
                <div class="tbr_brand">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/images/logo.png') }}" class="d-none d-xl-block" alt="MasjidPreneur">
                        <img src="{{ asset('assets/images/favicon_62x62.png') }}" class="d-xl-none" alt="MasjidPreneur">
                    </a>
                </div>
            </div>
            <div class="col-9">
                <div class="tbr_action_mobile">
                    <button type="button" class="btn btn-primary tbr_search">
                        <i class="icon icon-magnifier"></i>
                    </button>
                    <button type="button" id="sidebarCollapse" class="btn btn-primary">
                        <i class="icon icon-menu"></i> <span>Navigation</span>
                    </button>
                </div>

                <div class="tbr_right_content">
                    <nav class="tbr_main_menu">
                        <ul>
                            <li class="fp_home_page"><a href="{{ route('fp_home_page') }}">Home</a></li>
                            <li class="fp_business_directory"><a href="{{ route('fp_business_directory') }}">Direktori</a></li>
                            <li class="fp_about_us"><a href="{{ route('fp_about_us') }}">Pelatihan</a></li>
                            <li class="fp_contact_us"><a href="{{ route('fp_contact_us') }}">Masjid</a></li>
                            <li class="fp_contact_us"><a href="{{ route('fp_contact_us') }}">Syirkah</a></li>
                            <li class="fp_component"><a href="{{ route('fp_component') }}">Components</a></li>
                            <li class="tbr_search"><a><i class="icon icon-magnifier"></i></a></li>
                        </ul>
                    </nav>

                    <div class="tbr_action_header">
                        @if (Route::has('login'))
                            @auth
                                @if(Auth::user()->role == 99)
                                <a href="{{ ('/semua-pengguna') }}" 
                                    class="btn btn-primary">
                                    <i class="icon icon-home"></i> 
                                    <span>Dashboard</span></a>
                                @else
                                <a href="{{ ('/member/profil') }}" 
                                    class="btn btn-primary">
                                    <i class="icon icon-home"></i> 
                                    <span>Dashboard</span></a>
                                @endif
                            @else
                                <a href="{{ route('fp_member_register') }}" 
                                    class="btn btn-primary">
                                    <i class="icon icon-user"></i>
                                    Pendaftaran</a>
                                <a href="{{ url('/login') }}" 
                                    class="btn btn-primary ml-2">
                                    <i class="icon icon-login"></i> 
                                    Login</a>
                            @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>