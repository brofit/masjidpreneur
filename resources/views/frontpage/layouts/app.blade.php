<!doctype html>
<html class="tbr_frontpage" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>

		<meta charset="UTF-8">
		<title>MasjidPreneur - @yield('title')</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="keywords" content="ERP System" />
		<meta name="description" content="ERP System - Tebar System Development">
		<meta name="author" content="Tebar.co.id">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <link rel="shortcut icon" href="{{ asset('assets/images/favicon_62x62.png') }}" type="image/x-icon" />
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">

		<!-- Vendor CSS -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
   		<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/animate/animate.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}">
		@yield('blockhead')

		<!-- Assets CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/frontpage.css') }}">

		<!-- Head Libs -->
		<script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>

	</head>
	<body>

        <div class="tbr_wrapper">
            {{-- HEADER --}}
            @include('frontpage.layouts.header')

            {{-- DYNAMIC CONTENT --}}
            @yield('content')

            {{-- HEADER --}}
            @include('frontpage.layouts.footer')

            {{-- SEARCH --}}
            <div class="modal fade tbr_modal_search" id="search" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="" method="">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Ketik sesuatu di sini lalu tekan enter" autofocus>
                                <div class="input-group-append">
                                    <i class="icon icon-magnifier"></i>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- NAVIGATION DRAWER --}}
        @include('frontpage.layouts.drawer')
        <div class="tbr_drawer_overlay"></div>

        <!-- Vendor JS -->
		<script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
		<script src="{{ asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
		<script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        @yield('blockfoot')
        
        <script>
            $(document).ready(function () {
                /**
                 * SEARCH
                 */
                $('.tbr_search').click(function (){
                    $('#search').modal();
                });

                /**
                 * MOBILE DRAWER NAVIGATION
                 */
                $('#dismiss, .tbr_drawer_overlay').on('click', function () {
                    $('.tbr_nav_drawer').removeClass('active');
                    $('.tbr_drawer_overlay').removeClass('active');
                });

                $('#sidebarCollapse').on('click', function () {
                    $('.tbr_nav_drawer').addClass('active');
                    $('.tbr_drawer_overlay').addClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
    </body>
</html>
