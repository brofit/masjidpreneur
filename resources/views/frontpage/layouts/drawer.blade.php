<nav class="tbr_nav_drawer">
    <div class="tbr_nav_drawer_scroller">
        <div class="tbr_nd_header">
            <div class="tbr_brand">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('assets/images/favicon_62x62.png') }}" alt="MasjidPreneur">
                </a>
            </div>
            <div class="tbr_action_header">
                @if (Route::has('login'))
                    @auth
                        <a href="{{ ('/semua-pengguna') }}" class="btn btn-primary btn-block"><i class="icon icon-home"></i> <span>Dashboard</span></a>
                    @else
                        <a href="{{ route('fp_member_register') }}" class="btn btn-primary btn-block mb-2"><i class="icon icon-user"></i> Pendaftaran</a>
                        <a href="{{ url('/login') }}" class="btn btn-primary btn-block"><i class="icon icon-login"></i> Login</a>
                    @endauth
                @endif
            </div>
        </div>

        <ul class="tbr_mobile_main_menu">
            <li class="fp_home_page"><a href="{{ route('fp_home_page') }}">Home</a></li>
            <li class="fp_business_directory"><a href="{{ route('fp_business_directory') }}">Business Directory</a></li>
            <li class="fp_about_us"><a href="{{ route('fp_about_us') }}">About Us</a></li>
            <li class="fp_contact_us"><a href="{{ route('fp_contact_us') }}">Contact Us</a></li>
        </ul>
    </div>

    <div class="tbr_nd_footer">
        <ul>
            <li><a href=""><i class="icon icon-social-facebook"></i></a></li>
            <li><a href=""><i class="icon icon-social-instagram"></i></a></li>
            <li><a href=""><i class="icon icon-social-twitter"></i></a></li>
            <li><a href=""><i class="icon icon-social-linkedin"></i></a></li>
        </ul>
    </div>
</nav>