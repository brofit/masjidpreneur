<aside class="tbr_filter_aside">
    <h2 class="tbr_b_filter_heading"><i class="icon icon-equalizer"></i> Business Filter</h2>

    <div class="tbr_b_filter_location">
        <p class="tbr_b_filter_title"><i class="icon icon-location-pin"></i> Bedasarkan lokasi</p>
        <form action="">
            <div class="form-group">
                <select class="form-control" name="filter_location" id="filter_location">
                    <option>Pilih kabupaten</option>
                    <option value="Karanganyar">Karanganyar</option>
                    <option value="Sragen">Sragen</option>
                    <option value="Surakarta">Surakarta</option>
                </select>
            </div>
        </form>
    </div>

     <div class="tbr_b_filter_category">
        <p class="tbr_b_filter_title"><i class="icon icon-pin"></i> Bedasarkan bidang usaha</p>
        <form action="">
            <div class="tbr_list_category">
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[1]" id="business_category[1]">
                        <label class="custom-control-label" for="business_category[1]">Pertanian (Agriculture)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[2]" id="business_category[2]">
                        <label class="custom-control-label" for="business_category[2]">Pertambangan (Mining)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[3]" id="business_category[3]">
                        <label class="custom-control-label" for="business_category[3]">Pabrikasi (Manufacturing)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[4]" id="business_category[4]">
                        <label class="custom-control-label" for="business_category[4]">Kontruksi (Contruction)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[5]" id="business_category[5]">
                        <label class="custom-control-label" for="business_category[5]">Perdagangan (Trade)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[6]" id="business_category[6]">
                        <label class="custom-control-label" for="business_category[6]">Jasa Keuangan (Financial Service)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[7]" id="business_category[7]">
                        <label class="custom-control-label" for="business_category[7]">Jasa Perorangan (Personal Service)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[8]" id="business_category[8]">
                        <label class="custom-control-label" for="business_category[8]">Umum (Public Service)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[9]" id="business_category[9]">
                        <label class="custom-control-label" for="business_category[9]">Kontruksi (Contruction)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[10]" id="business_category[10]">
                        <label class="custom-control-label" for="business_category[10]">Perdagangan (Trade)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[11]" id="business_category[11]">
                        <label class="custom-control-label" for="business_category[11]">Jasa Keuangan (Financial Service)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[12]" id="business_category[12]">
                        <label class="custom-control-label" for="business_category[12]">Jasa Perorangan (Personal Service)</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="business_category[13]" id="business_category[13]">
                        <label class="custom-control-label" for="business_category[13]">Umum (Public Service)</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
</aside>