@extends('frontpage.layouts.app')
@section('title', 'Detail Usaha')
@section('blockhead')
@endsection
@section('content')
    <section class="tbr_business_feature_image" style="background-image: url('{{ asset('assets/images/sample_landscape_01.jpg') }}')">
        <div class="tbr_overlay_feature_image"></div>
    </section>

    <section class="tbr_business_content_top">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="tbr_business_logo">
                        <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6"> 
                            <div class="tbr_business_name">
                                <h2 class="tbr_business_title">Lynx Companion</h2>
                                <p class="mb-0">Bidang usaha : Teknologi (Technology)</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <ol class="breadcrumb mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="icon icon-home"></i></a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="{{ route('fp_business_directory') }}">Business Directory</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Lynx Companion</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tbr_business_mega_tab">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#businessDescription"><i class="icon icon-pencil"></i> Deskripsi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#businessProfile"><i class="icon icon-info"></i>Kontak</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#businessAddress"><i class="icon icon-location-pin"></i> Alamat</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="tbr_business_mega_tab_content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="businessDescription">
                            <div class="row">
                                <div class="col">
                                    <div class="card tbr_card">
                                        <div class="card-body">
                                            <p>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                            </p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="businessProfile">
                            <div class="card tbr_card">
                                <div class="card-body">
                                    <ul class="tbr_business_list_detail">
                                        {{-- <li class="clearfix">
                                            <div class="tbr_icon">
                                                <i class="icon icon-briefcase"></i>
                                            </div>
                                            <div class="tbr_meta">
                                                <span>Nama usaha</span>
                                                <span>Lynx Companion</span>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <div class="tbr_icon">
                                                <i class="icon icon-pin"></i>
                                            </div>
                                            <div class="tbr_meta">
                                                <span>Nama bidang usaha</span>
                                                <span>Teknologi (Technology)</span>
                                            </div>
                                        </li> --}}
                                        <li class="clearfix">
                                            <div class="tbr_icon">
                                                <i class="icon icon-paper-plane"></i>
                                            </div>
                                            <div class="tbr_meta">
                                                <span>Brand</span>
                                                <span>Elegent Themes</span>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <div class="tbr_icon">
                                                <i class="icon icon-phone"></i>
                                            </div>
                                            <div class="tbr_meta">
                                                <span>Kontak</span>
                                                <span>+77 812 9999 54</span>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <div class="tbr_icon">
                                                <i class="icon icon-envelope"></i>
                                            </div>
                                            <div class="tbr_meta">
                                                <span>Email</span>
                                                <span>-</span>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <div class="tbr_icon">
                                                <i class="icon icon-link"></i>
                                            </div>
                                            <div class="tbr_meta">
                                                <span>Lihat profil kami secara detail</span>
                                                <span><a href="https://www.tebar.co.id" target="_blank">https://www.tebar.co.id</a></span>
                                            </div>
                                        </li>
                                        {{-- <li class="clearfix">
                                            <div class="tbr_icon">
                                                <i class="icon icon-home"></i>
                                            </div>
                                            <div class="tbr_meta">
                                                <span>Alamat</span>
                                                <span>Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</span>
                                            </div>
                                        </li> --}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="businessAddress">
                            <div class="row">
                                <div class="col">
                                    <div class="card tbr_card">
                                        <div class="card-body">
                                            <p><i class="icon icon-home"></i> Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</p>
                                        </div>
                                    </div>
                                    <div class="card tbr_card tbr_fill_width_map">
                                        <div class="tbr_card_body">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31641.317712280303!2d110.79432973964092!3d-7.557012308106062!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a15dd9836b4f3%3A0xfea0e19a0ce6851c!2sTEBAR.CO.ID%20-%20Jasa%20Pembuatan%20Website%2C%20Toko%20Online%20%26%20IM%20Solo!5e0!3m2!1sen!2sid!4v1569450701217!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('blockfoot')
    <script>
        $('.fp_business_directory').addClass('active');
    </script>
@endsection