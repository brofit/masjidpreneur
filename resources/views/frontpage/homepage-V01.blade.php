@extends('frontpage.layouts.app')
@section('title', 'Home')
@section('blockhead')
@endsection
@section('content')
    <section class="tbr_cover_page tbr_donate_section" style="background-image: url('{{ asset('assets/images/donate_background_02.png') }}');">
        <div class="tbr_sc_overlay"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="tbr_sc_content">
                        <h1 class="tbr_sc_heading mb-4">Lewat Aplikasi Ini, Umat Muslim Bisa Donasi Pembangunan Masjid Indonesia di Gaza</h1>
                        <p class="tbr_sc_subheading mb-0">
                            Penyaluran donasi kini semakin mudah dengan beragamnya pilihan aplikasi.
                            Salah satunya melalui aplikasi daring www.masjidpreneur.com. Lewat aplikasi ini, warga muslim bisa menyalurkan sumbangan maupun wakaf untuk pembangunan masjid di Gaza, Palestina.
                        </p>
                        <a href="" class="btn btn-primary"><i class="icon icon-wallet"></i> Donasikan Sekarang</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tbr_donate_list">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="text-center mb-5">
                        <h4>Sistem apa yang akan kami bangun?</h4>
                        <p>Sistem yang akan dibangun pada aplikasi MasjidPreneur V.0.1 antara lain :</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-lg-6 col-sm-6 col-12">
                    <div class="card tbr_card tbr_card_collapse">
                        <div class="card-body">
                            <div class="tbr_card_icon"><i class="icon icon-layers"></i></div>
                            <div class="tbr_right_content">
                                <h2 class="tbr_right_title">Direktori Usaha</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <div class="tbr_collapsible_area">
                            <div class="tbr_button_collapse" data-toggle="collapse" href="#CardCollpase01" onmousedown="return false">Selengkapnya...</div>
                            <div class="collapse" id="CardCollpase01">
                                <div class="tbr_desc">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-lg-6 col-sm-6 col-12">
                    <div class="card tbr_card tbr_card_collapse">
                        <div class="card-body">
                            <div class="tbr_card_icon"><i class="icon icon-people tbr_color_purple"></i></div>
                            <div class="tbr_right_content">
                                <h2 class="tbr_right_title">Kegiatan Masjid</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <div class="tbr_collapsible_area">
                            <div class="tbr_button_collapse" data-toggle="collapse" href="#CardCollpase02" onmousedown="return false">Selengkapnya...</div>
                            <div class="collapse" id="CardCollpase02">
                                <div class="tbr_desc">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-lg-6 col-sm-6 col-12">
                    <div class="card tbr_card tbr_card_collapse">
                        <div class="card-body">
                            <div class="tbr_card_icon"><i class="icon icon-notebook tbr_color_rose"></i></div>
                            <div class="tbr_right_content">
                                <h2 class="tbr_right_title">Proposal</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <div class="tbr_collapsible_area">
                            <div class="tbr_button_collapse" data-toggle="collapse" href="#CardCollpase03" onmousedown="return false">Selengkapnya...</div>
                            <div class="collapse" id="CardCollpase03">
                                <div class="tbr_desc">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-lg-6 col-sm-6 col-12">
                    <div class="card tbr_card tbr_card_collapse">
                        <div class="card-body">
                            <div class="tbr_card_icon"><i class="icon icon-paper-plane tbr_color_green"></i></div>
                            <div class="tbr_right_content">
                                <h2 class="tbr_right_title">Coming Soon</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <div class="tbr_collapsible_area">
                            <div class="tbr_button_collapse" data-toggle="collapse" href="#CardCollpase04" onmousedown="return false">Selengkapnya...</div>
                            <div class="collapse" id="CardCollpase04">
                                <div class="tbr_desc">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('blockfoot')
    <script>
        $('.fp_home_page').addClass('active');
    </script>
@endsection