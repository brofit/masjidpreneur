@extends('frontpage.layouts.app')
@section('title', 'Pendaftaran')
@section('blockhead')
@endsection
@section('content')
    <section class="tbr_register_is_coming_soon">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card tbr_card tbr_card_shadow tbr_zi__1">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-md-5">
                                    <img src="{{ asset('assets/images/under_constraction.png') }}" alt="Under Constraction">
                                </div>
                                <div class="col-md-7">
                                    <h1>Oops!</h1>
                                    <p>Saat ini pendaftaran belum tersedia.</p>
                                    <p>
                                        Sistem pada fitur ini masih dalam proses pengembangan.
                                        Bantu kami untuk mengembangkan aplikasi guna agar umat muslim bisa menyalurkan sumbangan 
                                        maupun wakaf untuk pembangunan masjid.
                                    </p>
                                    <div class="action mt-4">
                                        <a href="{{ url('/') }}" class="btn btn-primary"><i class="icon icon-home"></i> Back to home</a>
                                        <a href="{{ url('/') }}" class="btn btn-primary"><i class="icon icon-wallet"></i> Donasi Sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('blockfoot')
    <script>
        $('.fp_register').addClass('active');
    </script>
@endsection