@extends('frontpage.layouts.app')
@section('title', 'Usaha Ditemukan')
@section('blockhead')
@endsection
@section('content')
    <section class="tbr_r_found_heading">
        <div class="container-fluid">
            <h3>Result found</h3>
            <p>Results found with keywords business &nbsp; " Lynx Companion "</p>
        </div>
    </section>

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb pt-0 pl-0 mb-0">
                <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="icon icon-home"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{ route('fp_business_result_found') }}">Business Directory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Result Found</li>
            </ol>
        </nav>
    </div>

    <section class="tbr_business_result">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <p>2 items business :</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="tbr_card tbr_card_shadow">
                        <div class="tbr_img_overflow">
                            <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_01.jpg') }}')">
                                <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_01.jpg') }}" alt="Resto"></a>
                            </div>
                        </div>
                        <div class="tbr_card_body">
                            <div class="tbr_company_logo">
                                <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                            </div>
                            <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                            <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)
                            <div class="tbr_card_meta">
                                <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                            </div>
                        </div>
                        <div class="tbr_card_footer clearfix">
                            <img class="tbr_avatar" src="{{ asset('assets/images/person_03.png') }}" alt="">
                            <div class="tbr_business_owner">
                                <p>John Doe Junior</p>
                                <p>CEO, Founder</p> 
                            </div>
                            <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="tbr_card tbr_card_shadow">
                        <div class="tbr_img_overflow">
                            <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_01.jpg') }}')">
                                <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_01.jpg') }}" alt="Resto"></a>
                            </div>
                        </div>
                        <div class="tbr_card_body">
                            <div class="tbr_company_logo">
                                <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                            </div>
                            <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                            <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)
                            <div class="tbr_card_meta">
                                <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                            </div>
                        </div>
                        <div class="tbr_card_footer clearfix">
                            <img class="tbr_avatar" src="{{ asset('assets/images/person_03.png') }}" alt="">
                            <div class="tbr_business_owner">
                                <p>John Doe Junior</p>
                                <p>CEO, Founder</p> 
                            </div>
                            <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('blockfoot')
@endsection