@extends('frontpage.layouts.app')
@section('title', 'Home')
@section('blockhead')
@endsection
@section('content')
    <div class="tbr_homescreen_v02">
        <section class="tbr_cover_page tbr_donate_section" style="background-image: url('{{ asset('assets/images/donate_background_02.png') }}');">
            <div class="tbr_sc_overlay"></div>
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="tbr_sc_content">
                            <h1 class="tbr_sc_heading mb-4">Lewat Aplikasi Ini, Umat Muslim Bisa Donasi Pembangunan Masjid Indonesia di Gaza</h1>
                            <p class="tbr_sc_subheading mb-0">
                                Penyaluran donasi kini semakin mudah dengan beragamnya pilihan aplikasi.
                                Salah satunya melalui aplikasi daring www.masjidpreneur.com. <br>
                                Lewat aplikasi ini, warga muslim bisa menyalurkan sumbangan maupun wakaf untuk pembangunan masjid di Gaza, Palestina.
                            </p>
                            <a href="" class="btn btn-primary"><i class="icon icon-wallet"></i> Donasikan Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>

            <section class="tbr_donate_list">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="text-center">
                                <h2>Sistem apa yang akan kami bangun?</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-lg-3 col-sm-6 col-12">
                            <div class="card tbr_card tbr_card_collapse">
                                <div class="card-body">
                                    <div class="tbr_card_icon">
                                        {{-- <i class="icon icon-layers"></i> --}}
                                        <img src="{{ asset('assets/images/icon_network.png') }}" alt="Masjid">
                                    </div>
                                    <div class="tbr_right_content">
                                        <h2 class="tbr_right_title">Direktori</h2>
                                        <p>On Progress</p>
                                    </div>
                                </div>
                                <div class="tbr_collapsible_area">
                                    <div class="tbr_button_collapse" data-toggle="modal" data-target="#ModuleDev_01" onmousedown="return false">Pelajari selengkapnya...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-lg-3 col-sm-6 col-12">
                            <div class="card tbr_card tbr_card_collapse">
                                <div class="card-body">
                                    <div class="tbr_card_icon">
                                        {{-- <i class="icon icon-layers"></i> --}}
                                        <img src="{{ asset('assets/images/icon_training.png') }}" alt="Masjid">
                                    </div>
                                    <div class="tbr_right_content">
                                        <h2 class="tbr_right_title">Pelatihan</h2>
                                        <p>Coming Soon</p>
                                    </div>
                                </div>
                                <div class="tbr_collapsible_area">
                                    <div class="tbr_button_collapse" data-toggle="modal" data-target="#ModuleDev_02" onmousedown="return false">Pelajari selengkapnya...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-lg-3 col-sm-6 col-12">
                            <div class="card tbr_card tbr_card_collapse">
                                <div class="card-body">
                                    <div class="tbr_card_icon">
                                        {{-- <i class="icon icon-layers"></i> --}}
                                        <img src="{{ asset('assets/images/icon_mosque.png') }}" alt="Masjid">
                                    </div>
                                    <div class="tbr_right_content">
                                        <h2 class="tbr_right_title">Masjid</h2>
                                        <p>Coming Soon</p>
                                    </div>
                                </div>
                                <div class="tbr_collapsible_area">
                                    <div class="tbr_button_collapse" data-toggle="modal" data-target="#ModuleDev_03" onmousedown="return false">Pelajari selengkapnya...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-lg-3 col-sm-6 col-12">
                            <div class="card tbr_card tbr_card_collapse">
                                <div class="card-body">
                                    <div class="tbr_card_icon">
                                        {{-- <i class="icon icon-layers"></i> --}}
                                        <img src="{{ asset('assets/images/icon_team_work.png') }}" alt="Masjid">
                                    </div>
                                    <div class="tbr_right_content">
                                        <h2 class="tbr_right_title">Syirkah</h2>
                                        <p>Coming Soon</p>
                                    </div>
                                </div>
                                <div class="tbr_collapsible_area">
                                    <div class="tbr_button_collapse" data-toggle="modal" data-target="#ModuleDev_04" onmousedown="return false">Pelajari selengkapnya...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>

    {{-- MODAL --}}
    <div class="modal fade" id="ModuleDev_01" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center mb-4">
                        <img src="{{ asset('assets/images/icon_network.png') }}" class="mb-3" alt="Masjid">
                        <h4>Direktori</h4>
                    </div>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur veritatis tempora consequuntur quae voluptatum rem dolor quaerat soluta! Aspernatur hic vel atque esse explicabo reprehenderit laboriosam dolorum magni aliquid ex.</p>
                    <p class="mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur veritatis tempora consequuntur quae voluptatum rem dolor quaerat soluta! Aspernatur hic vel atque esse explicabo reprehenderit laboriosam dolorum magni aliquid ex.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModuleDev_02" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center mb-4">
                        <img src="{{ asset('assets/images/icon_training.png') }}" class="mb-3" alt="Masjid">
                        <h4>Pelatihan</h4>
                    </div>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur veritatis tempora consequuntur quae voluptatum rem dolor quaerat soluta! Aspernatur hic vel atque esse explicabo reprehenderit laboriosam dolorum magni aliquid ex.</p>
                    <p class="mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur veritatis tempora consequuntur quae voluptatum rem dolor quaerat soluta! Aspernatur hic vel atque esse explicabo reprehenderit laboriosam dolorum magni aliquid ex.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModuleDev_03" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center mb-4">
                        <img src="{{ asset('assets/images/icon_mosque.png') }}" class="mb-3" alt="Masjid">
                        <h4>Masjid</h4>
                    </div>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur veritatis tempora consequuntur quae voluptatum rem dolor quaerat soluta! Aspernatur hic vel atque esse explicabo reprehenderit laboriosam dolorum magni aliquid ex.</p>
                    <p class="mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur veritatis tempora consequuntur quae voluptatum rem dolor quaerat soluta! Aspernatur hic vel atque esse explicabo reprehenderit laboriosam dolorum magni aliquid ex.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModuleDev_04" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center mb-4">
                        <img src="{{ asset('assets/images/icon_team_work.png') }}" class="mb-3" alt="Masjid">
                        <h4>Syirkah</h4>
                    </div>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur veritatis tempora consequuntur quae voluptatum rem dolor quaerat soluta! Aspernatur hic vel atque esse explicabo reprehenderit laboriosam dolorum magni aliquid ex.</p>
                    <p class="mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur veritatis tempora consequuntur quae voluptatum rem dolor quaerat soluta! Aspernatur hic vel atque esse explicabo reprehenderit laboriosam dolorum magni aliquid ex.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('blockfoot')
    <script>
        $('.fp_home_page').addClass('active');
    </script>
@endsection