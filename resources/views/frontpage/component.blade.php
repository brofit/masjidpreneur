@extends('frontpage.layouts.app')
@section('title', 'Components')
@section('blockhead')
@endsection
@section('content')
    {{-- COMING SOON SECTION --}}
    <section class="tbr_coming_soon_wrap" style="background-image: url('{{ asset('assets/images/coming_soon_background.jpg') }}');">
        <div class="tbr_sc_overlay"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="tbr_sc_content">
                        <h1 class="tbr_sc_heading">Comming Soon</h1>
                        <p class="mb-0">An awesome new company for web resources is coming very soon.</p>
                        <p>Enter your email to request an eraly invitation!</p>
                        <form action="" method="">
                            <div class="input-group tbr_advanced_form_control">
                                <input type="text" class="form-control" aria-describedby="tbr_btn_addon_search_dir" placeholder="Enter email address">
                                <div class="input-group-append">
                                    <button class="btn btn-purple" type="submit" id="tbr_btn_addon_search_dir"><i class="icon icon-paper-plane"></i> <span>Submit</span></button>
                                </div>
                            </div>
                        </form>
                        <a href="" class="btn btn-primary"><i class="icon icon-home"></i> Back to Home</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- BREADCRUMBS --}}
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="icon icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ url('') }}">Item 1</a></li>
                <li class="breadcrumb-item"><a href="{{ url('') }}">Item 2</a></li>
                <li class="breadcrumb-item"><a href="{{ url('') }}">Item 3</a></li>
                <li class="breadcrumb-item"><a href="{{ url('') }}">Item 4</a></li>
                <li class="breadcrumb-item active" aria-current="page">Components</li>
            </ol>
        </nav>
    </div>
    
    {{-- ADVANCED CARD --}}
    <section class="tbr_list_card">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="tbr_card tbr_card_shadow">
                        <div class="tbr_img_overflow">
                            <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_01.jpg') }}')">
                                <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_01.jpg') }}" alt="Resto"></a>
                            </div>
                        </div>
                        <div class="tbr_card_body">
                            <div class="tbr_company_logo">
                                <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                            </div>
                            <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                            <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)
                            <div class="tbr_card_meta">
                                <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                            </div>
                        </div>
                        <div class="tbr_card_footer clearfix">
                            <img class="tbr_avatar" src="{{ asset('assets/images/person_03.png') }}" alt="">
                            <div class="tbr_business_owner">
                                <p>John Doe Junior</p>
                                <p>CEO, Founder</p> 
                            </div>
                            <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="tbr_card tbr_card_shadow">
                        <div class="tbr_img_overflow">
                            <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_02.jpg') }}')">
                                <a href=""><img src="{{ asset('asset/images/sample_02.jpg') }}" alt="Resto"></a>
                            </div>
                        </div>
                        <div class="tbr_card_body">
                            <div class="tbr_company_logo">
                                <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                            </div>
                            <a href=""><h2 class="tbr_card_title">Lynx Companion</h2></a>
                            <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)
                            <div class="tbr_card_meta">
                                <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                            </div>
                        </div>
                        <div class="tbr_card_footer clearfix">
                            <img class="tbr_avatar" src="{{ asset('assets/images/person_01.png') }}" alt="">
                            <div class="tbr_business_owner">
                                <p>John Doe Junior</p>
                                <p>CEO, Founder</p> 
                            </div>
                            <a href="" class="btn btn-purple btn-sm">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="tbr_card tbr_card_shadow">
                        <div class="tbr_img_overflow">
                            <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_03.jpg') }}')">
                                <a href=""><img src="{{ asset('asset/images/sample_03.jpg') }}" alt="Resto"></a>
                            </div>
                        </div>
                        <div class="tbr_card_body">
                            <div class="tbr_company_logo">
                                <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                            </div>
                            <a href=""><h2 class="tbr_card_title">Lynx Companion</h2></a>
                            <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)
                            <div class="tbr_card_meta">
                                <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                            </div>
                        </div>
                        <div class="tbr_card_footer clearfix">
                            <img class="tbr_avatar" src="{{ asset('assets/images/person_02.png') }}" alt="">
                            <div class="tbr_business_owner">
                                <p>John Doe Junior</p>
                                <p>CEO, Founder</p> 
                            </div>
                            <a href="" class="btn btn-purple btn-sm">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="tbr_card tbr_card_shadow">
                        <div class="tbr_img_overflow">
                            <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_01.jpg') }}')">
                                <a href=""><img src="{{ asset('asset/images/sample_01.jpg') }}" alt="Resto"></a>
                            </div>
                        </div>
                        <div class="tbr_card_body">
                            <div class="tbr_company_logo">
                                <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                            </div>
                            <a href=""><h2 class="tbr_card_title">Lynx Companion</h2></a>
                            <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)
                            <div class="tbr_card_meta">
                                <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                            </div>
                        </div>
                        <div class="tbr_card_footer clearfix">
                            <img class="tbr_avatar" src="{{ asset('assets/images/person_03.png') }}" alt="">
                            <div class="tbr_business_owner">
                                <p>John Doe Junior</p>
                                <p>CEO, Founder</p> 
                            </div>
                            <a href="" class="btn btn-purple btn-sm">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- PAGINATION --}}
    <section class="mt-4">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav aria-label="Page navigation text-center">
                        <ul class="pagination justify-content-center tbr_pagination mt-3 mb-5">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    {{-- SEARCH LARGE --}}
    <section class="tbr_business_search">
        <div class="container">
            <div class="row">
                <div class="col">
                    <form action="" method="">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg" aria-describedby="tbr_btn_addon_search_dir" placeholder="Ketik di sini lalu tekan enter">
                            <div class="input-group-append">
                                <button class="btn btn-purple btn-lg" type="submit" id="tbr_btn_addon_search_dir"><i class="icon icon-magnifier"></i> <span>Search</span></button>
                            </div>
                        </div>
                        <p class="help-block">Terdapat 57 bisnis dalam 9 kabupaten di Indonesia. Mulai dari bidang teknologi, peternakan, industri, pertanian, fashion, dan banyak lagi.</p>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
    {{-- HEADING --}}
    <section class="tbr_page_heading">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>Business Directory</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt recusandae eveniet totam odit similique natus, consectetur ullam.</p>
                </div>
            </div>
        </div>
    </section>

    {{-- FOOTER --}}
    <footer class="tbr_footer mb-5">
        <div class="container">
            <div class="tbr_footer_widget">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="tbr_widget tbr_widget_statistic">
                            <h4 class="tbr_widget_title">Company</h4>
                            <img src="{{ asset('assets/images/favicon_62x62.png') }}" alt="MasjidPreneur">
                            <div class="tbr_statistic_list">
                                <i class="icon icon-location-pin"></i>
                                <div class="tbr_statistic_item">
                                    <span>19 Kabupaten</span>
                                    <span>Dari 33 provinsi</span>
                                </div>
                            </div>
                            <div class="tbr_statistic_list">
                                <i class="icon icon-pin"></i>
                                <div class="tbr_statistic_item">
                                    <span>47 Usaha</span>
                                    <span>Dari 19 kabupaten</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="tbr_widget">
                            <h4 class="tbr_widget_title">Userfull link</h4>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Business Directory</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Gallery</a></li>
                                <li><a href="#">Another Page</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="tbr_widget">
                            <h4 class="tbr_widget_title">Discover</h4>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Business Directory</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Gallery</a></li>
                                <li><a href="#">Another Page</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="tbr_widget">
                            <h4 class="tbr_widget_title">Join us on</h4>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus maiores qui sed animi repellendus aut quos officiis quibusdam, eos vero tempore aperiam rem ea inventore illo nisi veniam. Aut, perferendis!
                        </div>
                    </div>
                </div>
            </div>
            <div class="tbr_company_info">
                <div class="row">
                    <div class="col-md-8">
                        <p>&copy; Copyright 2019 - <a href="">MasjidPreneur Ltd</a>. All Rights Reserved.</p>
                    </div>
                    <div class="col-md-4">
                        <ul class="tbr_list_social_icon">
                            <li><a href=""><i class="icon icon-social-facebook"></i></a></li>
                            <li><a href=""><i class="icon icon-social-instagram"></i></a></li>
                            <li><a href=""><i class="icon icon-social-twitter"></i></a></li>
                            <li><a href=""><i class="icon icon-social-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    {{-- MEGA TAB --}}
    <section class="tbr_business_mega_tab mb-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#"><i class="icon icon-home"></i> Navigation 1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#"><i class="icon icon-pencil"></i> Navigation 2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#"><i class="icon icon-heart"></i> Navigation 3</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#"><i class="icon icon-info"></i> Navigation 4</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#"><i class="icon icon-user"></i> Navigation 5</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    {{-- CARD & LIST --}}
    <section>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card tbr_card">
                        <div class="card-body">
                            <ul class="tbr_business_list_detail">
                                <li class="clearfix">
                                    <div class="tbr_icon">
                                        <i class="icon icon-briefcase"></i>
                                    </div>
                                    <div class="tbr_meta">
                                        <span>Nama usaha</span>
                                        <span>Lynx Companion</span>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="tbr_icon">
                                        <i class="icon icon-pin"></i>
                                    </div>
                                    <div class="tbr_meta">
                                        <span>Nama bidang usaha</span>
                                        <span>Teknologi (Technology)</span>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="tbr_icon">
                                        <i class="icon icon-paper-plane"></i>
                                    </div>
                                    <div class="tbr_meta">
                                        <span>Brand</span>
                                        <span>Elegent Themes</span>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="tbr_icon">
                                        <i class="icon icon-phone"></i>
                                    </div>
                                    <div class="tbr_meta">
                                        <span>Kontak</span>
                                        <span>+77 812 9999 54</span>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="tbr_icon">
                                        <i class="icon icon-home"></i>
                                    </div>
                                    <div class="tbr_meta">
                                        <span>Alamat</span>
                                        <span>Jl. Palem RT 05 RW IX Cemani, Grogol, Sukoharjo, Jawa Tengah 57552</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- CARD --}}
    <section>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card tbr_card mb-0">
                        <div class="card-body">
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- OOPS! --}}
    <section class="tbr_register_is_coming_soon">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card tbr_card tbr_card_shadow tbr_zi__1">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-md-5">
                                    <img src="{{ asset('assets/images/under_constraction.png') }}" alt="Under Constraction">
                                </div>
                                <div class="col-md-7">
                                    <h1>Oops!</h1>
                                    <p>Saat ini pendaftaran belum tersedia.</p>
                                    <p>
                                        Sistem pada fitur ini masih dalam proses pengembangan.
                                        Bantu kami untuk mengembangkan aplikasi guna agar umat muslim bisa menyalurkan sumbangan 
                                        maupun wakaf untuk pembangunan masjid.
                                    </p>
                                    <div class="action mt-4">
                                        <a href="{{ url('/') }}" class="btn btn-primary"><i class="icon icon-home"></i> Back to home</a>
                                        <a href="{{ url('/') }}" class="btn btn-primary"><i class="icon icon-wallet"></i> Donasi Sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('blockfoot')
    <script>
        $('.fp_component').addClass('active');
    </script>
@endsection