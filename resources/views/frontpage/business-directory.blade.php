@extends('frontpage.layouts.app')
@section('title', 'Direktori Usaha')
@section('blockhead')
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
@endsection
@section('content')
    <section class="tbr_business_search tbr_business_search_sm">
        <div class="tbr_sc_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <form action="" method="">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg" aria-describedby="tbr_btn_addon_search_dir" placeholder="Ketik di sini lalu tekan enter">
                            <div class="input-group-append">
                                <button class="btn btn-purple btn-lg" type="submit" id="tbr_btn_addon_search_dir"><i class="icon icon-magnifier"></i> <span>Search</span></button>
                            </div>
                        </div>
                        {{-- <p class="help-block">Terdapat 57 bisnis dalam 9 kabupaten di Indonesia. Mulai dari bidang teknologi, peternakan, industri, pertanian, fashion, dan banyak lagi.</p> --}}
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="tbr_business_directory">
        <div class="tbr_filter_business">

            @include('frontpage.layouts.business-filter')

        </div>

        <div class="tbr_wrap_business">
            <div class="container">
                <h2 class="tbr_b_filter_heading tbr_open_mobile_filter d-lg-none"><i class="icon icon-equalizer"></i> Business Filter</h2>
                {{-- <div class="row no-gutters align-items-center mb-3">
                    <div class="col-md-12">
                    </div>
                    <div class="col-2">
                        <p class="mb-0 tbr_all_item">47 Items</p>
                    </div>
                    <div class="col-10">
                        <div class="text-right">
                            <div class="btn-group tbr_btn_group" role="group" aria-label="Basic">
                                <button type="button" class="btn btn-default btn-sm">Bisnis Terkahir</button>
                                <button type="button" class="btn btn-default btn-sm">Bisnis Populer</button>
                            </div>
                        </div>
                    </div>
                </div> --}}

                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_landscape_01.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_landscape_01.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_02.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_02.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_03.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_03.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_landscape_01.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_landscape_01.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_02.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_02.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_03.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_03.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_landscape_01.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_landscape_01.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_02.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_02.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_03.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_03.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_landscape_01.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_landscape_01.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_02.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_02.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="tbr_card tbr_card_landscape tbr_card_shadow">
                            <div class="tbr_img_overflow">
                                <div class="tbr_card_feature_img" style="background-image: url('{{ asset('assets/images/sample_03.jpg') }}')">
                                    <a href="{{ route('fp_detail_business') }}"><img src="{{ asset('asset/images/sample_03.jpg') }}" alt="Resto"></a>
                                </div>
                            </div>
                            <div class="tbr_card_body">
                                <div class="tbr_company_logo">
                                    <img src="{{ asset('assets/images/company_logo.jpg') }}" alt="Company Logo">
                                </div>
                                <a href="{{ route('fp_detail_business') }}"><h2 class="tbr_card_title">Lynx Companion</h2></a>
                                <p class="tbr_card_subtitle">Bidang usaha : Teknologi (Technology)</p>
                                <p class="mb-0">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    Quasi amet ab alias? Soluta quod vel laborum minima...
                                </p>
                            </div>
                            <div class="tbr_card_footer">
                                <div class="tbr_card_meta">
                                    <p><i class="icon icon-location-pin"></i> Kabupaten Karanganyar</p>
                                    <p><i class="icon icon-phone"></i> +77 812 9999 54</p>
                                </div>
                                <a href="{{ route('fp_detail_business') }}" class="btn btn-purple btn-sm">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <nav aria-label="Page navigation text-center">
                            <ul class="pagination justify-content-center tbr_pagination mt-3 mb-5">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('blockfoot')
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script>
        $(document).ready(function() {
            /**
             * SELECT2
             */
            $('#filter_location').select2({
                theme: 'bootstrap'
            });

            /**
             * MOBILE DIRECTORIES FILTER
             */
            $('#dismiss, .tbr_drawer_overlay').on('click', function () {
                $('.tbr_filter_business').removeClass('active');
                $('.tbr_drawer_overlay').removeClass('active');
            });

            $('.tbr_open_mobile_filter').on('click', function () {
                $('.tbr_filter_business').addClass('active');
                $('.tbr_drawer_overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });

            /**
             * NAVIGATION ACTIVE
             */
            $('.fp_business_directory').addClass('active');
        });
    </script>
@endsection