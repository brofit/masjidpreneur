<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/semua-pengguna', 'HomeController@index')->name('semua_pengguna');

/*
|--------------------------------------------------------------------------
| ADMIN ROUTES
|--------------------------------------------------------------------------
*/
Route::prefix('admin')->group(function () {
	// path: [host]/admin/
	Route::get('/', 'UserController@index');
	Route::get('/pengaturan', 'UserController@setting')->name('pengaturan');
	Route::post('/changeAvatar', 'MemberController@changeAvatar')->name('change_avatar_admin');

	// MODULE : Pengguna
	// path: [host]/admin/pengguna/
	Route::prefix('/pengguna')->group(function () {
		Route::get('/', 'UserController@index')->name('semua-pengguna');
		Route::get('/data', 'UserController@list_users')->name('user.data');
        Route::get('/tambah', 'UserController@create')->name('add-new-pengguna');
        Route::get('/getRegencies/{id}', 'UserController@getRegencies')->name('getRegencies');
        Route::get('/getDistricts/{id}', 'UserController@getDistrict')->name('getDistrict');
        Route::post('/store', 'UserController@store')->name('user.store');
        Route::get('/get_id_user','UserController@get_id_user')->name('user.get_id_user');
		Route::get('/detail/{id_user}', 'UserController@user_detail')->name('detail.pengguna');
		Route::patch('/update/{id_user}', 'UserController@user_update')->name('update.pengguna');
		Route::delete('/hapus/{id_user}', 'UserController@user_destroy')->name('hapus.pengguna');
	});

	// MODULE : Anggota 
	// path: [host]/admin/anggota/
	Route::prefix('/anggota')->group(function () {
		Route::get('/', 'MemberController@index')->name('semua-anggota');
		Route::get('/data', 'MemberController@list_members')->name('member.data');
		Route::get('/id_user','MemberController@get_id_user')->name('user.id_user');
		Route::get('/kabupaten/{id_province}', 'MemberController@regencies');
		Route::get('/kecamatan/{id_regency}', 'MemberController@districts');
		Route::get('/program/kategori/{id_procat}', 'MemberController@program_categories');

		// route: crud anggota
		Route::get('/tambah', 'MemberController@add_members')->name('tambah-anggota');
		Route::get('/detail/{id_member}', 'MemberController@member_detail')->name('anggota.detail');
		Route::delete('/hapus/{id_member}', 'MemberController@member_destroy')->name('anggota.destroy');
		Route::post('/store', 'MemberController@member_store')->name('member.store');
		
		// route: bisnis / usaha anggota
		Route::get('/usaha/{id_user}', 'MemberController@list_member_business')->name('usaha.anggota');
		Route::post('/usaha/store', 'MemberController@member_business_store')->name('usaha.anggota.store');
		Route::get('/usaha/detail/{id_business}', 'MemberController@member_business_detail')->name('usaha.anggota.detail');
		Route::patch('/usaha/update/{id_business}', 'MemberController@member_business_update')->name('usaha.anggota.update');
		Route::delete('/usaha/hapus/{id_business}', 'MemberController@member_business_destroy')->name('usaha.anggota.destroy');

		// route: program yang diikuti anggota
		Route::get('/program/{id_user}', 'MemberController@list_member_programs')->name('program.anggota');
		Route::post('/program/store', 'MemberController@member_program_store')->name('program.anggota.store');
		Route::get('/program/detail/{id_business}', 'MemberController@member_program_detail')->name('program.anggota.detail');
		Route::patch('/program/update/{id_business}', 'MemberController@member_program_update')->name('program.anggota.update');
		Route::delete('/program/hapus/{id_business}', 'MemberController@member_program_destroy')->name('program.anggota.destroy');

		// route: general setting / edit data pribadi profil anggota
		Route::patch('/profil/update/{id_user}', 'MemberController@member_profil_update')->name('profil.anggota.update');
		Route::patch('/akun/update/{id_user}', 'MemberController@member_account_update')->name('akun.anggota.update');

    });
    
	// MODULE : Usaha
	// path: [host]/admin/usaha/
	Route::prefix('/usaha')->group(function () {
        Route::get('/', 'BusinessController@index')->name('semua-usaha');
		Route::get('/data', 'BusinessController@list_business')->name('usaha.data');

		// MODULE: Bidang Usaha
        Route::get('/bidang-usaha', 'BusinessController@business_category')->name('bidang-usaha');
		Route::get('/bidang-usaha/data', 'BusinessController@list_business_fields')->name('bidang.usaha.data');
        Route::post('/bidang-usaha/store', 'BusinessController@business_fields_store')->name('bidang.usaha.store');
		Route::get('/bidang-usaha/edit/{id_bsfield}', 'BusinessController@business_fields_edit')->name('bidang.usaha.edit');
		Route::patch('/bidang-usaha/update/{id_bsfield}', 'BusinessController@business_fields_update')->name('bidang.usaha.update');
		Route::delete('/bidang-usaha/hapus/{id_bsfield}', 'BusinessController@business_fields_destroy')->name('bidang.usaha.destroy');
	
		// MODULE: Usaha Anggota
		Route::get('/detail/{id_business}', 'BusinessController@business_member_detail')->name('usaha.anggota.detail');
		Route::patch('/update/{id_business}', 'BusinessController@business_member_update')->name('usaha.anggota.update');
		Route::delete('/hapus/{id_business}', 'BusinessController@business_member_destroy')->name('usaha.anggota.destroy');
	});

	// MODULE : Program
	Route::prefix('/program')->group(function () {
		Route::get('/', 'ProgramController@index')->name('semua-program');
		Route::get('/data', 'ProgramController@list_programs')->name('program.data');
		Route::get('/tambah', 'ProgramController@add_program')->name('tambah-program');
		Route::post('/store', 'ProgramController@program_store')->name('program.store');
		Route::get('/edit/{id_program}', 'ProgramController@program_edit')->name('program.edit');
		Route::patch('/update/{id_program}', 'ProgramController@program_update')->name('program.update');
		Route::delete('/hapus/{id_program}', 'ProgramController@program_destroy')->name('program.destory');

		// MODULE: Kategori Program
		Route::get('/kategori', 'ProgramController@program_category')->name('kategori-program');
		Route::get('/kategori-program/data', 'ProgramController@list_program_categories')->name('kategori.program.data');
		Route::post('/kategori-program/store', 'ProgramController@program_category_store')->name('kategori.program.store');
		Route::get('/kategori-program/edit/{id_procat}', 'ProgramController@program_category_edit')->name('kategori.program.edit');
		Route::patch('/kategori-program/update/{id_procat}', 'ProgramController@program_category_update')->name('kategori.kategori.update');
		Route::delete('/kategori-program/hapus/{id_procat}', 'ProgramController@program_category_destroy')->name('kategori.kategori.destroy');
	});
});


/*
|--------------------------------------------------------------------------
| MEMBER ROUTES
|--------------------------------------------------------------------------
*/
// path: [host]/member/profil
Route::prefix('member')->group(function () {
	Route::prefix('/profil')->group(function () {
		Route::get('/', 'MemberController@member_profile')->name('member-profile');
		Route::post('/changeAvatar/{id_user}', 'MemberController@changeAvatar')->name('change_avatar');

		Route::get('/kabupaten/{id_province}', 'MemberController@regencies');
		Route::get('/kecamatan/{id_regency}', 'MemberController@districts');
		Route::get('/program/kategori/{id_procat}', 'MemberController@program_categories');
		Route::get('/detail/{id_member}', 'MemberController@member_detail')->name('profil.detail');
		
		// route: bisnis / usaha profil anggota
		Route::get('/usaha/{id_user}', 'MemberController@list_member_business')->name('usaha.profil');
		Route::post('/usaha/store', 'MemberController@member_business_store')->name('usaha.profil.store');
		Route::get('/usaha/detail/{id_business}', 'MemberController@member_business_detail')->name('usaha.profil.detail');
		Route::patch('/usaha/update/{id_business}', 'MemberController@member_business_update')->name('usaha.profil.update');
		Route::delete('/usaha/hapus/{id_business}', 'MemberController@member_business_destroy')->name('usaha.profil.destroy');

		// route: program yang diikuti profil anggota
		Route::get('/program/{id_user}', 'MemberController@list_member_programs')->name('program.profil');
		Route::post('/program/store', 'MemberController@member_program_store')->name('program.profil.store');
		Route::get('/program/detail/{id_business}', 'MemberController@member_program_detail')->name('program.profil.detail');
		Route::patch('/program/update/{id_business}', 'MemberController@member_program_update')->name('program.profil.update');
		Route::delete('/program/hapus/{id_business}', 'MemberController@member_program_destroy')->name('program.profil.destroy');

		// route: general setting / edit data pribadi profil anggota
		Route::patch('/update/{id_user}', 'MemberController@member_profil_update')->name('profil.member.update');
		Route::patch('/akun/update/{id_user}', 'MemberController@member_account_update')->name('akun.profil.update');
	});
});

/*
|--------------------------------------------------------------------------
| FRONTPAGE ROUTES
|--------------------------------------------------------------------------
*/
Route::prefix('/')->group(function () {
    Route::get('/', 'FrontpageController@index')->name('fp_home_page');
    Route::get('/home-v01', 'FrontpageController@home_v01')->name('fp_home_page_v01');

    Route::get('/direktori', 'FrontpageController@business_directory')->name('fp_business_directory');
    Route::get('/direktori/detail', 'FrontpageController@detail_business')->name('fp_detail_business');
    Route::get('/direktori/ditemukan', 'FrontpageController@business_result_found')->name('fp_business_result_found');
    Route::get('/direktori/tidak-ditemukan', 'FrontpageController@business_result_not_found')->name('fp_business_result_not_found');

    Route::get('/tentang-kami', 'FrontpageController@about_us')->name('fp_about_us');
    Route::get('/hubungi-kami', 'FrontpageController@contact_us')->name('fp_contact_us');
    Route::get('/pendaftaran', 'FrontpageController@member_register')->name('fp_member_register');
    Route::get('/komponen', 'FrontpageController@component')->name('fp_component');
});

/*
|--------------------------------------------------------------------------
| MASARI TEST ROUTES
|--------------------------------------------------------------------------
*/
Route::get('admin/anggota/tes', 'MemberController@tes')->name('tes');