<?php

use Illuminate\Database\Seeder;

class MembersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->insert([
            [
                'id_member'          => '9999',
                'id_user'            => 'U201909069999',
                'member_contact'     => '0813 9099 0019',
                'member_status'      => 'Aktif',
                'dateofbirth'        => '1997-10-22',
                'hometown'           => 'Karanganyar',
                'id'                 => '3311050',
                'address'            => 'Jl. Setapak Block B No.9',
                'gender'             => 'Laki - Laki',
                'group'              => 'Pembina',
            ],
            [
                'id_member'          => '7777',
                'id_user'            => 'U201909067777',
                'member_contact'     => '0813 9099 0017',
                'member_status'      => 'Aktif',
                'dateofbirth'        => '1997-10-22',
                'hometown'           => 'Karanganyar',
                'id'                 => '3311050',
                'address'            => 'Jl. Setapak Block B No.7',
                'gender'             => 'Laki - Laki',
                'group'              => 'Pembina',
            ]
        ]);
    }
}
