<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(MembersSeeder::class);
        $this->call(ProvincesSeeder::class);
        $this->call(RegenciesSeeder::class);
        $this->call(DistrictsSeeder::class);
    }
}
