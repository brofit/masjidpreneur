<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id'                 => '9999',
                'id_user'            => 'U201909069999',
                'fullname'           => 'Brofit Joko Pierce',
                'user_name'          => 'brofit',
                'email'              => 'brofitjp@gmail.com',
                'email_verified_at'  => '2019-09-04 13:38:32',
                'password'           => bcrypt('~'),
                'avatar'             => NULL,
                'role'               => 99,
                'user_status'        => 'Aktif',
            ],
            [
                'id'                 => '7777',
                'id_user'            => 'U201909067777',
                'fullname'           => 'David Alexander Pierce',
                'user_name'          => 'david',
                'email'              => 'david@gmail.com',
                'email_verified_at'  => '2019-09-04 13:38:32',
                'password'           => bcrypt('~'),
                'avatar'             => NULL,
                'role'               => 1,
                'user_status'        => 'Aktif',
            ]
        ]);
    }
}
