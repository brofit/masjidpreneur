<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id_member')->unsigned();
            $table->unsignedInteger('id_user');
            $table->string('contact', 50);
            $table->enum('status', ['Aktif', 'Tidak Aktif']);
            $table->date('dateofbirth');
            $table->string('hometown', 100);
            $table->string('id', 7);
            $table->text('address');
            $table->enum('gender', ['Laki - Laki', 'Perempuan']);
            $table->enum('group', ['Anggota Biasa', 'Anggota Kehormatan', 'Pembina']);
            $table->timestamps();

            $table->foreign('id_user')
                  ->references('id_user')->on('mp_users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
