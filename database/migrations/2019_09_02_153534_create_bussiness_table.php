<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBussinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bussiness', function (Blueprint $table) {
            $table->bigIncrements('id_bussiness')->unsigned();
            $table->bigInteger('id_bsfield');
            $table->bigInteger('id_member');
            $table->string('brand', 200);
            $table->text('desc');
            $table->text('address');
            $table->string('contact', 50);
            $table->string('website', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bussiness');
    }
}
