<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->bigInteger('id_program');
            $table->bigInteger('id_catprog');
            $table->string('program_name');
            $table->text('desc');
            $table->string('attachment');
            $table->enum('program_status', ['Open', 'Close']);
            $table->date('due_date');
            $table->time('due_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
