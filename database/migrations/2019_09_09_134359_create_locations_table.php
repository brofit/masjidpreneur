<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('loc_provinces')) {
            Schema::create('loc_provinces', function (Blueprint $table) {
               $table->char('id', 2)->primary();
               $table->string('name');
            });
        }

        if (! Schema::hasTable('loc_regencies')) {
            Schema::create('loc_regencies', function (Blueprint $table) {
                $table->char('id', 4)->primary();
                $table->char('province_id', 2);
                $table->string('name');
            });
        }

        if (! Schema::hasTable('loc_districts')) {
            Schema::create('loc_districts', function (Blueprint $table) {
                $table->char('id', 7)->primary();
                $table->char('regency_id', 4);
                $table->string('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
